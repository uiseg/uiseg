const path = require('path')
const webpack = require('webpack')
const jsonImporter = require('node-sass-json-importer')

const SRC_DIR = path.resolve(__dirname, 'src')
const BUILD_DIR = path.resolve(__dirname, 'public')
const NODE_MODULE_DIR = path.resolve(__dirname, 'node_modules')

const config = {
    devtool: 'cheap-module-source-map',
    entry: {
        labeler: `${SRC_DIR}/index.js`,
        vendor: [
            'react', 'semantic-ui-react', 'redux', 'redux-saga', 'prop-types',
            'react-redux', 'reselect', 'tinycolor2', 'react-dom',
        ],
    },
    output: {
        path: BUILD_DIR,
        filename: '[name].entry.js',
        chunkFilename: '[id].chunk.js',
    },
    module: {
        rules: [{
            loader: 'babel-loader',
            test: /\.js$/,
            exclude: NODE_MODULE_DIR,
            query: {
                presets: ['latest', 'react'],
                plugins: [
                    'transform-object-rest-spread',
                    'transform-class-properties',
                    [
                        'transform-runtime', {
                            polyfill: false,
                            regenerator: true,
                        },
                    ],
                ],
            },
        }, {
            test: /\.css$/,
            loaders: ['style-loader', 'css-loader'],
        }, {
            test: /\.json$/,
            loaders: ['json-loader'],
        }, {
            test: /\.sass$/,
            loader: ['style-loader', 'css-loader', {
                loader: 'sass-loader',
                options: {
                    sourceMap: true,
                    importer: jsonImporter,
                },
            }],
        }],
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV),
            },
        }),
        new webpack.optimize.CommonsChunkPlugin({
            names: ['vendor'],
            filename: '[name].js',
        }),
    ],
}


if(process.env.NODE_ENV === 'production') {
    config.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                screw_ie8: true,
            },
        }))
}

module.exports = config
