/* eslint import/no-extraneous-dependencies: "off", global-require: "off" */

const fs = require('fs')
const path = require('path')
const cors = require('cors')
const colors = require('colors') // eslint-disable-line
const trash = require('trash')
const sizeOf = require('image-size')
const express = require('express')
const program = require('commander')
const PrettyError = require('pretty-error')
const bodyParser = require('body-parser')
const request = require('sync-request')

program
    .option('--dataset-dir <DIR>', 'Dataset directory', __dirname)
    .parse(process.argv)

const app = express()
app.use(cors({
    methods: ['GET', 'POST'],
}))

const dataDir = path.join(program.datasetDir, 'raw')
const contourCacheDir = path.join(program.datasetDir, 'contours')
const recordPath = path.join(program.datasetDir, 'records.txt')

new PrettyError().start()

if (!fs.existsSync(dataDir)) {
    throw new Error(`Data directory ${dataDir} not found.`)
}

/*
 * Entry point: /public/<path>
 * Return webpack generated files.
 */
if (process.env.NODE_ENV !== 'production') {
    console.log('***** IN DEVELOPMENT BUILD *****'.yellow)
    const webpack = require('webpack')
    const webpackConfig = require('./webpack.config')
    const webpackMiddleware = require('webpack-dev-middleware')

    const webpackCompiler = webpack(webpackConfig)

    app.use(webpackMiddleware(webpackCompiler, {
        publicPath: '/public/',
        noInfo: true,
        stats: {
            colors: true,
            chunks: false,
            modules: false,
            'errors-only': true,
        },
    }))
}

/*
 * Entry point: /public/<path>
 * Return files in /public folder
 * */
app.use('/public', express.static(path.join(__dirname, 'public')))

/*
 * Entry point: /
 * Return index.html.
 */
app.get('/', (req, res) => {
    res.sendFile(`${__dirname}/index.html`)
})

/*
 * Entry point: /data/
 * Return json data indicating the data path for one file that hasn't been labeled.
 * If no such files, return empty json dict.
 */
app.get('/data/', (req, res) => {
    const records = new Map()
    if (fs.existsSync(recordPath)) {
        for (const rec of fs.readFileSync(recordPath).toString().split('\n')) {
            const [hash, url, realUrl] = rec.split(' ')
            records.set(hash, { url, realUrl })
        }
    }

    let files = fs.readdirSync(dataDir)

    const fileSet = new Map()
    for (const file of files) {
        const [name, ext] = file.split('.')
        let value = 0
        if (fileSet.has(name)) {
            value = fileSet.get(name)
        }
        switch (ext.toLowerCase()) {
            case 'png':
            case 'jpg':
                value += 1
                break
            case 'json':
                value += 2
                break
            case 'txt':
                value += 4
                break
            default:
                break
        }

        fileSet.set(name, value)
    }

    files = Array.from(fileSet.entries()).filter(x => x[1] === 3)
    if (files.length === 0) {
        res.json({})
        return
    }

    const randomFile = files[Math.floor(Math.random() * files.length)]
    if (records.has(randomFile[0])) {
        res.json({
            hash: randomFile[0],
            ...records.get(randomFile[0]),
        })
        return
    }
    res.json({
        hash: randomFile[0],
    })
})

app.get('/delete/:hash', (req, res) => {
    const hash = req.params.hash
    const targets = fs.readdirSync(dataDir).filter(x => x.startsWith(hash))

    trash(targets)
        .then(() => {
            console.log(`Remove ${targets.join(', ')}`)
            res.sendStatus(200)
        })
        .catch((e) => {
            console.log(e.message)
            res.sendStatus(404)
        })
})

/*
 * Entry point: /data/<hash>
 * Return data point <hash> in json format. These columns are included:
 *     1. hash
 *     2. image
 *     3. image size
 *     4. bounding boxes
 */
app.get('/data/:hash', (req, res) => {
    const hash = req.params.hash
    const jsonPath = path.join(dataDir, `${hash}.json`)
    if (!fs.existsSync(jsonPath)) {
        res.status(404).send({})
        return
    }

    let bboxes = null
    const contourPath = path.join(contourCacheDir, `${hash}.json`)
    if (!fs.existsSync(contourPath)) {
        try {
            const bboxRes = request('GET', `http://localhost:8001/${hash}`)
            bboxes = JSON.parse(bboxRes.getBody('utf-8'))
        } catch ({ message }) {
            res.json({ error: message, hash })
            return
        }
        if (bboxes.status !== 'ok') {
            res.json({ error: bboxes.results, hash })
            return
        }

        bboxes = bboxes.results
        fs.writeFileSync(contourPath, JSON.stringify(bboxes))
    } else {
        bboxes = JSON.parse(fs.readFileSync(contourPath))
    }

    for (const ext of ['png', 'jpg']) {
        const imgPath = path.join(dataDir, `${hash}.${ext}`)
        if (fs.existsSync(imgPath)) {
            const imageSize = sizeOf(imgPath)
            const image = fs.readFileSync(imgPath, 'base64')

            res.json({
                hash,
                image,
                imageSize,
                bboxes,
            })

            return
        }
    }

    res.status(404).send({})
})

const jsonParser = bodyParser.json()
app.post('/save/:hash', jsonParser, (req, res) => {
    const hashName = path.join(dataDir, req.params.hash)
    if (!fs.existsSync(`${hashName}.png`) &&
        !fs.existsSync(`${hashName}.jpg`)) {
        res.sendStatus(404)
        return
    }

    try {
        const ret = JSON.stringify(req.body)
        fs.writeFileSync(`${hashName}.label.json`, ret)
    } catch (e) {
        console.error(e)
        res.status(404).send(e.message)
    }

    res.sendStatus(200)
})

app.listen(3000, () => {
    console.log('Labeler listening on port 3000!')
})
