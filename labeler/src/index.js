import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'

import App from './components/App'

import sagas from './sagas'
import reducers from './reducer/'
import { appMiddleware } from './app'

const sagaMiddleware = createSagaMiddleware()

const middleware = compose(
    applyMiddleware(sagaMiddleware, appMiddleware),
    window.devToolsExtension ? window.devToolsExtension() : f => f,
)

const store = createStore(
    reducers,
    middleware,
)

sagaMiddleware.run(sagas)

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root'),
)

// For debugging purpose
window.store = store
