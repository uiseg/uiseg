import {
    saveUrlSelector,
    fetchUrlSelector,
    randomFetchUrlSelector,
    trashDataUrlSelector,
} from './config/config'

export function* fetchData(hash) {
    const data = yield fetch(fetchUrlSelector(hash))
        .then(res => res.text())
        .then(res => {
            console.log(res)
            return JSON.parse(res)
        })

    return data
}

export function* saveData(hash, data) {
    const success = yield fetch(saveUrlSelector(hash), {
        method: 'POST',
        body: JSON.stringify(data),
        headers: new Headers({ 'Content-Type': 'application/json' }),
    }).then(res => res.ok)

    return success
}

export function* trashData(hash) {
    const success = yield fetch(trashDataUrlSelector(hash)).then(res => res.ok)
    return success
}

export function* randomFetchData() {
    const { hash, url, realUrl } = yield fetch(randomFetchUrlSelector())
        .then(res => res.text())
        .then(res => {
            console.log(res)
            return JSON.parse(res)
        })
    if (!hash) {
        throw new Error(
            'No url hash returned. The backend API cannot handle randomness fetch')
    }

    const data = yield fetchData(hash)

    return { hash, url, realUrl, ...data }
}
