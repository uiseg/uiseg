/* eslint no-bitwise: "off" */
const COLORS = ['red', 'orange', 'yellow', 'olive', 'green', 'teal',
    'blue', 'violet', 'purple', 'pink', 'brown', 'grey']
export const TYPES = []

export const mapTypeToColor = (type, isTyping = false) => {
    if (TYPES.includes(type)) {
        return COLORS[TYPES.indexOf(type) % COLORS.length]
    }
    if (isTyping && TYPES.length > 0) {
        TYPES[TYPES.length - 1] = type
        return mapTypeToColor(type)
    }

    TYPES.push(type)
    return mapTypeToColor(type)
}

export function toggleSet(set, item) {
    const newSet = new Set(set)
    if (newSet.has(item)) {
        newSet.delete(item)
    } else {
        newSet.add(item)
    }

    return newSet
}

export function mergeSet(set, modifier) {
    const newSet = new Set(set)
    for (const node of modifier) {
        newSet.add(node)
    }
    return newSet
}

export function subtractSet(set, modifier) {
    const newSet = new Set(set)
    for (const node of modifier) {
        newSet.delete(node)
    }
    return newSet
}

export function intersectSet(setA, setB) {
    const intersection = new Set()
    for (const elem of setB) {
        if (setA.has(elem)) {
            intersection.add(elem)
        }
    }
    return intersection
}

export function toggleMap(map, key, value) {
    const newMap = new Map(map)
    if (newMap.has(key)) {
        newMap.delete(key)
    } else {
        newMap.set(key, value)
    }

    return newMap
}

export function bisectRight(arr, x) {
    let lo = 0
    let hi = arr.length

    while (lo < hi) {
        const mid = ((lo + hi) / 2) | 0 // Math.floor
        if (x < arr[mid]) {
            hi = mid
        } else {
            lo = mid + 1
        }
    }

    return lo
}

export function bisectLeft(arr, x) {
    let lo = 0
    let hi = arr.length

    while (lo < hi) {
        const mid = ((lo + hi) / 2) | 0 // Math.floor
        if (arr[mid] < x) {
            lo = mid + 1
        } else {
            hi = mid
        }
    }

    return lo
}

export function sample(arr, count) {
    const indices = []
    const result = new Array(count)
    for (let i = 0; i < count; i++) {
        const j = Math.floor(Math.random() * (arr.length - i) + i)
        result[i] = arr[indices[j] === undefined ? j : indices[j]]
        indices[j] = indices[i] === undefined ? i : indices[i]
    }

    return result
}
