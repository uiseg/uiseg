// Alert system
// Each time alert is called, all subscribers will be notified with a list of
// messages sorted by the time added to the message pool.
//
// Each message consists of key (the time added to the pool), message, and
// level. If alert is called with persistSeconds specified, the message will be
// automatically removed after N seconds.

const ALERTS = new Map()
const SUBSCRIBER = new Set()

export const LEVEL = new Set(['success', 'info', 'warning', 'error'])

export function subscribe(func) {
    SUBSCRIBER.add(func)
}

export function unsubscribe(func) {
    SUBSCRIBER.delete(func)
}

// Invert sorting so that the newest item will be the top one.
function dataCompare(a, b) {
    if (a.key < b.key) return 1
    else if (a.key > b.key) return -1
    return 0
}

function notify() {
    const messages = []
    for (const value of ALERTS.values()) {
        messages.push(value)
    }

    messages.sort(dataCompare)

    for (const func of SUBSCRIBER.values()) {
        func(messages)
    }
}

export function cancel(key) {
    if (ALERTS.has(key)) {
        ALERTS.delete(key)
        notify()
    }
}

export function alert(message, level, persistSeconds = 0) {
    if (!LEVEL.has(level)) return -1

    const key = new Date().getTime().toString()
    const data = {
        key,
        level,
        message,
    }

    ALERTS.set(key, data)

    if (persistSeconds !== 0) {
        setTimeout(
            () => cancel(key),
            persistSeconds * 1000)
    }
    notify()

    return key
}

const alertHelper = (level, message, seconds) => alert(message, level, seconds)
const timeAlertHelper = (seconds, level, message) => alert(message, level, seconds)

export const alertSuccess = alertHelper.bind(null, 'success')
export const alertWarning = alertHelper.bind(null, 'warning')
export const alertError = alertHelper.bind(null, 'error')
export const alertInfo = alertHelper.bind(null, 'info')

export const shortAlertSuccess = timeAlertHelper.bind(null, 5, 'success')
export const shortAlertWarning = timeAlertHelper.bind(null, 5, 'warning')
export const shortAlertError = timeAlertHelper.bind(null, 5, 'error')
export const shortAlertInfo = timeAlertHelper.bind(null, 5, 'info')

export const longAlertSuccess = timeAlertHelper.bind(null, 10, 'success')
export const longAlertWarning = timeAlertHelper.bind(null, 10, 'warning')
export const longAlertError = timeAlertHelper.bind(null, 10, 'error')
export const longAlertInfo = timeAlertHelper.bind(null, 10, 'info')
