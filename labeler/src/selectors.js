import { createSelector } from 'reselect'

import { bisectLeft } from './utils'

import { canvasWidth } from './config/layout.json'

export const nodeSelector = createSelector(
    state => state.data.imageSize.width,
    state => state.data.nodes,
    state => state.data.extraNodes,
    (width, ...nodesCollections) => {
        const ret = new Map()
        const ratio = canvasWidth / width
        for (const nodes of nodesCollections) {
            for (const [index, node] of nodes.entries()) {
                const { dim } = node
                ret.set(index, {
                    ...node,
                    dim: [
                        dim[0] * ratio,
                        dim[1] * ratio,
                        dim[2] * ratio,
                        dim[3] * ratio,
                    ],
                })
            }
        }

        return ret
    },
)

export const currentNodeSelector = createSelector(
    [
        nodeSelector,
        state => state.selection.current,
    ],
    (nodes, current) => (nodes.has(current) ? nodes.get(current) : null),
)

function nodeCompare(a, b) {
    if(a.dim[2] < b.dim[2]) return -1
    if(a.dim[2] === b.dim[2]) return a.dim[3] - b.dim[3]
    return 1
}

const numberCompare = (a, b) => a - b

export const boundarySelector = createSelector(
    nodeSelector,
    (nodes) => {
        let xAnchors = new Set()
        let yAnchors = new Set()
        const xNodes = []
        const yNodes = []
        for (const { dim } of nodes.values()) {
            xAnchors.add(dim[0])
            xAnchors.add(dim[0] + dim[2])
            yAnchors.add(dim[1])
            yAnchors.add(dim[1] + dim[3])
        }

        xAnchors = Array.from(xAnchors)
        xAnchors.sort(numberCompare)
        for (let i = 0; i <= xAnchors.length; ++i) {
            xNodes.push([])
        }

        yAnchors = Array.from(yAnchors)
        yAnchors.sort(numberCompare)
        for (let i = 0; i <= yAnchors.length; ++i) {
            yNodes.push([])
        }

        for (const node of nodes.values()) {
            const x1 = xAnchors.indexOf(node.dim[0])
            const x2 = xAnchors.indexOf(node.dim[0] + node.dim[2])
            const y1 = yAnchors.indexOf(node.dim[1])
            const y2 = yAnchors.indexOf(node.dim[1] + node.dim[3])

            for (let i = x1; i <= x2; ++i) xNodes[i].push(node)
            for (let i = y1; i <= y2; ++i) yNodes[i].push(node)
        }

        for (const nodeArr of xNodes) nodeArr.sort(nodeCompare)
        for (const nodeArr of yNodes) nodeArr.sort(nodeCompare)

        return {
            xNodes,
            yNodes,
            xAnchors,
            yAnchors,
        }
    },
)

export const boundaryGetNodeSelector = createSelector(
    boundarySelector,
    ({ xNodes, yNodes, xAnchors, yAnchors }) => (x, y) => {
        const xTargets = xNodes[bisectLeft(xAnchors, x) - 1]
        const yTargets = yNodes[bisectLeft(yAnchors, y) - 1]
        if (!xTargets || !yTargets) return []

        const targets = []
        // NOTE: Don't iterate over xTargets
        for (const ny of yTargets) {
            if (xTargets.indexOf(ny) !== -1) targets.push(ny)
        }

        return targets
    },
)
