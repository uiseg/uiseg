// A Command stores mappings from names to functions and provides
// auto-completions backend.
export default class Command {
    constructor(oldCommand) {
        if (oldCommand) {
            this.command = new Map(oldCommand.command)
        } else {
            this.command = new Map()
        }
    }

    register(cmd, func) {
        if (!cmd) {
            throw new Error('Command cannot be an empty string.')
        }

        this.command.set(cmd, func)
    }

    unregister(cmd) {
        this.command.delete(cmd)
    }

    complete(cmd) {
        if (!cmd) return ''

        let ret = ''

        for (const target of this.command.keys()) {
            if (target.startsWith(cmd)) {
                if (!ret) ret = target
                if (ret.length > target.length) ret = target
            }
        }

        return ret
    }

    get(cmd) {
        if (this.command.has(cmd)) {
            return this.command.get(cmd)
        }

        return this.command.get(this.complete(cmd))
    }

    has(cmd) {
        return this.command.has(cmd)
    }
}
