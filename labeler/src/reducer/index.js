import { combineReducers } from 'redux'

import dataReducer from './data'
import optionReducer from './options'
import selectionReducer from './selection'
import resultReducer from './results'
import modeReducer from './mode'

const rootReduer = combineReducers({
    data: dataReducer,
    options: optionReducer,
    selection: selectionReducer,
    results: resultReducer,
    mode: modeReducer,
})

export default rootReduer
