import { alertWarning, alertSuccess } from '../alert'
import { initialOptions } from '../config/config'

function parseValue(value) {
    try {
        const ret = JSON.parse(value)
        return ret
    } catch(e) {
        return value
    }
}

export default function optionReducer(options = initialOptions, action) {
    switch (action.type) {
        case 'TOGGLE_OPTION': {
            if (!(action.key in options)) {
                alertWarning(`Unknown option name "${action.key}"`, 5)
                return options
            } else if (typeof options[action.key] !== 'boolean') {
                alertWarning(`Option "${action.key}" is not a boolean value.`, 5)
                return options
            }

            const newOptions = { ...options }
            newOptions[action.key] = !newOptions[action.key]

            alertSuccess(`Option "${action.key}" is set to "${newOptions[action.key]}"`, 3)
            return newOptions
        }

        case 'UPDATE_OPTION': {
            if (!(action.key in options)) {
                alertWarning(`Unknown option name "${action.key}"`, 5)
                return options
            }

            let value = action.value
            if (value === undefined) {
                if (typeof options[action.key] === 'boolean') {
                    value = !options[action.key]
                } else {
                    alertWarning('Option is not a boolean value. Cannot omit value.', 5)
                    return options
                }
            } else if (typeof options[action.key] === 'string') {
                value = action.value
            } else {
                value = parseValue(action.value)
            }

            const newOptions = { ...options }
            newOptions[action.key] = value
            alertSuccess(`Option "${action.key}" is set to "${value}"`, 3)
            return newOptions
        }

        default:
            return options
    }
}
