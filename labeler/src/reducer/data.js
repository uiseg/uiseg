import { alertWarning } from '../alert'

const initialData = {
    url: '',
    hash: '',
    image: '',
    imageSize: {},
    realUrl: '',
    nodes: new Map(),
    extraNodes: new Map(),
}

export default function dataReducer(data = initialData, action) {
    switch (action.type) {
        case 'DATA_FETCH_SUCCEEDED':
            return {
                ...action.data,
                extraNodes: new Map(),
                fetching: false,
            }

        case 'DATA_FETCH_FAILED':
            return {
                ...data,
                fetching: false,
            }

        case 'DATA_FETCH_REQUESTED': {
            return {
                ...data,
                fetching: true,
            }
        }

        case 'DATA_SAVE_REQUESTED': {
            return {
                ...data,
                saving: true,
            }
        }

        case 'DATA_SAVE_SUCCEEDED':
            return {
                ...data,
                saving: false,
            }

        case 'DATA_SAVE_FAILED':
            return {
                ...data,
                saving: false,
            }

        case 'DATA_TRASH_REQUESTED': {
            return {
                ...data,
                trashing: true,
            }
        }

        case 'DATA_TRASH_SUCCEEDED':
            return {
                ...data,
                trashing: false,
            }

        case 'DATA_TRASH_FAILED':
            return {
                ...data,
                trashing: false,
            }

        case 'ADD_EXTRA_NODE': {
            if (action.index &&
                (data.nodes.has(action.index) ||
                data.extraNodes.has(action.index))) {
                alertWarning('Index exists in data', 3)
                return data
            }
            const index = action.index || Math.max(
                ...data.nodes.keys(),
                ...data.extraNodes.keys()) + 1

            const extraNodes = new Map(data.extraNodes)
            extraNodes.set(index, { ...action.node, index })

            return {
                ...data,
                extraNodes,
            }
        }

        case 'REMOVE_EXTRA_NODE': {
            if (!data.extraNodes.has(action.index)) return data

            const extraNodes = new Map(data.extraNodes)
            extraNodes.delete(action.index)

            return {
                ...data,
                extraNodes,
            }
        }

        default:
            return data
    }
}
