import {
    toggleSet,
} from '../utils'

const initialSelection = {
    current: -1,
    attributes: {},
}

export default function selectionReducer(selection = initialSelection, action) {
    switch (action.type) {
        case 'DATA_FETCH_REQUESTED':
        case 'DATA_FETCH_SUCCEEDED':
            return initialSelection

        case 'TOGGLE_SELECTION_LABEL':
            return {
                ...selection,
                labels: toggleSet(selection.labels, action.label),
            }

        case 'SELECT_NODE': {
            return {
                ...initialSelection,
                current: action.node,
            }
        }

        case 'UPDATE_SELECT_ATTR': {
            return {
                ...selection,
                attributes: {
                    ...selection.attributes,
                    ...action.func(selection.attributes, ...action.args),
                },
            }
        }

        case 'RESTART':
            return initialSelection

        default:
            return selection
    }
}
