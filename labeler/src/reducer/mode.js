import { initialMode } from '../config/config'

// Update mode data inplaced.
function updateData(mode, data) {
    const newData = mode[mode.mode] ?
        { ...mode[mode.mode], ...data } : data
    mode[mode.mode] = newData /* eslint no-param-reassign: "off" */

    return mode
}

export default function modeReducer(mode = initialMode, action) {
    switch (action.type) {
        case 'MODE_CHANGE': {
            const newMode = {
                ...mode,
                mode: action.mode,
            }

            if (action.data) {
                updateData(newMode, action.data)
            }

            return newMode
        }
        case 'MODE_UPDATE': {
            const newMode = { ...mode }

            updateData(newMode, action.data)

            return newMode
        }

        case 'DATA_FETCH_SUCCEEDED':
        case 'RESTART':
            return initialMode

        default:
            return mode
    }
}
