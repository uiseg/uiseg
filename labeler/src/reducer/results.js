const initialResult = {}

export default function resultReducer(results = initialResult, action) {
    switch (action.type) {
        case 'UPDATE_RESULT': {
            if (!action.func) return results

            return {
                ...results,
                ...action.func(results),
            }
        }

        case 'DATA_FETCH_SUCCEEDED':
        case 'RESTART':
            return initialResult

        default:
            return results
    }
}
