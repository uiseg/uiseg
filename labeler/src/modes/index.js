import NormalMode from './normal'
import ViewMode from './view'
import RunMode from './run'

// Anyone who wants to change the behavior of this app should modify the
// following defaults.

// All modes supported by this app
const modes = {
    normal: new NormalMode(),
    view: new ViewMode(),
    run: new RunMode(),
}

export default modes
