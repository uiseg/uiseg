import { createSelector } from 'reselect'
import Color from 'tinycolor2'

import BaseMode from './base'
import {
    nodeSelector,
    currentNodeSelector,
} from '../selectors'

import {
    selectNodeAction,
    modeUpdateAction,
    modeChangeAction,
} from '../actions'

import { colors } from '../config/color.json'

const currentStroke = Color(colors.orange).setAlpha(0.8).toString()
const childrenStroke = Color(colors.red).setAlpha(0.8).toString()
const backgroundMask = Color(colors.grey).setAlpha(0.5).toString()

const canvasLClickHandler = (state, dispatch, action) => {
    const newData = {}

    if (state.mode.normal.cursorDim) {
        newData.cursorDim = null
    }

    if (action.targets && action.targets.length > 0) {
        newData.clickLoc = [action.x, action.y]
        dispatch(selectNodeAction(action.targets[0].index))
    }

    if (Object.getOwnPropertyNames(newData).length !== 0) {
        dispatch(modeUpdateAction(newData))
    }
}

const canvasHighlightSelector = createSelector(
    [
        nodeSelector,
        currentNodeSelector,
    ],
    (nodes, current) => {
        const highlights = []

        if (current) {
            highlights.push({
                lineWidth: 4,
                fillStyle: 'clear',
                strokeStyle: currentStroke,
                bboxes: [current.dim],
            })

            if (!current.children) return highlights

            const childrenBboxes = []
            for (const index of current.children) {
                if (nodes.has(index)) {
                    childrenBboxes.push(nodes.get(index).dim)
                }
            }

            highlights.push({
                lineWidth: 2,
                fillStyle: 'clear',
                strokeStyle: childrenStroke,
                bboxes: childrenBboxes,
            })
        }
        return highlights
    },
)

const canvasBackgroundSelector = createSelector(
    [
        currentNodeSelector,
        state => state.options.masking,
        state => state.options.dark,
    ],
    (current, masking, dark) => {
        if (!current || !masking) return 'transparent'

        return dark ? 'black' : backgroundMask
    },
)


export default class NormalMode extends BaseMode {
    constructor() {
        super()
        this.command.register('run', (state, dispatch) => {
            dispatch(modeChangeAction('run'))
        })
        this.command.register('V', (state, dispatch) => {
            dispatch(modeChangeAction('view', 'normal'))
        })
        this.command.register('Escape', (state, dispatch) => {
            if (state.selection.current !== -1) dispatch(selectNodeAction(-1))

            const data = state.mode.normal
            if (data.clickLoc) {
                dispatch(modeUpdateAction({ clickLoc: undefined }))
            }
        })
    }

    mount(state, dispatch, clickLoc = undefined) {
        dispatch(modeUpdateAction({
            clickLoc,
        }))
    }

    handle(action, state, dispatch) {
        switch(action.type) {
            case 'UI_COMMAND_EXEC':
                return this.commandExec(action, state, dispatch)

            case 'UI_CANVAS_LCLICK':
                return canvasLClickHandler(state, dispatch, action)

            default:
                return action
        }
    }

    select(key, state) {
        switch(key) {
            case 'command':
                return this.command

            case 'canvasHighlight':
                return canvasHighlightSelector(state)

            case 'canvasBackground':
                return canvasBackgroundSelector(state)

            case 'canvasSelect':
                return true

            case 'tools':
                return ['NormalInfo', 'BoundaryList']

            case 'clickLoc':
                return state.mode.normal.clickLoc

            default:
                return undefined
        }
    }
}
