import { createSelector } from 'reselect'
import Color from 'tinycolor2'

import BaseMode from './base'

import {
    selectNodeAction,
    modeChangeAction,
    modeUpdateAction,
} from '../actions'

import {
    nodeSelector,
} from '../selectors'

import { colors } from '../config/color.json'

const backgroundMask = Color(colors.grey).setAlpha(0.4).toString()
const bboxesStroke = Color(colors.red).setAlpha(0.9).toString()

const canvasHighlightSelector = createSelector(
    nodeSelector,
    (nodes) => {
        const highlights = []

        const bboxes = []
        for (const node of nodes.values()) {
            bboxes.push(node.dim)
        }

        highlights.push({
            lineWidth: 2,
            strokeStyle: bboxesStroke,
            bboxes,
        })

        return highlights
    },
)

const canvasBackgroundSelector = createSelector(
    state => state.options.dark,
    dark => (dark ? 'black' : backgroundMask),
)


export default class ViewMode extends BaseMode {
    mount(state, dispatch) {
        dispatch(modeUpdateAction({
            prevMode: state.mode.mode,
        }))
    }

    handle(action, state, dispatch) {
        switch(action.type) {
            case 'UI_COMMAND_EXEC':
                dispatch(modeUpdateAction({ prevMode: undefined }))
                dispatch(modeChangeAction(state.mode.view.prevMode))
                return undefined

            case 'UI_CANVAS_LCLICK': {
                if (action.targets && action.targets.length > 0) {
                    dispatch(selectNodeAction(action.targets[0].index))
                    return modeChangeAction(state.mode.view.prevMode, [
                        action.x, action.y])
                }

                return undefined
            }

            default:
                break
        }

        return action
    }

    select(key, state) {
        switch(key) {
            case 'command':
                return this.command

            case 'canvasHighlight':
                return canvasHighlightSelector(state)

            case 'canvasBackground':
                return canvasBackgroundSelector(state)

            case 'canvasSelect':
                return true

            case 'commandDisplayText':
                return '(Press any key to exit)'

            case 'charCommand':
                return true

            case 'tools':
                return ['NormalInfo']

            default:
                return undefined
        }
    }
}
