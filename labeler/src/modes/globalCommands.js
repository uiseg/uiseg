import {
    restartAction,
    selectNodeAction,
    toggleSelectionLabelAction,
    toggleOptionAction,
    updateOptionAction,
} from '../actions'

import { alertWarning } from '../alert'

export const selectNodeCommand = (state, dispatch, index) => {
    if (!index) {
        alertWarning('No index provided', 3)
        return
    }
    if (Number.isNaN(+index)) {
        alertWarning('Index is not a number', 3)
        return
    }

    dispatch(selectNodeAction(+index))
}

export const toggleLabelByCommand = (cmd, state, dispatch) => {
    dispatch(toggleSelectionLabelAction(cmd))
}

export const finishCommand = () => {
    alertWarning('This command is not yet implemented.', 3)
}

export const restartCommand = (state, dispatch) => {
    dispatch(restartAction())
}

export const setOptionCommand = (state, dispatch, key, value) => {
    if(!key) {
        alertWarning('Option name is not specified.', 3)
        return
    }
    dispatch(updateOptionAction(key, value))
}

export const toggleOptionCommand = (state, dispatch, key) => {
    if(!key) {
        alertWarning('Option name is not specified.', 3)
        return
    } else if (typeof state.options[key] !== 'boolean') {
        alertWarning(`Option "${key}" is not a boolean value.`, 3)
        return
    }

    dispatch(toggleOptionAction(key))
}
