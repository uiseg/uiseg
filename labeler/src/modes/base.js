import Command from '../command'

import { shortAlertWarning } from '../alert'

import {
    finishCommand,
    restartCommand,
    setOptionCommand,
    toggleOptionCommand,
} from './globalCommands'

// A mode is a specific way to receive incoming user actions and respond with
// different UI modification. In the world of React and Redux, This is done by:
//   1. providing a middleware to receive UI-oriented actions and transform it
//      into data-oriented actions.
//   2. providing a root selector function that return a selector for each
//      input key.
//
// There are strong constraints under this semantic:
//   1. UIs (react components) no longer dispatch redux actions directly;
//      alternatively, they dispatch UI-oriented actions, which can only be
//      handled by mode middlewares.
//   2. There's a 1-to-1 mapping between states and UI representations.
//      A component renders data either from raw state (from redux store) or
//      selectors (provided by the current mode).
//   3. If a mode stores its internal state as class properties, and stores
//      its global state in the redux store. The global states can be updated
//      by dispatching MODE_UPDATE actions.
export default class BaseMode {
    constructor() {
        this.command = new Command()

        this.command.register('finish', finishCommand)
        this.command.register('set', setOptionCommand)
        this.command.register('toggle', toggleOptionCommand)
        this.command.register('restart', restartCommand)
    }

    commandExec(action, state, dispatch, command) {
        const { cmd, args } = action

        const func = command ? command.get(cmd) : this.command.get(cmd)
        if (func) {
            return func(state, dispatch, ...args || [])
        }

        shortAlertWarning(`Command "${cmd}" not found.`)
        return action
    }

    // init function is called when there's no [mode] key in the state. This
    // function should return an object representing the initial substate. Each
    // substate is maintained by the mode that created it.
    //
    // If a mode doesn't store its substate, this function should be
    // implemented as a no-op.
    mount(state, dispatch, ...args) {} // eslint-disable-line
    unmount(state, dispatch) {} // eslint-disable-line

    // handle function serves as a middleware: it is expected to receives an
    // UI action and transform the action into a reducer action. It can either
    // return the new action (to pass to the next middleware), or dispatch it
    // and return nothing.
    //
    // If it does not interfere with the action, it should
    // return the original action.
    handle(action, state, dispatch) { // eslint-disable-line
        throw new Error(`Nonimplemented "handle" function in ${this.constructor.name}`)
    }

    // select function serves as a re-selector: it selects selector based on
    // the input key and pass the state to the target selector and return the
    // results.
    //
    // It should return undefined if no selector is registered for the key.
    select(key, state) { // eslint-disable-line
        throw new Error(`Nonimplemented "select" function in ${this.constructor.name}`)
    }
}
