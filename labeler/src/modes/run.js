import { createSelector } from 'reselect'
import Color from 'tinycolor2'

import BaseMode from './base'
import {
    nodeSelector,
    currentNodeSelector,
} from '../selectors'

import {
    selectNodeAction,
    modeUpdateAction,
    modeChangeAction,
    updateResultAction,
    updateSelectionAttrAction,
} from '../actions'

import { alertWarning } from '../alert'

import { colors } from '../config/color.json'

import { sample, toggleSet, intersectSet } from '../utils'

const currentStroke = Color(colors.orange).setAlpha(0.8).toString()
const backgroundMask = Color(colors.grey).setAlpha(0.5).toString()

const expectSegmentEvaluationSet = new Set([
    'perfect',
    'good',
    'fair',
    'bad',
])
const expectClusterEvaluationSet = new Set([
    'perfect cluster',
    'missing children',
    'additional children',
    'bad cluster',
])
const saveNodesCommand = (state, dispatch) => {
    const current = currentNodeSelector(state)
    if (!current) {
        alertWarning('No node selected.', 3)
        return
    }

    const labels = state.selection.attributes.labels
    if (intersectSet(labels, expectSegmentEvaluationSet).size !== 1) {
        alertWarning('Expect exactly one segmentation evalutation value selected.', 3)
        return
    } else if (intersectSet(labels, expectClusterEvaluationSet).size !== 1) {
        alertWarning('Expect exactly one cluster evalutation value selected.', 3)
        return
    }

    dispatch(updateResultAction((results) => {
        const labeled = results.labeled || new Map()
        labeled.set(current, labels)
        return { labeled }
    }))

    let { index } = state.mode.run
    const { keys } = state.mode.run
    index++
    if (index < keys.length) {
        dispatch(selectNodeAction(keys[index]))
        dispatch(modeUpdateAction({ index }))
    } else {
        dispatch(selectNodeAction(-1))
        dispatch(modeUpdateAction({ index }))
        dispatch(modeChangeAction('normal'))
        alertWarning('All nodes labeled.', 3)
    }
}

const canvasHighlightSelector = createSelector(
    [
        currentNodeSelector,
    ],
    (current) => {
        const highlights = []

        if (current) {
            highlights.push({
                lineWidth: 4,
                fillStyle: 'clear',
                strokeStyle: currentStroke,
                bboxes: [current.dim],
            })
        }
        return highlights
    },
)

const canvasBackgroundSelector = createSelector(
    [
        currentNodeSelector,
        state => state.options.masking,
        state => state.options.dark,
    ],
    (current, masking, dark) => {
        if (!current || !masking) return 'transparent'

        return dark ? 'black' : backgroundMask
    },
)


export default class RunMode extends BaseMode {
    constructor() {
        super()
        this.command.register('cancel', (state, dispatch) => {
            dispatch(modeChangeAction('normal'))
        })
        this.command.register('Escape', (state, dispatch) => {
            if (state.selection.current !== -1) dispatch(selectNodeAction(-1))
            dispatch(modeChangeAction('normal'))
        })
        this.command.register('rerun', (state, dispatch, resampling = -1) => {
            const keys = resampling >= 0 ?
                this.sample(nodeSelector(state), resampling) : state.mode.run.keys
            const index = 0
            dispatch(modeUpdateAction({ keys, index }))
            dispatch(selectNodeAction(keys[index]))
        })

        this.command.register('Enter', saveNodesCommand)
    }

    handle(action, state, dispatch) {
        switch(action.type) {
            case 'UI_COMMAND_EXEC':
                return this.commandExec(action, state, dispatch)

            case 'UI_LABEL_BUTTON_CLICK':
                this.toggleLabel(dispatch, action.label)
                return undefined

            default:
                return action
        }
    }

    mount(state, dispatch, ratio = 0.2) {
        const nodes = nodeSelector(state)
        if (nodes.size === 0) {
            alertWarning('No node to run', 5)
            dispatch(modeChangeAction('normal'))
        } else if (state.mode.run) {
            const { keys, index } = state.mode.run
            if (index === keys.length) {
                alertWarning('It\'s done! Type "rerun" to restart', 5)
                return
            }

            dispatch(selectNodeAction(keys[index]))
        } else {
            const keys = this.sample(nodes, ratio)
            const index = 0

            dispatch(modeUpdateAction({ keys, index }))
            dispatch(selectNodeAction(keys[index]))
        }
    }

    sample(nodes, ratio) {
        const length = Math.floor(nodes.size * ratio)
        const candidates = []
        for (const [index, node] of nodes.entries()) {
            if (node.children && node.children.length) candidates.push(index)
        }
        return sample(candidates, length)
    }

    toggleLabel(dispatch, label) {
        dispatch(updateSelectionAttrAction(({ labels }) => ({
            labels: toggleSet(labels || new Set(), label),
        })))
    }

    select(key, state) {
        switch(key) {
            case 'command':
                return this.command

            case 'canvasHighlight':
                return canvasHighlightSelector(state)

            case 'canvasBackground':
                return canvasBackgroundSelector(state)

            case 'tools':
                return ['RunInfo', 'NormalInfo', 'BoundaryList', 'LabelList']

            default:
                return undefined
        }
    }
}
