import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import './ModeIndicator.sass'

const mapModeToColor = {
    normal: 'green',
    generalize: 'blue',
}

const mapStateToProps = state => ({
    mode: state.mode.mode,
    className: mapModeToColor[state.mode.mode],
})

const _ModeIndicator = props => <p className={`mode-indicator ${props.className}`}>{props.mode}</p>

_ModeIndicator.propTypes = {
    dispatch: PropTypes.func.isRequired,
    mode: PropTypes.string,
    className: PropTypes.string,
}

const ModeIndicator = connect(mapStateToProps)(_ModeIndicator)
export default ModeIndicator
