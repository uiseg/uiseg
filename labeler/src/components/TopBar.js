import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Menu, Icon, Input } from 'semantic-ui-react'

import {
    dataAction,
} from '../actions'

const mapStateToProps = state => ({
    saved: !state.results.labeled || state.results.labeled.size === 0,
    hash: state.data.hash,
    realUrl: state.data.realUrl || '',
})

class _TopBar extends React.PureComponent {
    constructor(props) {
        super(props)

        this.onSave = this.onSave.bind(this)
        this.onForward = this.onForward.bind(this)
        this.onTrash = this.onTrash.bind(this)
    }

    onSave() {
        function selector(state) {
            const hash = state.data.hash
            const nodes = []
            for (const nodeCollection of [state.data.nodes, state.data.extraNodes]) {
                for (const [index, { dim, parent }] of nodeCollection.entries()) {
                    const bbox = [dim[0], dim[1], dim[0] + dim[2], dim[1] + dim[3]]
                    nodes.push({ index, bbox, parent })
                }
            }

            const labels = []
            for (const [node, labelSet] of state.results.labeled.entries()) {
                labels.push({
                    index: node.index,
                    labels: Array.from(labelSet),
                })
            }

            const data = { nodes, labels }
            return { hash, data }
        }

        if (!this.props.saved) {
            this.props.dispatch(dataAction('save', 'requested', { selector }))
        }
    }

    onTrash() {
        this.props.dispatch(dataAction('trash', 'requested'))
    }

    onForward() {
        this.props.dispatch(dataAction('fetch', 'requested'))
    }

    render() {
        return (
            <Menu inverted attached='top'>
                <Menu.Item header style={{ width: '20%' }}>
                    {this.props.hash}
                </Menu.Item>
                <Menu.Item header style={{ flex: 1 }}>
                    <Input inverted transparent value={this.props.realUrl}>
                        <input style={{ textAlign: 'center' }} />
                    </Input>
                </Menu.Item>
                <Menu.Menu position='right'>
                    <Menu.Item
                        onClick={this.onTrash}>
                        <Icon name='trash'/>
                        Trash
                    </Menu.Item>
                    <Menu.Item
                        onClick={this.onForward}>
                        <Icon name='forward'/>
                            Forward
                    </Menu.Item>
                    <Menu.Item active={!this.props.saved} color='green'
                        onClick={this.props.saved ? undefined : this.onSave}>
                        <Icon name='save'/>
                        Save and Forward
                    </Menu.Item>
                </Menu.Menu>
            </Menu>
        )
    }
}

_TopBar.propTypes = {
    hash: PropTypes.string,
    realUrl: PropTypes.string,
    saved: PropTypes.bool,
    dispatch: PropTypes.func.isRequired,
}

const TopBar = connect(mapStateToProps)(_TopBar)
export default TopBar
