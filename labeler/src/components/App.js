import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Grid } from 'semantic-ui-react'

import TopBar from './TopBar'
import CommandPanel from './CommandPanel'
import ToolPanel from './ToolPanel'
import Canvas from './Canvas'
import ModeIndicator from './ModeIndicator'
import AlertPanel from './AlertPanel'

import './App.sass'

import {
    dataAction,
    modeChangeAction,
} from '../actions'

class _App extends React.PureComponent {
    componentDidMount() {
        this.props.dispatch(modeChangeAction('normal'))
        this.props.dispatch(dataAction('fetch', 'requested'))
    }

    componentWillUnmount() {
    }

    render() {
        return (
            <Grid>
                <Grid.Row id='app'>
                    <Grid.Column width={16}>
                        <TopBar />
                    </Grid.Column>
                    <CommandPanel />
                    <ToolPanel />
                    <Canvas />
                    <ModeIndicator />
                    <AlertPanel />
                </Grid.Row>
                <Grid.Row className='non-functionable-warning'>
                    <Grid.Column width={16}>
                        <p>
                            This application requires viewport width bigger than 1440px
                        </p>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}

_App.propTypes = {
    dispatch: PropTypes.func.isRequired,
}

const App = connect()(_App)
export default App
