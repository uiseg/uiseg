import React from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'

import './CommandPanel.sass'

import { commandExecAction } from './uiActions'
import { appSelector } from '../app'

const mapStateToProps = state => ({
    displayText: appSelector('commandDisplayText')(state),
    charCommand: appSelector('charCommand')(state),
    command: appSelector('command')(state),
})

class _CommandPanel extends React.PureComponent {
    constructor(props) {
        super(props)
        this.keyEventHandler = this.keyEventHandler.bind(this)
    }

    executeCommand() {
        if (this.text) {
            const splits = this.text.split('‧')
            this.props.dispatch(commandExecAction(...splits))
            this.clearCommand()
        }
    }

    clearCommand() {
        if (this.text) {
            this.text = ''
            this.rerender()
        }
    }

    completeCommand() {
        this.text += `${this.complete.innerText}‧`
        this.rerender()
    }

    appendChar(c) {
        if (c === ' ' && this.text.length === 0) return
        if (c === ' ') c = '‧' // eslint-disable-line

        this.text += c
        this.rerender()
    }

    deleteChar() {
        if (this.text) {
            this.text = this.text.slice(0, -1)
            this.rerender()
        }
    }

    getComplete() {
        if (!this.text || !this.props.command) return ''

        const cand = this.props.command.complete(this.text)
        return cand ? cand.substr(this.text.length) : ''
    }

    // Maintain its own state (because keyboard event must be fast...)
    keyEventHandler(e) {
        const { key, ctrlKey } = e
        if (ctrlKey) return

        if (key.length !== 1) {
            // Not a printable character
            switch (key) {
                case 'Space':
                    if (this.text) {
                        this.appendChar(' ')
                    } else {
                        this.props.dispatch(commandExecAction(key))
                    }
                    break

                case 'Enter':
                    if (this.text) {
                        this.executeCommand()
                    } else {
                        this.props.dispatch(commandExecAction(key))
                    }
                    break

                case 'Escape':
                    if (this.text) {
                        this.clearCommand()
                    } else {
                        this.props.dispatch(commandExecAction(key))
                    }
                    break

                case 'Backspace':
                    this.deleteChar()
                    break

                case 'Tab':
                    this.completeCommand()
                    break

                case 'Delete':
                    this.props.dispatch(commandExecAction(key))
                    break

                case 'ArrowUp':
                case 'ArrowDown':
                case 'ArrowLeft':
                case 'ArrowRight':
                    this.props.dispatch(commandExecAction(key))
                    break

                default:
                    // console.log('Unknown key', key)
                    return
            }
        } else if (this.props.charCommand) {
            this.props.dispatch(commandExecAction(key))
        } else if ((key >= '0' && key <= '9') || key.toLowerCase() !== key) {
            // Printable uppercase character or Numbers
            // Only considered as a command when it is the first character of
            // the command.
            if (this.text) {
                this.appendChar(key)
            } else {
                this.props.dispatch(commandExecAction(key))
            }
        } else {
            // Printable lowercase character, not numbers
            this.appendChar(key)
        }
        e.preventDefault()
    }

    componentWillUnmount() {
        this.text = ''
        delete this.node
        delete this.cmd
        delete this.complete
        window.removeEventListener('keydown', this.keyEventHandler)
    }

    componentDidMount() {
        this.text = ''
        this.node = ReactDOM.findDOMNode(this) // eslint-disable-line react/no-find-dom-node
        this.cmd = this.node.children[0]
        this.complete = this.node.children[1]
        window.addEventListener('keydown', this.keyEventHandler, false)
        this.rerender()
    }

    componentDidUpdate() {
        this.rerender()
    }

    rerender() {
        const displayText = this.props.displayText || ''
        const cmdText = displayText + this.text

        this.cmd.innerText = cmdText
        this.complete.innerText = this.getComplete()

        this.node.style.display = cmdText ? 'unset' : 'none'
    }

    render() {
        const className = this.props.className || ''
        return <div className={`${className} command-panel`}>
            <span className="command" />
            <span className="complete" />
        </div>
    }
}

_CommandPanel.propTypes = {
    // The default text to be shown. Optional.
    displayText: PropTypes.string,

    // Any valid character is a command. Optional.
    charCommand: PropTypes.bool,

    // the class name of the root DOM. Optional.
    className: PropTypes.string,

    // The command store, this should be provided by the mode middleware.
    // Optional.
    command: PropTypes.object,

    // The dispatch function provided by react-redux
    dispatch: PropTypes.func.isRequired,
}

const CommandPanel = connect(mapStateToProps)(_CommandPanel)
export default CommandPanel
