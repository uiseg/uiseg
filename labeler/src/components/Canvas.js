import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import {
    imageMarginTop,
    imageMarginBottom,
    topBarHeight,
    canvasWidth,
} from '../config/layout.json'

import {
    boundarySelector,
    boundaryGetNodeSelector,
} from '../selectors'

import { appSelector } from '../app'

import {
    canvasLeftClickAction,
    canvasRectSelectAction,
} from './uiActions'

import './Canvas.sass'

const mapStateToProps = state => ({
    image: state.data.image,
    imageHeight: state.data.imageSize.height,
    imageWidth: state.data.imageSize.width,
    highlights: appSelector('canvasHighlight')(state),
    background: appSelector('canvasBackground')(state),
    boundary: boundarySelector(state),
    getNode: boundaryGetNodeSelector(state),
    select: appSelector('canvasSelect')(state),
    rectDraw: appSelector('canvasRectDraw')(state),
})

// TODO: Mouse events ??

class _Canvas extends React.PureComponent {
    constructor(props) {
        super(props)
        this.onWheel = this.onWheel.bind(this)
        this.onResize = this.onResize.bind(this)
        this.onMouseMove = this.onMouseMove.bind(this)
        this.onMouseDown = this.onMouseDown.bind(this)
        this.onMouseUp = this.onMouseUp.bind(this)
        this.onMouseOut = this.onMouseOut.bind(this)
        this.onScroll = this.onScroll.bind(this)

        this.height = window.innerHeight - topBarHeight
        this.width = canvasWidth

        this.aniHandle = null
        this.cursorDim = null
        this.mousedownLoc = null
    }

    componentDidMount() {
        this.canvas = document.querySelector('#canvas')
        this.container = document.querySelector('#canvasContainer')

        const image = document.querySelector('#targetImage')
        image.ondragstart = () => false
        image.onload = this.componentDidMount.bind(this)

        this.container.addEventListener('wheel', this.onWheel)
        this.container.addEventListener('scroll', this.onScroll)
        window.addEventListener('resize', this.onResize)
        this.canvas.addEventListener('mouseout', this.onMouseOut)
        this.canvas.addEventListener('mousemove', this.onMouseMove)
        this.canvas.addEventListener('mousedown', this.onMouseDown)
        this.canvas.addEventListener('mouseup', this.onMouseUp)
        this.canvas.ondragstart = () => false

        this.scrollHeight = this.container.scrollHeight
        this.scrollTop = this.container.scrollTop
        this.height = window.innerHeight - topBarHeight
        this.width = canvasWidth
    }

    componentWillUnmount() {
        this.container.removeEventListener('wheel', this.onWheel)
        window.removeEventListener('resize', this.onResize)
        this.canvas.removeEventListener('mousemove', this.onMouseMove)
        this.canvas.removeEventListener('mousedown', this.onMouseDown)
        this.canvas.removeEventListener('mouseup', this.onMouseUp)
        delete this.canvas
        delete this.container
        delete this.cursorDim
    }

    componentDidUpdate() {
        this.height = window.innerHeight - topBarHeight
        this.scrollHeight = this.container.scrollHeight
        this.cursorDim = null
        this.draw()
    }

    onResize() {
        this.scrollHeight = this.container.scrollHeight
        this.height = window.innerHeight - topBarHeight
        this.canvas.height = this.height
        this.draw()
    }

    focus(y) {
        this.container.scrollTop = y
        this.scrollTop = y
    }

    onScroll(e) {
        this.scrollTop = this.container.scrollTop

        this.draw()
        e.preventDefault()
    }

    onWheel(e) {
        const container = this.container
        const deltaY = (e.deltaMode === 1) ? e.deltaY * 10 : e.deltaY
        let scrollTop = Math.max(0, this.scrollTop + deltaY)
        scrollTop = Math.min(scrollTop, this.scrollHeight - this.height)

        container.scrollTop = scrollTop
        this.scrollTop = scrollTop

        this.draw()
        e.preventDefault()
    }

    onMouseDown (e) {
        const { offsetX, offsetY } = e
        const x = offsetX
        const y = offsetY - imageMarginTop

        this.mousedownLoc = [x, y]
        if (this.props.rectDraw) this.cursorDim = [x, y + this.scrollTop, 0, 0]
    }

    onMouseUp (e) {
        if (!this.mousedownLoc) return

        const { button, offsetX, offsetY } = e
        const x = offsetX
        let y = offsetY - imageMarginTop

        // Make sure it is a left button click
        if (this.props.select &&
            x === this.mousedownLoc[0] &&
            y === this.mousedownLoc[1] &&
            button === 0) {
            y += this.scrollTop
            const targets = this.props.getNode(x, y)
            this.props.dispatch(canvasLeftClickAction(x, y, { targets }))

            this.cursorDim = null
            this.draw()
        } else if (this.props.rectDraw &&
            x !== this.mousedownLoc[0] &&
            y !== this.mousedownLoc[1]) {
            const dim = [
                this.mousedownLoc[0],
                this.mousedownLoc[1] + this.scrollTop,
                x - this.mousedownLoc[0],
                y - this.mousedownLoc[1],
            ]
            if (dim[2] < 0) {
                dim[0] += dim[2]
                dim[2] = -dim[2]
            }
            if (dim[3] < 0) {
                dim[1] += dim[3]
                dim[3] = -dim[3]
            }

            this.props.dispatch(canvasRectSelectAction(dim))

            this.cursorDim = null
            this.draw()
        }

        this.mousedownLoc = null
        e.preventDefault()
    }

    onMouseMove({ offsetX, offsetY }) {
        const x = offsetX
        const y = offsetY - imageMarginTop

        if (this.props.rectDraw && this.mousedownLoc) {
            this.cursorDim[2] = x - this.cursorDim[0]
            this.cursorDim[3] = y - this.cursorDim[1] + this.scrollTop
            this.draw()
        } else if (this.props.select) {
            const targets = this.props.getNode(x, y + this.scrollTop)
            if (targets.length !== 0) this.cursorDim = [...targets[0].dim]

            this.draw()
        }
    }

    onMouseOut() {
        if (this.props.select && !this.mousedownLoc) {
            this.cursorDim = null
            this.draw()
        }
    }

    getCanvasDim(dim) {
        return [dim[0], dim[1] - this.scrollTop + imageMarginTop, dim[2], dim[3]]
    }

    highlight(ctx, { lineWidth, fillStyle, strokeStyle, bboxes }) {
        ctx.save()
        ctx.lineWidth = lineWidth
        ctx.strokeStyle = strokeStyle
        if (fillStyle) {
            if (fillStyle === 'clear') {
                for (let bbox of bboxes) {
                    bbox = this.getCanvasDim(bbox)
                    ctx.clearRect(...bbox)
                    ctx.strokeRect(...bbox)
                }
            } else {
                ctx.fillStyle = fillStyle
                for (let bbox of bboxes) {
                    bbox = this.getCanvasDim(bbox)
                    ctx.fillRect(...bbox)
                    ctx.strokeRect(...bbox)
                }
            }
        } else {
            for (let bbox of bboxes) {
                bbox = this.getCanvasDim(bbox)
                ctx.strokeRect(...bbox)
            }
        }
        ctx.restore()
    }

    drawBackground(ctx, spec) {
        ctx.save()
        ctx.fillStyle = spec
        const pageTop = Math.max(0, imageMarginTop - this.scrollTop)
        const pageHeight = Math.min(
            this.props.imageHeight,
            this.height,
            this.scrollHeight - imageMarginBottom - this.scrollTop,
        )
        ctx.fillRect(0, pageTop, canvasWidth, pageHeight)
        ctx.restore()
    }

    drawBbox(ctx, { lineWidth, strokeStyle, bboxes }) {
        ctx.save()
        ctx.lineWidth = lineWidth
        ctx.strokeStyle = strokeStyle

        for (let bbox of bboxes) {
            bbox = this.getCanvasDim(bbox)
            ctx.strokeRect(...bbox)
        }
        ctx.restore()
    }

    animate(ctx, specs) {
        if (this.aniHandle) window.cancelAnimationFrame(this.aniHandle)

        const func = (t) => {
            for (const spec of specs) {
                for (const bbox of spec.bboxes) {
                    ctx.clearRect(...this.getCanvasDim(bbox))
                }
                spec.fillStyle.setAlpha((Math.sin((t % 2160 * Math.PI) / 2160) + 1) / 3)
                this.highlight(ctx, spec)
            }
            this.aniHandle = window.requestAnimationFrame(func)
        }

        this.aniHandle = window.requestAnimationFrame(func)
    }

    draw() {
        // The main draw function
        // Redraw on wheelEvent, resizeEvent, or state changes.
        const context = this.canvas.getContext('2d')
        context.clearRect(0, 0, this.width, this.height)

        const { background, highlights } = this.props

        if (highlights) {
            if (background) this.drawBackground(context, background)

            for (const spec of highlights) {
                if (spec.fillStyle) {
                    for (const bbox of spec.bboxes) {
                        context.clearRect(...this.getCanvasDim(bbox))
                    }
                }
            }

            for (const spec of highlights) {
                this.highlight(context, spec)
            }
        }

        if (this.cursorDim) {
            this.highlight(context, {
                fillStyle: 'rgba(255, 255, 255, 0.3)',
                bboxes: [this.cursorDim],
            })
        }
    }

    render() {
        return (
            <div id='canvasContainer'>
                <canvas id='canvas'
                    width={this.width} height={this.height} />
                <img
                    id='targetImage'
                    src={`data:image/jpeg;base64, ${this.props.image}`}
                    draggable="false" />
            </div>
        )
    }
}

_Canvas.propTypes = {
    image: PropTypes.string,
    imageHeight: PropTypes.number,
    imageWidth: PropTypes.number,
    background: PropTypes.string,
    highlights: PropTypes.arrayOf(PropTypes.object),
    boundary: PropTypes.object,
    getNode: PropTypes.func,
    select: PropTypes.bool,
    rectDraw: PropTypes.bool,
    dispatch: PropTypes.func.isRequired,
}

const Canvas = connect(mapStateToProps)(_Canvas)
export default Canvas
