import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import TOOLS from './tools'

import { appSelector } from '../app'

import './ToolPanel.sass'

const mapStateToProps = state => ({
    state: state,
    tools: appSelector('tools')(state),
})

class _ToolPanel extends React.PureComponent {
    render() {
        const state = this.props.state
        const dispatch = this.props.dispatch
        const className = this.props.className || ''
        const widgets = []

        const tools = this.props.tools || []

        for (const toolName of tools) {
            if (toolName in TOOLS) {
                widgets.push(TOOLS[toolName](state, dispatch))
            } else {
                console.error(`No widget named "${toolName}".`)
            }
        }

        return (<div className={`${className} tool-panel`}>
            { widgets }
        </div>)
    }
}

_ToolPanel.propTypes = {
    className: PropTypes.string,
    dispatch: PropTypes.func.isRequired,
    state: PropTypes.object.isRequired,
    tools: PropTypes.arrayOf(PropTypes.string),
}

const ToolPanel = connect(mapStateToProps)(_ToolPanel)
export default ToolPanel
