import React from 'react'
import { Button } from 'semantic-ui-react'

import { appSelector } from '../../app'

const classButtonClickAction = cls => ({
    type: 'UI_CLASS_BUTTON_CLICK',
    class: cls,
})

export default function ClassList(state, dispatch) {
    const selectedClasses = appSelector('classRule')(state)
    const current = appSelector('currentNode')(state)

    if (!current) return null

    const ret = []
    let i = 0
    for (const cls of current.classes) {
        i++
        const selected = selectedClasses.has(cls)

        const buttonClick = (e, { content }) => { // eslint-disable-line
            dispatch(classButtonClickAction(content))
            e.target.blur()
        }

        ret.push(<Button
            color='black'
            key={cls}
            circular
            label={i}
            content={cls}
            labelPosition='left'
            onClick={buttonClick}
            basic={!selected}>
        </Button>)
    }

    return <div className='class-list-panel list-panel' key='ClassList'>{ ret }</div>
}
