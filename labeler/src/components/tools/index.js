import LabelList from './LabelList'
import ClassList from './ClassList'
import NormalInfo from './NormalInfo'
import RunInfo from './RunInfo'
import BoundaryList from './BoundaryList'

const tools = {
    RunInfo,
    LabelList,
    ClassList,
    NormalInfo,
    BoundaryList,
}
export default tools
