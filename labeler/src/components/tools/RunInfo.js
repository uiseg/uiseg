/* eslint no-bitwise: 'off' */
import React from 'react'

import './RunInfo.sass'

export default function NormalInfo(state) {
    const runInfo = state.mode.run
    if (!runInfo) return null

    const { index, keys } = runInfo

    const content = (index === keys.length) ?
        'Finished' : `${index + 1}/${keys.length}`

    return <div
        key='RunInfo'
        className='list-panel runinfo-list-panel'>
        {content}
    </div>
}
