/* eslint no-bitwise: 'off' */
import React from 'react'

import {
    currentNodeSelector,
} from '../../selectors'

import { canvasWidth } from '../../config/layout.json'

import './NormalInfo.sass'

export default function NormalInfo(state) {
    const current = currentNodeSelector(state)
    if (!current) return undefined

    const header = current.name ?
        `${current.index}: ${current.name}` : `${current.index}`
    const ratio = state.data.imageSize.width / canvasWidth
    const dim = [...current.dim]
    for (let i = 0; i < 4; ++i) dim[i] = ~~(dim[i] * ratio)

    return <div
        key='NormalInfo'
        className='list-panel normalinfo-list-panel'>
        <p><b>{header}</b></p>
        <p>(x, y, w, h) = ({dim.join(', ')})</p>
    </div>
}
