/* eslint no-bitwise: 'off' */
import React from 'react'

import { appSelector } from '../../app'
import {
    boundaryGetNodeSelector,
    currentNodeSelector,
} from '../../selectors'

import { selectNodeAction } from '../../actions'

import './BoundaryList.sass'

function onClick(dispatch, index) {
    dispatch(selectNodeAction(index))
}

export default function BoundaryList(state, dispatch) {
    const current = currentNodeSelector(state)
    if (!current) return undefined

    const getNode = boundaryGetNodeSelector(state)

    const loc = appSelector('clickLoc')(state)
    if (!loc) return undefined

    const targets = getNode(loc[0], loc[1])
    if (targets.length === 0) return undefined

    const ret = []
    for (const node of targets.slice(0, 5)) {
        const header = node.name ? `${node.index}: ${node.name}` : `${node.index}`
        const dim = [...node.dim]
        for (let i = 0; i < 4; ++i) dim[i] = ~~dim[i]

        ret.push(<div
            key={node.index}
            className='node-info'
            onClick={onClick.bind(null, dispatch, node.index)}>
            <a><b>{header}</b></a>
            <p>(x, y, w, h) = ({dim.join(', ')})</p>
        </div>)
    }

    return <div
        key='BoundaryList' className='boundarylist-list-panel list-panel'>
        { ret }
    </div>
}
