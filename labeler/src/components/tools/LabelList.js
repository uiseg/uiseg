import React from 'react'
import { Button } from 'semantic-ui-react'

import { labels as LABELS } from '../../config/config'
import { mapTypeToColor } from '../../utils'

export const labelButtonClickAction = label => ({
    type: 'UI_LABEL_BUTTON_CLICK',
    label,
})

const emptySet = new Set()

export default function LabelList(state, dispatch) {
    const selectedLabels = state.selection.attributes.labels || emptySet

    const ret = []
    for (const label of LABELS) {
        const selected = selectedLabels.has(label)

        const buttonClick = (e, { content }) => { // eslint-disable-line
            dispatch(labelButtonClickAction(content))
            e.target.blur()
        }

        ret.push(<Button
            key={label}
            circular
            content={label}
            basic={!selected}
            onClick={buttonClick}
            color={mapTypeToColor(label)}>
        </Button>)
    }

    return <div className='label-list-panel list-panel' key='LabelList'>{ ret }</div>
}
