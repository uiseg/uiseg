import React from 'react'
import { Transition, List, Card } from 'semantic-ui-react'

import { subscribe, cancel } from '../alert'
import './AlertPanel.sass'

const COLOR_MAPPING = new Map([
    ['success', 'green'],
    ['warning', 'orange'],
    ['error', 'red'],
    ['info', 'teal'],
])

export default class AlertPanel extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            messages: [],
        }

        subscribe(this.update.bind(this))
    }

    update(messages) {
        this.setState({ messages })
    }

    onClick(key) {
        cancel(key)
    }

    render() {
        return <Transition.Group
            as={List}
            className='alert-panel'
        >
            {this.state.messages.map(item => (
                <List.Item key={item.key} as={Card}
                    color={COLOR_MAPPING.get(item.level) || 'teal'}
                    description={item.message}
                    onClick={this.onClick.bind(this, item.key)}
                />
            ))}
        </Transition.Group>
    }
}
