export const canvasLeftClickAction = (x, y, data) => ({
    type: 'UI_CANVAS_LCLICK',
    x,
    y,
    ...data,
})

export const canvasRightClickAction = (x, y, data) => ({
    type: 'UI_CANVAS_RCLICK',
    x,
    y,
    ...data,
})

export const canvasRectSelectAction = dim => ({
    type: 'UI_CANVAS_RECT_SELECT',
    dim,
})

export const commandExecAction = (cmd, ...args) => ({
    type: 'UI_COMMAND_EXEC',
    cmd,
    args,
})

export const labelButtonClickAction = label => ({
    type: 'UI_LABEL_BUTTON_CLICK',
    label,
})

export const classButtonClickAction = cls => ({
    type: 'UI_CLASS_BUTTON_CLICK',
    class: cls,
})
