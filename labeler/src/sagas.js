import { call, put, takeLatest, all, select } from 'redux-saga/effects'

import {
    dataAction,
} from './actions'
import {
    randomFetchData as _fetchData,
    saveData as _saveData,
    trashData as _trashData,
} from './api'

import { alertSuccess, alertError, alertInfo, cancel } from './alert'


function getImageDimensions(file) {
    return new Promise((resolved) => {
        const i = new Image()
        i.onload = function() {
            const width = i.naturalWidth
            const height = i.naturalHeight
            resolved({ width, height })
        }
        i.src = `data:image/jpeg;base64, ${file}`
    })
}


function* saveData(action) {
    const alertKey = alertInfo('Sending labels to server...')
    if (!action.selector) {
        cancel(alertKey)
        yield put(dataAction('save', 'failed',
            { message: 'Saving failed. No result selector in data action.' },
        ))
    }

    try {
        const { hash, data } = yield select(action.selector)
        const success = yield call(_saveData, hash, data)
        cancel(alertKey)
        if (success) {
            alertSuccess('Saving succeeded.', 5)
            yield put(dataAction('save', 'succeeded'))
            yield put(dataAction('fetch', 'requested'))
        } else {
            alertError('Saving failed. Server error.', 5)
            yield put(dataAction('save', 'failed',
                { message: 'Unknown server error.' },
            ))
        }
    } catch ({ message }) {
        cancel(alertKey)
        alertError(`Saving failed. ${message}.`, 5)
        yield put(dataAction('save', 'failed', { message }))
    }
}

function* trashData() {
    const hash = yield select(state => state.data.hash)
    const alertKey = alertInfo(`Trashing ${hash}...`)
    try {
        const success = yield call(_trashData, hash)
        cancel(alertKey)
        if (success) {
            alertSuccess('Trashing succeeded.', 5)
            yield put(dataAction('trash', 'succeeded'))
            yield put(dataAction('fetch', 'requested'))
        } else {
            alertError('Trashing failed. Server error.', 5)
            yield put(dataAction('trash', 'failed',
                { message: 'Unknown server error.' },
            ))
        }
    } catch ({ message }) {
        cancel(alertKey)
        alertError(`Trashing failed. ${message}.`, 5)
        yield put(dataAction('trash', 'failed', { message }))
    }
}

function* fetchData() {
    const alertKey = alertInfo('Fetching random data...')
    try {
        const { hash, image, error, bboxes } = yield call(_fetchData)

        if (error) {
            throw new Error(`${error}`)
        } else if (!hash) {
            throw new Error('No hash provided when fetching data')
        } else if (!image) {
            throw new Error(`No image provided when fetching ${hash}`)
        }

        const imageSize = yield getImageDimensions(image)

        const nodes = new Map()
        for (const node of bboxes) {
            if (node.name === 'body') continue

            if (node.classes) {
                node.classes = node.classes.map(cls => `.${cls}`)
            } else {
                node.classes = []
            }

            if (node.name) {
                node.classes.push(node.name)
            }

            if (node.id) {
                node.classes.push(`#${node.id}`)
                delete node.id
            }

            if (node.bbox) {
                const { bbox } = node
                node.dim = [bbox[0], bbox[1], bbox[2] - bbox[0], bbox[3] - bbox[1]]
                delete node.bbox
            }

            node.children = []

            nodes.set(node.index, node)
        }

        for (const [index, node] of nodes.entries()) {
            if (!nodes.has(node.parent)) continue

            nodes.get(node.parent).children.push(index)
        }

        const data = {
            hash,
            image,
            imageSize,
            nodes,
        }

        cancel(alertKey)
        alertSuccess(`Fetching data ${hash} succeeded`, 5)
        yield put(dataAction('fetch', 'succeeded', { data }))
    } catch ({ message }) {
        cancel(alertKey)
        alertError(`Fetching data failed. ${message}`)
        yield put(dataAction('fetch', 'failed', { message }))
    }
}

function* rootSaga() {
    yield all([
        takeLatest('DATA_FETCH_REQUESTED', fetchData),
        takeLatest('DATA_SAVE_REQUESTED', saveData),
        takeLatest('DATA_TRASH_REQUESTED', trashData),
    ])
}

export default rootSaga
