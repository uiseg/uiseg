export const initialMode = {
    mode: 'normal',
    normal: {
        clickLoc: null,
    },
}

export const initialOptions = {
    dark: false,
    masking: true,
    ignoreLabeled: true,
}

export const labels = new Set([
    'perfect',
    'good',
    'fair',
    'bad',
    'perfect cluster',
    'missing children',
    'additional children',
    'bad cluster',
])

export const saveUrlSelector = key => `save/${key}`
export const fetchUrlSelector = key => `data/${key}`
export const randomFetchUrlSelector = () => 'data'
export const trashDataUrlSelector = key => `delete/${key}`
