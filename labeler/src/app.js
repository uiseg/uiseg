import NormalMode from './modes/normal'
import ViewMode from './modes/view'
import RunMode from './modes/run'

const MODES = {
    normal: new NormalMode(),
    view: new ViewMode(),
    run: new RunMode(),
}

export const appMiddleware = store => next => (action) => {
    const state = store.getState()
    const modeName = state.mode.mode

    if (action.type === 'MODE_CHANGE') {
        if (!(action.mode in MODES)) return undefined

        if (MODES[modeName]) MODES[modeName].unmount(state, store.dispatch)
        const args = action.args || []
        next(action)
        MODES[action.mode].mount(state, store.dispatch, ...args)
        return undefined
    } else if (MODES[modeName]) {
        const newAction = MODES[modeName].handle(action, state, store.dispatch)
        return newAction ? next(newAction) : undefined
    }

    return next(action)
}


export const appSelector = key => (state) => {
    const { mode } = state
    if (mode.mode in MODES && MODES[mode.mode]) {
        return MODES[mode.mode].select(key, state)
    }

    console.warn(`There's no "${key}" selector in ${mode.mode} mode.`)
    return undefined
}
