/**
 * Data action, this will be proccessed by redux-saga
 * @param {('save'|'trash'|'fetch')} action possible action
 * @param {('requested'|'succeeded'|'failed')} status possible status
 * @param {Object} payload
 */
export const dataAction = (action, status, payload) => ({
    type: `DATA_${action.toUpperCase()}_${status.toUpperCase()}`,
    ...payload,
})

/**
 * Modify selection.attributes
 * @param {func} a function return the new attributes given the old attributes
 */
export const updateSelectionAttrAction = (func, ...args) => ({
    type: 'UPDATE_SELECT_ATTR',
    func,
    args,
})

export const commandExecuteAction = (command = '', ...args) => ({
    type: 'COMMAND_EXECUTE',
    command,
    args,
})

/**
 * Select a new node. Note that the associated attributes will be reset
 * @param {node} the index of node to be selected
 */
export const selectNodeAction = node => ({
    type: 'SELECT_NODE',
    node,
})

export const toggleOptionAction = key => ({
    type: 'TOGGLE_OPTION',
    key,
})

export const updateOptionAction = (key, value) => ({
    type: 'UPDATE_OPTION',
    key,
    value,
})

export const addExtraNodeAction = (node, index) => ({
    type: 'ADD_EXTRA_NODE',
    node,
    index,
})

export const removeExtraNodeAction = index => ({
    type: 'REMOVE_EXTRA_NODE',
    index,
})

export const modeChangeAction = (mode, ...args) => ({
    type: 'MODE_CHANGE',
    mode,
    args,
})

export const updateResultAction = (func, ...args) => ({
    type: 'UPDATE_RESULT',
    func,
    args,
})

export const modeUpdateAction = data => ({
    type: 'MODE_UPDATE',
    data,
})

export const restartAction = () => ({
    type: 'RESTART',
})
