# pylint: disable=unsubscriptable-object
import json
import random
from argparse import ArgumentParser

from utils.region import (
    Region,
    intersection,
    is_intersect,
    get_best_covered_region,
)
from cv.prepare_text import (
    figure_loader,
    region_loader,
    preprocess,
)
from utils.task import dispatch_task
import utils.dataset as dataset

PROCESS_TIMEOUT = 180
SIZE = (512, 512)


def proposal_gen(figure, regions, size):
    img_region = Region((0, 0), figure.size)
    remains = set(region for region in regions
                  if is_intersect(region, img_region))
    results = []

    while remains:
        target = remains.pop()
        proposal = get_best_covered_region(img_region, regions, target, size)
        if proposal is None:
            continue

        covered = set()
        for reg in regions:
            inter = intersection(proposal, reg)
            if inter is not None and\
                (inter.area / reg.area > 0.5 or inter.w > 2 * inter.h):
                # calculate transformed bbox
                inter.shift(-proposal.x1, -proposal.y1)
                covered.add(inter)

                # if it has not been the target, remove it from remains
                remains.discard(reg)

        if covered:
            results.append((proposal, covered))

    return results


def run(key):
    input_dir = dataset.raw
    output_dir = dataset.data

    figure = figure_loader(input_dir.path('png', key))
    nodes = region_loader(input_dir.path('json', key))

    results = proposal_gen(figure, nodes, SIZE)
    for idx, (proposal, objs) in enumerate(results):
        new_figure = figure.crop(proposal.bbox)
        objs = preprocess(new_figure, objs)

        ret = {
            'proposal_bbox': proposal.bbox,
            'bboxes': [obj.bbox for obj in objs],
        }

        new_figure.save(output_dir.path('png', key, idx))
        with open(output_dir.path('json', key, idx), 'w') as f:
            json.dump(ret, f)

    return key


def split_dataset():
    dataset.data.update()
    pages = set(dataset.data.keys)
    train_pages = set(random.sample(pages, int(len(pages) * 0.75)))
    val_pages = pages - train_pages

    with open(dataset['train.txt'], 'w') as f:
        f.write('\n'.join(dataset.data.crops(train_pages)))
    with open(dataset['val.txt'], 'w') as f:
        f.write('\n'.join(dataset.data.crops(val_pages)))


def main(args):
    args = parser.parse_args()
    print('Dataset directory:', args.dataset)

    dataset.init(args.dataset)
    dataset.mkdir('data')

    candidates = set(dataset.raw)
    print(f'Found {len(candidates)} webpage(s)')

    dispatch_task(run, candidates,
                  timeout=PROCESS_TIMEOUT)
    split_dataset()


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('dataset',
                        help='Dataset directory')

    main(parser.parse_args())
