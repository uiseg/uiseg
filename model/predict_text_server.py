from argparse import ArgumentParser
from functools import lru_cache

import torch
import bottle
import numpy as np
from bottle import Bottle

import utils.dataset2 as dataset
from faster_rcnn import FasterRCNNVGG16
from faster_rcnn.utils.config import Config
from faster_rcnn.dataset.utils import read_image
from faster_rcnn.dataset.webui_text_dataset import (
    preprocess as preprocess_image,
    get_scale,
)

SIZE = (512, 512)

@lru_cache(maxsize=10)
def get_image(key):
    if key in dataset.raw.png:
        path = dataset.raw.png[key]

    elif key in dataset.raw.jpg:
        path = dataset.raw.jpg[key]

    else:
        raise FileNotFoundError(f'"{key}" not found in {dataset.raw}')

    return read_image(path, color=True)


def model_loader(model_path):
    state_dict = torch.load(model_path)
    config = Config.from_dict(state_dict['config'])
    config.data.data_dir = str(dataset)
    config.model.load_path = model_path

    print(config)

    print('Loading model...')
    model = FasterRCNNVGG16(config)
    model.load_state_dict(state_dict['model'])
    model = model.cuda()

    return config, model


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('dataset', help='Dataset directory')
    parser.add_argument('model', help='Model path')
    args = parser.parse_args()

    print('Dataset directory:', args.dataset)
    dataset.init(args.dataset)
    config, model = model_loader(args.model)

    app = Bottle()

    @app.route('/<key>')
    def predict(key):
        info = bottle.request.query
        x, y = info.get('x'), info.get('y')
        try:
            x, y = int(x), int(y)
        except (TypeError, ValueError):
            print(f'Receive {info.dict}')
            bottle.abort(404)

        try:
            image = get_image(key)
        except FileNotFoundError:
            print(f'"{key}" not found in {dataset.raw}')
            bottle.abort(404)

        scale = get_scale(SIZE, config.data.min_size, config.data.max_size)
        image = image[:, y:y+SIZE[1], x:x+SIZE[0]].copy()
        image = preprocess_image(image, scale)
        image = torch.FloatTensor(image[np.newaxis, ...]).cuda()
        image = torch.autograd.Variable(image, volatile=True)

        bboxes, _, scores = model.predict(image, scale)
        bboxes = bboxes[0]
        scores = scores[0]

        bboxes = bboxes[scores > 0.7][:, [1, 0, 3, 2]] / scale
        scores = scores[scores > 0.7]

        return dict(bboxes=bboxes.tolist(), scores=scores.tolist(), x=x, y=y)

    bottle.run(app, host='0.0.0.0', port=8080)
