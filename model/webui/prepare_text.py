import sys
import json
from collections import defaultdict
from functools import reduce

from utils.figure import Figure
from utils.figure_toolkit import (
    detect_background_only,
    remove_foreground,
    draw_regions,
)
from utils.plane import Plane
from utils.region import Region, union, is_intersect


def figure_loader(path):
    return Figure.load(path)


def region_loader(path):
    with open(path) as f:
        regions = json.load(f)
        # regions = set(Region.from_dim(d['dim']) for d in regions)
        try:
            regions = set(Region.from_dim(d['dim'])
                          for d in regions if d['name'] == '#text')
        except TypeError as e:
            print(e, path)
        # regions = set(Region(d['x1'], d['y1'], d['x2'], d['y2']) for d in regions)

    return regions


def clean_texts(fig, plane):
    removed = set()
    for region in plane.regions:
        if region.h < 6:
            remove_foreground(fig, region)
            removed.add(region)

        elif region.h > region.w and region.w < 20:
            remove_foreground(fig, region)
            removed.add(region)

        elif detect_background_only(fig, region):
            removed.add(region)

    plane.remove_regions(removed)


def horizontal_join_text_regions(plane):
    while True:
        removed = set()
        for region in plane:
            for candidate in plane.intersect(region):
                if (candidate.h != region.h or
                        candidate.y1 != region.y1):
                    continue

                if candidate.x1 <= region.x2 and candidate.x1 >= region.x1:
                    if candidate.x2 > region.x2:
                        plane.update_region(region, x2=candidate.x2)
                    removed.add(candidate)

            if removed:
                break

        if removed:
            plane.remove_regions(removed)
        else:
            break


def vertical_join_text_regions(plane):
    join_table = defaultdict(set)

    def join_group(a, b):
        if b not in join_table:
            return

        join_table[a] |= join_table[b]
        for element in join_table[b]:
            join_table[element] = join_table[a]

    processed = set()
    for region in plane.regions:
        join_table[region].add(region)

    for region in plane:
        if region in processed:
            continue

        processed.add(region)

        x1, y1, x2, y2 = region.bbox
        h = region.h

        y = y1
        distance = 1.5
        while True:
            y -= distance * h
            for candidate in plane[x1:x2, y:y+h]:
                if (candidate.h == region.h and
                        region.y1 - candidate.y2 <= distance * h):
                    join_group(region, candidate)
            else:
                break

        y = y2
        while True:
            y += distance * h
            for candidate in plane[x1:x2, y:y+h]:
                if (candidate.h == region.h and
                        candidate.y1 - region.y2 <= distance * h):
                    join_group(region, candidate)
            else:
                break

    ret = []
    while join_table:
        key, join_set = join_table.popitem()

        join_region = reduce(union, join_set, key)
        ret.append(join_region)

        for item in join_set:
            if item in join_table:
                del join_table[item]

    return ret


def join_close_text_regions(plane):
    unprocessed = set(plane)

    def distance(a, b):
        ax, ay = a.center
        bx, by = b.center
        return max(abs(ax - bx) - (a.w + b.w) / 2,
                   abs(ay - by) - (a.h + b.h) / 2)

    while True:
        join_set = set()
        for region in unprocessed:
            x1, y1, x2, y2 = region.bbox

            for candidate in plane[x1-10:x2+10, y1-10:y2+10]:
                if candidate is region or candidate in join_set:
                    continue

                if distance(region, candidate) < 10 or is_intersect(region, candidate):
                    join_set.add(candidate)

            if join_set:
                break

        unprocessed.discard(region)
        if join_set:
            join_region = reduce(union, join_set, region)
            plane.update_region(region, *join_region.bbox)
            plane.remove_regions(join_set)
            unprocessed -= join_set
        else:
            break


def preprocess(fig, regions):
    plane = Plane(regions)

    clean_texts(fig, plane)

    horizontal_join_text_regions(plane)

    join_regions = vertical_join_text_regions(plane)
    plane = Plane(join_regions)

    join_close_text_regions(plane)

    removed = set(reg for reg in plane.regions if reg.area == 0)
    plane.remove_regions(removed)

    return plane.regions


vis = None

def debug_show(fig, plane, name):
    if vis is None:
        return

    fig = Figure(fig.image)
    draw_regions(fig, plane.regions)
    vis.figure(name, fig)


if __name__ == '__main__':
    # from utils.visualizer import Visualizer
    # vis = Visualizer(env='prepare_text_dataset', server='http://localhost')

    def main():
        *name, ext = sys.argv[1].split('.')
        name = '.'.join(name)
        image_path = f'{name}.{ext}'
        json_path = f'{name}.json'

        fig = figure_loader(image_path)
        regions = region_loader(json_path)

        regions = preprocess(fig, regions)

        if vis is not None:
            draw_regions(fig, regions)
            fig.show(async_show=True)

    main()
