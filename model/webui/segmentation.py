from math import floor, ceil
from functools import reduce
from collections import defaultdict

import cv2
import numpy as np

import utils.debugger as debugger
from utils.region import Region, cover_ratio
from utils.line import Line, LineH, LineV
from utils.plane import Plane


SIZE = (512, 512)
CANNY_MINIMUM = 50
CANNY_MAXIMUM = 150
HORIZONTAL_LINE_MINIMUM = 150
VERTICAL_LINE_MINIMUM = 150
CONTOUR_TEXT_RATIO = 0.6
INVALID_LINE_COVER_RATIO = 0.7
INVALID_LINE_DIFF_DISTANCE = 12


class Contour(Region):
    __slots__ = ('children', )

    def __init__(self, x1, y1, x2=None, y2=None):
        super().__init__(x1, y1, x2, y2)
        self.children = []


def morph(image, shape, size=None):
    if isinstance(shape, str):
        shape = getattr(cv2, shape)
        kernel = cv2.getStructuringElement(shape, size)
    else:
        kernel = shape

    image = cv2.erode(image, kernel, iterations=1)
    image = cv2.dilate(image, kernel, iterations=1)
    return image


def get_lines(edges, text_plane):
    kernel = np.ones((3, 3), dtype=np.uint8)
    kernel[::2, ::2] = 0
    dilated_line = cv2.dilate(edges, kernel, iterations=1)
    # debugger.figure('Dilated lines', dilated_line)

    horizontal_lines = morph(
        dilated_line, 'MORPH_RECT', (HORIZONTAL_LINE_MINIMUM, 2))
    vertical_lines = morph(
        dilated_line, 'MORPH_RECT', (2, VERTICAL_LINE_MINIMUM))

    _1, h_contours, _2 = cv2.findContours(horizontal_lines,
                                          cv2.RETR_LIST,
                                          cv2.CHAIN_APPROX_SIMPLE)
    _1, v_contours, _2 = cv2.findContours(vertical_lines,
                                          cv2.RETR_LIST,
                                          cv2.CHAIN_APPROX_SIMPLE)

    kernel = np.ones((3, 3), dtype=np.uint8)
    horizontal_lines = cv2.dilate(horizontal_lines, kernel, iterations=1)
    vertical_lines = cv2.dilate(vertical_lines, kernel, iterations=1)

    h_lines = set()
    for c in h_contours:
        c = c.reshape(-1, 2)
        x1 = floor(c[:, 0].min())
        x2 = ceil(c[:, 0].max())
        y1 = c[:, 1].min()
        y2 = c[:, 1].max()
        y = int((y1 + y2) / 2)

        line = Region(x1, y1, x2, y2)
        if any(cover_ratio(reg, line) > 0.7
               for reg in text_plane.intersect(line)):
            horizontal_lines[y1:y2, x1:x2] = 0
            continue

        h_lines.add(LineH(y, x1, x2))

    v_lines = set()
    for c in v_contours:
        c = c.reshape(-1, 2)
        y1 = floor(c[:, 1].min())
        y2 = ceil(c[:, 1].max())
        x1 = c[:, 0].min()
        x2 = c[:, 0].max()
        x = int((x1 + x2) / 2)

        line = Region(x1, y1, x2, y2)
        if any(cover_ratio(reg, line) > 0.7
               for reg in text_plane.intersect(line)):
            vertical_lines[y1:y2, x1:x2] = 0
            continue

        v_lines.add(LineV(x, y1, y2))

    no_lines = edges - (horizontal_lines + vertical_lines)
    no_lines[no_lines != 255] = 0
    # debugger.figure('H lines', horizontal_lines)
    # debugger.figure('V lines', vertical_lines)
    # debugger.figure('no lines', no_lines)

    return (h_lines, v_lines), no_lines


def get_contours(no_lines):
    kernel = np.ones((3, 3), dtype=np.uint8)
    no_lines = cv2.dilate(no_lines, kernel, iterations=1)
    _, contours, hierarchies = cv2.findContours(no_lines,
                                                cv2.RETR_TREE,
                                                cv2.CHAIN_APPROX_SIMPLE)
    if not contours:
        return set()

    bboxes = {}
    for i, c in enumerate(contours):
        c = c.reshape(-1, 2)
        x1 = c[:, 0].min()
        x2 = c[:, 0].max()
        y1 = c[:, 1].min()
        y2 = c[:, 1].max()

        c = Contour(x1, y1, x2, y2)
        bboxes[i] = c

    hierarchies = hierarchies.reshape(-1, 4)
    for i, h in enumerate(hierarchies[:, 3]):
        if h in bboxes:
            bboxes[h].children.append(i)

    ignored = set()
    for i, c in bboxes.items():
        if (c.area < 40 or c.w < 5) and c.h < 10:
            ignored.add(i)
        if c.children:
            ignored.update(c.children)
    bboxes = set(Region.from_bbox(c.bbox)
                 for i, c in bboxes.items()
                 if i not in ignored)

    plane = Plane(bboxes)
    plane.merge_overlapped()

    return plane


def refine_boxes(text_plane, contour_plane, lines):
    line_pairs = defaultdict(set)
    for line in lines:
        x1, y1, x2, y2 = line.bbox
        d = 3
        nears = (contour_plane.covered_regions(x1-d, y1-d, x1+d, y1+d) |
                 contour_plane.covered_regions(x2-d, y1-d, x2+d, y1+d))
        if len(nears) == 2:
            nears = list(nears)
            nears.sort(key=lambda x: x.y1+x.x1)
            line_pairs[tuple(nears)].add(line)

    removed_texts = set()
    for (reg1, reg2), lines in line_pairs.items():
        if len(lines) != 2:
            continue
        line1, line2 = lines

        contour_plane.remove_region(reg2)
        lines.remove(line1)
        lines.remove(line2)

        reg2.iunion(reg1)\
            .iunion(Region.from_bbox(line1.bbox))\
            .iunion(Region.from_bbox(line2.bbox))
        contour_plane.update_region(reg1, *reg2.bbox)

        for text in text_plane.intersect(reg2):
            if cover_ratio(reg2, text) > 0.7:
                removed_texts.add(text)
    text_plane.remove_regions(removed_texts)

    ignored = set()
    for c in contour_plane:
        if c.area < 40 or c.w < 5 or c.h < 10:
            ignored.add(c)
    contour_plane.remove_regions(ignored)
    contour_plane.merge_overlapped()


def refine_text_regions(text_plane, contour_plane):
    processed = set()
    while True:
        covered = list()
        for text in text_plane:
            if text in processed:
                continue
            processed.add(text)

            for target in contour_plane.intersect(text):
                intersect = text.intersection(target)
                if (intersect is not None and
                        intersect.area / target.area > CONTOUR_TEXT_RATIO and
                        text.area > target.area):
                    covered.append(target)

            if covered:
                break

        if covered:
            u = reduce(Region.iunion, covered, covered[0].copy())
            text_plane.update_region(text, *u.bbox)
            contour_plane.remove_regions(covered)
        else:
            break


def remove_invalid_lines(plane, lines):
    removed = set()
    for line in lines:
        if line.anchor == 0 or line.anchor == SIZE[1] - 1:
            removed.add(line)
            continue

        line_region = Region.from_bbox(line.bbox)
        intersection = plane.intersect(line_region)
        if any(cover_ratio(reg, line_region) > INVALID_LINE_COVER_RATIO
               for reg in intersection):
            removed.add(line)
            for reg in intersection:
                plane.update_region(
                    reg, *Region.union(reg, line_region).bbox)
            continue

        for region in plane.nearest(line_region, k=2):
            if line.axis is Line.H:
                start, end = region.x1, region.x2
                # distance = abs(abs(region.center[1] - line.anchor) - (region.h / 2))
            else:
                start, end = region.y1, region.y2
                # distance = abs(abs(region.center[0] - line.anchor) - (region.w / 2))

            start = abs(start - line.start)
            end = abs(end - line.end)
            # if distance < 4:
            #     print(f'{distance} between {line} and {region}: {start}, {end}')

            if start + end < INVALID_LINE_DIFF_DISTANCE:
                # print(f'Remove {line} by checking {region}')
                removed.add(line)
                break

    lines -= removed


def auto_canny(image, sigma=0.5):
    # compute the median of the single channel pixel intensities
    v = np.median(image)

    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)

    # return the edged image
    return edged


def segment(fig, texts):
    text_plane = Plane(texts)
    text_plane.merge_overlapped()

    # 1. Do edge detection using grayscale image
    image = cv2.cvtColor(fig.image, cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(image, CANNY_MINIMUM, CANNY_MAXIMUM)

    # 2. Get lines and contours from edges and texts
    (h_lines, v_lines), no_lines = get_lines(edges, text_plane)

    # 3. Get contour_plane from no_lines and refine them with lines
    contour_plane = get_contours(no_lines)
    refine_boxes(text_plane, contour_plane, h_lines)
    refine_boxes(text_plane, contour_plane, v_lines)
    remove_invalid_lines(contour_plane, h_lines)
    remove_invalid_lines(contour_plane, v_lines)

    # 4. Identify those contours bounded by text regions and refine these
    #    text regions.
    refine_text_regions(text_plane, contour_plane)

    return (h_lines, v_lines), set(text_plane.regions), set(contour_plane.regions)
