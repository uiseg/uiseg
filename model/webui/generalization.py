from itertools import chain

import numpy as np
import matplotlib.pyplot as plt

from utils.region import Region, union
from utils.plane import Plane
import utils.debugger as debugger


W_H_RATIO = 3
NEARST_RANGE = list(range(1, 4))
NEARST_THRESHOLD = list(range(100, 800, 200))
LINE_THRESHOLD_RANGE = list(range(25, 150, 25))


def distance(reg1, reg2):
    xc1, yc1 = reg1.center
    xc2, yc2 = reg2.center

    w = (abs(xc1-xc2)-(reg1.w+reg2.w)/2) / W_H_RATIO
    h = (abs(yc1-yc2)-(reg1.h+reg2.h)/2)

    return max(w, h)


def width(reg1, reg2):
    xc1, yc1 = reg1.center
    xc2, yc2 = reg2.center

    w = (abs(xc1-xc2)-(reg1.w+reg2.w)/2)
    h = (abs(yc1-yc2)-(reg1.h+reg2.h)/2)

    if w > h:
        return w

    return 0


def line_detection(start, end, line_plane, threshold=0):
    region = Region(*start.center, *end.center)
    for line_region in line_plane.intersect(region):
        if line_region.w > line_region.h:
            if line_region.w > threshold:   # LineH
                return True
        else:
            if line_region.h > threshold:   # LineV
                return True

    return False


def generalize_close(region_plane, line_plane, nearest, threshold, d):
    while True:
        update_pair = None
        for reg in region_plane:
            for target in region_plane.nearest(reg, k=nearest):
                if distance(reg, target) > d:
                    continue

                if not line_detection(reg, target, line_plane, threshold):
                    update_pair = reg, target
                    break

            if update_pair:
                break

        if update_pair:
            # print(update_pair)
            reg, target = update_pair
            new_region = union(reg, target)
            region_plane.remove_region(target)
            region_plane.update_region(reg, *new_region.bbox)
        else:
            break

    return region_plane.regions


def distance_analysis(region_plane, line_plane):
    results = []
    for reg in region_plane:
        for x in region_plane.nearest(reg, k=4):
            results.append(
                (line_detection(reg, x, line_plane), distance(reg, x)))

    return results


def width_analysis(region_plane, line_plane):
    results = []
    for reg in region_plane:
        for x in region_plane.nearest(reg, k=4):
            w = width(reg, x)
            if w != 0:
                results.append(
                    (line_detection(reg, x, line_plane), w))

    return results


def draw_histogram(name, region_plane, line_plane):
    distances = distance_analysis(region_plane, line_plane)
    plot, (ax1, ax2) = plt.subplots(nrows=2, figsize=(20, 4))

    t = [r[1] for r in distances if r[0]]
    f = [r[1] for r in distances if not r[0]]
    ax1.hist(f, bins=50, range=(0, 100))
    ax2.hist(t, bins=50, range=(0, 100))

    if debugger.on:
        debugger.vis.pyplot(name, plot)

    plt.close(plot)


x = 2
def get_threshold(distances):
    global x
    mean, std = np.mean(distances), np.std(distances)
    th = mean - 1 / x * std
    x += 1
    if th < 0:
        return mean

    return th


def generalize(texts, regions, lines):
    h_lines, v_lines = lines
    regions = set(reg.copy() for reg in chain(regions, texts))

    line_regions = set()
    for line in chain(h_lines, v_lines):
        line = Region.from_bbox(line.bbox)
        line_regions.add(line)

    region_plane = Plane(regions)
    region_plane.merge_overlapped()
    line_plane = Plane(line_regions)

    results = set()
    draw_histogram('0', region_plane, line_plane)

    distances = distance_analysis(region_plane, line_plane)
    distances = [r for b, r in distances if not b]
    th = get_threshold(distances)
    print(th)
    results.update(reg.bbox
                   for reg in generalize_close(region_plane, line_plane, 4, 0, th))

    draw_histogram('1', region_plane, line_plane)

    distances = distance_analysis(region_plane, line_plane)
    distances = [r for b, r in distances if not b]
    th = get_threshold(distances)
    print(th)
    results.update(reg.bbox
                   for reg in generalize_close(region_plane, line_plane, 4, 0, th))

    draw_histogram('2', region_plane, line_plane)

    distances = distance_analysis(region_plane, line_plane)
    distances = [r for b, r in distances if not b]
    th = get_threshold(distances)
    print(th)
    results.update(reg.bbox
                   for reg in generalize_close(region_plane, line_plane, 4, 0, th))

    draw_histogram('3', region_plane, line_plane)

    distances = distance_analysis(region_plane, line_plane)
    distances = [r for b, r in distances if not b]
    th = get_threshold(distances)
    print(th)
    results.update(reg.bbox
                   for reg in generalize_close(region_plane, line_plane, 4, 0, th))

    draw_histogram('4', region_plane, line_plane)
    return set(Region.from_bbox(bbox) for bbox in results)
