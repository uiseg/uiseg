import math
from itertools import product, chain
from functools import reduce, partial
from collections import defaultdict

import numpy as np

from utils.region import Region, cover_ratio
from utils.plane import Plane
from utils.line import Line
import utils.debugger as debugger


W_H_RATIO = 3


times = 0
def debug_stub(dm, th):
    from matplotlib import pyplot as plt
    global times

    if debugger.on:
        distances = [min(d, 1000) for d in dm._cache.values()]

        distances.sort()
        fig, ax = plt.subplots()
        numbers = list(range(len(distances)-1))
        distances = np.array(distances)
        distances = distances[1:] - distances[:-1]
        ax.plot(numbers, distances)

        debugger.vis.pyplot(f'{times} {th}', fig)
        times += 1

        plt.close(fig)


def test_mcl(dm):
    import markov_clustering as mc
    order, matrix = dm.to_csr_matrix()
    result = mc.run_mcl(matrix, inflation=1.01)
    clusters = mc.get_clusters(result)
    clusters = [Cluster(set(order[i] for i in c)) for c in clusters]
    return clusters


def reject_outliers_2(data, m=2.):
    d = np.abs(data - np.median(data))
    mdev = np.median(d)
    s = d/(mdev if mdev else 1.)
    return data[s < m]


def _not_implemented(cls):
    raise NotImplementedError


class Cluster(Region):
    __slots__ = ('set', )

    def __init__(self, regions):
        if not regions:
            raise ValueError('Cluster should have at least one element.')

        self.set = set(regions)
        super().__init__(reduce(Region.__or__, self.set))

    def add(self, reg):
        self.set.add(reg)
        super().__ior__(reg)

    def discard(self, reg):
        self.set.discard(reg)
        super().__init__(reduce(Region.__or__, self.set))

    copy = _not_implemented

    shift = _not_implemented

    from_bbox = _not_implemented

    from_dim = _not_implemented

    def __len__(self):
        return len(self.set)

    def __contains__(self, reg):
        return reg in self.set

    def __iter__(self):
        return iter(self.set)

    def __str__(self):
        bbox = f'({self.x1:.2f}, {self.y1:.2f}, {self.x2:.2f}, {self.y2:.2f})'
        cls_name = self.__class__.__name__
        return f'{cls_name} [bbox={bbox}, {len(self.set)} element(s)]'

    def __repr__(self):
        cls_name = self.__class__.__name__
        return f'{cls_name}({self.set})'


class DistanceManager:
    def __init__(self, distance_func, regions=None):
        self._elements = set()
        self._cache = {}
        self.distance_func = distance_func

        if regions is not None:
            self.add(regions)

    def __call__(self, m, n):
        if id(m) < id(n):
            m, n = n, m
        return self._cache.get((m, n), math.inf)

    def remove(self, regions):
        self._elements -= set(regions)
        self._cache = {(m, n): d for (m, n), d in self._cache.items()
                       if m not in regions and n not in regions}

    def add(self, regions):
        for m, n in product(regions, self._elements | regions):
            if id(m) < id(n):
                m, n = n, m
            elif id(m) == id(n):
                continue

            d = self.distance_func(m, n)
            if math.isfinite(d):
                self._cache[(m, n)] = d

        # debug_stub(list(self._cache.values()))

        self._elements.update(regions)

    def update(self):
        self.add(self._elements)

    def below(self, th):
        return set((m, n) for (m, n), d in self._cache.items() if d <= th)

    def above(self, th):
        return set((m, n) for (m, n), d in self._cache.items() if d >= th)

    def nearest(self, m, k=1):
        distances = [(x if y is m else y, d) for (x, y), d in self._cache.items()
                     if x is m or y is m]

        if k == 1:
            if not distances:
                return None
            return max(distances, key=lambda item: item[1])

        distances.sort(key=lambda item: item[1])
        return distances[:k]

    def min(self, k=1):
        if k == 1:
            return min(self._cache.values())

        distances = list(self._cache.values())
        distances.sort()
        return distances[:k]

    def to_csr_matrix(self):
        from scipy.sparse import csr_matrix
        order = list(self._elements)
        elements = {e: i for i, e in enumerate(order)}

        cache = {(m, n): d for (m, n), d in self._cache.items()
                 if math.isfinite(d) and d < 3000}

        max_d = max(d for d in cache.values())

        length = len(cache)
        data = np.zeros(2 * length, dtype=np.int64)
        row_ind = np.zeros(2 * length, dtype=np.int64)
        col_ind = np.zeros(2 * length, dtype=np.int64)

        for k, ((m, n), d) in enumerate(cache.items()):
            m = elements[m]
            n = elements[n]
            data[k] = max_d / d if d != 0 else 0
            row_ind[k] = m
            col_ind[k] = n
            row_ind[k + length] = n
            col_ind[k + length] = m

        matrix = csr_matrix((data, (row_ind, col_ind)),
                            shape=(len(elements), len(elements)),
                            dtype=np.int64)

        return order, matrix


def create_cc(relations):
    adjacency = defaultdict(set)
    for m, n in relations:
        adjacency[m].add(n)
        adjacency[n].add(m)

    ret = []
    marked = set()
    for u in adjacency:
        if u in marked:
            continue

        cc = set()
        staged = {u}
        while staged:
            cc.update(staged)
            marked.update(staged)
            staged = set(x
                         for x in chain.from_iterable(adjacency[v] for v in staged)
                         if x not in marked)

        ret.append(cc)

    return ret


def line_exponent(start, end, line_plane, threshold=0):
    region = Region(*start.center, *end.center)
    for line in line_plane.intersect(region):
        if line.length > threshold:   # LineH
            return True

    return False

def get_threshold(plane, dm, virtual_domain=False):
    distances = []
    if not virtual_domain:
        for reg in plane:
            for x in plane.nearest(reg, k=4):
                d = dm(reg, x)
                if math.isfinite(d):
                    distances.append(d)
    else:
        for reg in plane:
            x = dm.nearest(reg, 1)
            if x is not None:
                distances.append(x[1])
        # DEBUG
        # distances.sort()
        # distances = distances[:len(distances)//2]
        # print(distances)

    th = -1
    if not distances:
        return th

    distances.sort()
    distances = np.array(distances)

    i = 0
    derivative = distances[1:] - distances[:-1]
    for i, de1, de2 in zip(range(derivative.size), derivative[:-1], derivative[1:]):
        if de1 > de2:
            break

    j = i+1
    for j, de1, de2 in zip(range(j, derivative.size), derivative[j:-1], derivative[j+1:]):
        if de1 < de2:
            break

    j = min(j, len(distances)-1)
    th = distances[j]
    # mean, std = np.mean(distances), np.std(distances)
    # if mean < 0:
    #     print('mean', mean)
    #     return -1

    # th, i = -1, 1
    # while th < 0:
    #     th = mean - 1 / i * std
    #     i += 1

    return th*2


def distance_func(line_plane, m, n):
    xc1, yc1 = m.center
    xc2, yc2 = n.center
    w = abs(xc2-xc1) - (m.w+n.w)/2
    h = abs(yc2-yc1) - (m.h+n.h)/2

    if w < 0:
        if xc1 > xc2:
            xc1, xc2 = xc2, xc1
        w = xc2-xc1
    else:
        if xc1 > xc2:
            xc1, xc2 = xc2+n.w/2, xc1-m.w/2
        else:
            xc1, xc2 = xc1+m.w/2, xc2-n.w/2

    if h < 0:
        if yc1 > yc2:
            yc1, yc2 = yc2, yc1
        h = yc2-yc1
    else:
        if yc1 > yc2:
            yc1, yc2 = yc2+n.h/2, yc1-m.h/2
        else:
            yc1, yc2 = yc1+m.h/2, yc2-n.h/2

    exponent = 1.

    size_ratio = m.area / n.area
    if size_ratio > 1:
        size_ratio = 1 / size_ratio
    exponent += 2 / (1+size_ratio) - 1

    dim_w = min(m.w, n.w)
    dim_h = min(m.h, n.h)
    for line in line_plane[xc1:xc2, yc1:yc2]:
        length = line.length
        if line.axis is Line.V and length > dim_h:
            exponent *= length / dim_h
        elif line.axis is Line.H and length > dim_w:
            exponent *= length / dim_w

    if exponent > 3:
        return math.inf
    return max(w / W_H_RATIO, h)**int(exponent)


def cluster(boxes, contours, texts, lines):
    h_lines, v_lines = lines

    region_plane = Plane(contours | texts)
    region_plane.merge_overlapped()
    line_plane = Plane(chain.from_iterable(lines))
    box_plane = Plane(boxes)

    if len(region_plane) + len(box_plane) == 1:
        return set()

    # Add the outmost box
    box_plane.add_region(
        Region(*region_plane.bound) | Region(*box_plane.bound))

    def process_box(box, regions):
        plane = Plane(regions)

        while True:
            dm = DistanceManager(partial(distance_func, line_plane),
                                 set(plane))
            th = get_threshold(plane, dm)
            updates = set(dm.below(th))

            if not updates and th != -1:
                th = get_threshold(plane, dm, True)
                updates = set(dm.below(th))

            if not updates:
                break

            # print(f'----- {th} ({len(updates)}) -----')
            # if th > 200:
            #     for a, b in updates:
            #         print(dm(a, b), str(a), str(b))

            # debug_stub(dm, th)

            ccs = create_cc(updates)
            for cc in ccs:
                plane.remove_regions(cc)

                cc = Cluster(cc)
                covered_regions = plane.covered_regions(cc)
                for c in covered_regions:
                    cc.add(c)
                    plane.remove_region(c)

                plane.add_region(cc)

        ret = set(plane)
        if len(ret) == 1:
            return ret.pop()

        return Cluster(ret)

    # Process small boxes first, so that when one box covers another,
    # the inner one get processed first.
    waiting = list(box_plane)
    waiting.sort(key=lambda b: b.area)
    while waiting:
        box = waiting.pop(0)

        box_covered = None
        covered_regions = set()
        for reg in region_plane.covered_regions(box):
            # if cover_ratio(reg, box) > 0.9:
            #     box_covered = reg
            #     break

            if cover_ratio(box, reg) > 0.7:
                covered_regions.add(reg)

        # If this box is inside a region:
        # remove this box
        if box_covered is not None:
            box_plane.remove_region(box)
            continue

        # If this box doesn't cover any region:
        # the box is a region
        if len(covered_regions) == 0:
            box_plane.remove_region(box)
            region_plane.add_region(box)

        # If this box covers exactly one region:
        # remove this box and align the region with the box
        elif len(covered_regions) == 1:
            reg = covered_regions.pop()
            box_plane.remove_region(box)
            region_plane.update_region(reg, *box.bbox)

        # If this box covers multiple regions:
        # merge these regions into one cluster
        else:
            cluster = process_box(box, covered_regions)
            box_plane.remove_region(box)

            region_plane.remove_regions(covered_regions)
            region_plane.add_region(cluster)
            region_plane.update_region(cluster, *box.bbox)

    return Cluster(set(region_plane))
