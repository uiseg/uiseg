from functools import reduce
from itertools import tee

import cv2
import numpy as np

from utils.region import Region, cover_ratio
from utils.line import LineH, LineV
from utils.plane import Plane


SIZE = (512, 512)
CANNY_MINIMUM = 50
CANNY_MAXIMUM = 150
HORIZONTAL_LINE_MINIMUM = 150
VERTICAL_LINE_MINIMUM = 100
CONTOUR_TEXT_RATIO = 0.6
INVALID_LINE_COVER_RATIO = 0.7
INVALID_LINE_DIFF_DISTANCE = 12

class Contour(Region):
    __slots__ = ('points', )

    def __init__(self, x1, y1, x2=None, y2=None):
        super().__init__(x1, y1, x2, y2)
        self.points = []


def test_approx(image):
    pass

def morph(image, shape, size=None):
    if isinstance(shape, str):
        shape = getattr(cv2, shape)
        kernel = cv2.getStructuringElement(shape, size)
    else:
        kernel = shape

    image = cv2.erode(image, kernel, iterations=1)
    image = cv2.dilate(image, kernel, iterations=1)
    return image


def get_lines(image, text_plane, boxes):
    edges = cv2.Canny(image, CANNY_MINIMUM / 4, CANNY_MAXIMUM / 4)
    mask = np.zeros(edges.shape, dtype=edges.dtype)
    for box in boxes:
        x1, y1, x2, y2 = box.bbox
        mask[y1:y1+4, x1:x2] = 255
        mask[y2-4:y2, x1:x2] = 255
        mask[y1:y2, x1:x1+4] = 255
        mask[y1:y2, x2-4:x2] = 255
    edges[mask == 255] = 0

    kernel = np.ones((3, 3), dtype=np.uint8)
    kernel[::2, ::2] = 0
    dilated_line = cv2.dilate(edges, kernel, iterations=1)

    horizontal_lines = morph(
        dilated_line, 'MORPH_RECT', (HORIZONTAL_LINE_MINIMUM, 2))
    vertical_lines = morph(
        dilated_line, 'MORPH_RECT', (2, VERTICAL_LINE_MINIMUM))

    _1, h_contours, _2 = cv2.findContours(horizontal_lines,
                                          cv2.RETR_LIST,
                                          cv2.CHAIN_APPROX_SIMPLE)
    _1, v_contours, _2 = cv2.findContours(vertical_lines,
                                          cv2.RETR_LIST,
                                          cv2.CHAIN_APPROX_SIMPLE)

    kernel = np.ones((3, 3), dtype=np.uint8)
    horizontal_lines = cv2.dilate(horizontal_lines, kernel, iterations=1)
    vertical_lines = cv2.dilate(vertical_lines, kernel, iterations=1)

    h_lines = set()
    for c in h_contours:
        x1, y1, w, h = cv2.boundingRect(c)
        x2, y2 = x1+w, y1+h
        y = int((y1+y2)/2)

        # Remove lines that are actually texts
        line = Region(x1, y1, x2, y2)
        if any(cover_ratio(reg, line) > 0.7
               for reg in text_plane.intersect(line)):
            horizontal_lines[y1:y2, x1:x2] = 0
            continue

        line = LineH(y, x1, x2)
        h_lines.add(line)

    v_lines = set()
    for c in v_contours:
        x1, y1, w, h = cv2.boundingRect(c)
        x2, y2 = x1+w, y1+h
        x = int((x1+x2)/2)

        # Remove lines that are actually texts
        line = Region(x1, y1, x2, y2)
        if any(cover_ratio(reg, line) > 0.7
               for reg in text_plane.intersect(line)):
            vertical_lines[y1:y2, x1:x2] = 0
            continue

        line = LineV(x, y1, y2)
        v_lines.add(line)

    no_lines = cv2.Canny(image, CANNY_MINIMUM, CANNY_MAXIMUM)
    no_lines[mask == 255] = 0
    no_lines[(horizontal_lines | vertical_lines) != 0] = 0

    v_line_plane = Plane(h_lines)
    waiting = set(v_lines)
    for line in waiting:
        anchors = [h.anchor for h in v_line_plane.covered_regions(*line.bbox)]
        anchors.insert(0, line.start)
        anchors.insert(-1, line.end)

        v_lines.remove(line)
        start, end = tee(anchors, 2)
        next(end)
        x = line.anchor

        for s, e in zip(start, end):
            v_lines.add(LineV(x, s, e))

    return (h_lines, v_lines), no_lines

def get_boxes(image, text_plane):
    kernel = np.ones((3, 3), dtype=np.uint8)
    edges = cv2.Canny(image, CANNY_MINIMUM, CANNY_MAXIMUM)
    edges = cv2.dilate(edges, kernel, iterations=1)
    _1, contours, hierarchy = cv2.findContours(
        edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    hierarchy = hierarchy.reshape(-1, 4)
    areas = {}
    children = {}
    bboxes = {}
    for i, (c, p) in enumerate(zip(contours, hierarchy[:, 3])):
        areas[i] = cv2.contourArea(c)
        bboxes[i] = Region.from_dim(cv2.boundingRect(c))
        p = int(p)

        if p == -1:
            continue

        if p not in children:
            children[p] = set()
        children[p].add(i)

    ratio = {}
    for i, a in areas.items():
        if i not in children:
            continue
        children_area = sum(areas[j] for j in children[i])
        ratio[i] = (a-children_area)/a

    ratio = list(ratio.items())
    ratio.sort(key=lambda r: r[1])

    ret = set()
    for i, r in ratio:
        if r > 0.35:
            continue

        if areas[i]/bboxes[i].area < 0.8:
            continue

        reg = bboxes[i]
        texts = text_plane.intersect(reg)
        if any(cover_ratio(reg, text) < 0.7 for text in texts):
            continue

        ret.add(reg)

    return ret

    # debugger.set_debug(True)
    # plot, ax = plt.subplots()
    # ax.plot([r for i, r in ratio])
    # debugger.vis.pyplot('asdf', plot)
    # debugger.set_debug(False)
    # plt.close()


def get_contours(no_lines):
    kernel = np.ones((3, 3), dtype=np.uint8)
    no_lines = cv2.dilate(no_lines, kernel, iterations=1)
    _1, contours, _2 = cv2.findContours(
        no_lines, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    if not contours:
        return set()

    ret = set()
    for c in contours:
        x, y, w, h = cv2.boundingRect(c)
        contour = Region(x, y, x+w, y+h)
        ret.add(contour)

    contour_plane = Plane(ret)
    contour_plane.merge_overlapped()

    return set(reg for reg in contour_plane
               if reg.w > 5 and reg.h > 10)


def refine_text_regions(text_plane, contour_plane, lines):
    h_lines, v_lines = lines
    for line in h_lines:
        texts = text_plane.covered_regions(*line.bbox)
        for text in texts:
            if (line.start < text.x1 and line.end > text.x2 and
                    line.anchor > text.y1+5 and line.anchor < text.y2-5):
                new_text = text.copy()
                new_text.y1 = line.anchor+1

                text_plane.update_region(text, y2=line.anchor-1)
                print(new_text)
                text_plane.add_region(new_text)

    processed = set()
    while True:
        covered = list()
        for text in text_plane:
            if text in processed:
                continue
            processed.add(text)

            for target in contour_plane.intersect(text):
                intersect = text & target
                if (intersect is not None and
                        intersect.area / target.area > CONTOUR_TEXT_RATIO and
                        text.area > target.area):
                    covered.append(target)

            if covered:
                break

        if covered:
            u = reduce(Region.__ior__, covered, covered[0].copy())
            text_plane.update_region(text, *u.bbox)
            contour_plane.remove_regions(covered)
        else:
            break


def remove_invalid_lines(plane, lines):
    removed = set()
    for line in lines:
        if line.anchor == 0 or line.anchor == SIZE[1] - 1:
            removed.add(line)
            continue

        line_region = Region.from_bbox(line.bbox)
        if line_region.area == 0:
            removed.add(line)
            continue

        intersection = plane.intersect(line_region)
        if any(cover_ratio(reg, line_region) > INVALID_LINE_COVER_RATIO
               for reg in intersection):
            removed.add(line)
            for reg in intersection:
                plane.update_region(
                    reg, *(reg | line_region).bbox)
            continue

    lines -= removed


def segment(fig, texts):
    text_plane = Plane(texts)
    text_plane.merge_overlapped()

    # 1. Convert to grayscale image
    image = cv2.cvtColor(fig.image, cv2.COLOR_BGR2GRAY)
    image[:1] = 255 - image[:1]
    image[-1:] = 255 - image[-1:]
    image[:, :1] = 255 - image[:, :1]
    image[:, -1:] = 255 - image[:, -1:]
    # draw_regions(fig, box_regions, color=(255, 0, 0))

    # 2. Get lines, bboxes, and contours from image and texts
    boxes = get_boxes(image, text_plane)
    (h_lines, v_lines), no_lines = get_lines(image, text_plane, boxes)
    contours = get_contours(no_lines)

    # # 3. Get contour_plane from no_lines and refine them with lines
    contour_plane = Plane(contours)
    remove_invalid_lines(contour_plane, h_lines)
    remove_invalid_lines(contour_plane, v_lines)

    # # 4. Identify those contours bounded by text regions and refine these
    # #    text regions.
    refine_text_regions(text_plane, contour_plane, (h_lines, v_lines))
    contours = set(contour_plane)
    texts = set(text_plane)

    return (h_lines, v_lines), boxes, contours, texts
