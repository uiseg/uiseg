# Model

## Scripts

1. `preprocess_text`: find text regions from DOM nodes and image clues.
2. `preprocess_contours`: merge regions from image contours and Faster-RCNN predicted text regions.
