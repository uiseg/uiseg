import json
import random
from time import sleep
from argparse import ArgumentParser
import socket

import requests

from utils.visualizer import Visualizer
from utils.figure import Figure
from utils.figure_toolkit import draw_regions, draw_bboxes
from utils.region import Region
from utils import dataset


vis = Visualizer(env='present_server', server='http://localhost')
SIZE = (512, 512)
SOCK = ''


def get_countours(key, x, y):
    data = json.dumps(
        dict(key=key,
             x=int(x),
             y=int(y)))

    with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as sock:
        sock.connect(SOCK)
        sock.sendall(data.encode('utf-8'))

        received = str(sock.recv(1024), encoding='utf-8')

    received = json.loads(received)
    regions = set(Region.from_bbox(bbox) for bbox in received['bboxes'])
    h_lines = set(tuple(line) for line in received['h_lines'])
    v_lines = set(tuple(line) for line in received['v_lines'])
    return (h_lines, v_lines), regions

def get_texts(key, x, y):
    results = requests.get(f'http://localhost:8000/{key}?x={x}&y={y}')
    results = results.json()
    return set(Region.from_bbox(bbox) for bbox in results['bboxes'])


def main():
    HISTORY = []
    image_set = set(dataset.raw)
    if not image_set:
        raise ValueError(f'No image found in {dataset}')

    print(f'Found {len(image_set)} image(s)')

    while True:
        image_key = random.sample(image_set, 1)[0]
        image_path = dataset.raw.path('png', image_key)
        figure = Figure.load(image_path)

        w, h = figure.size

        for _ in range(10):
            x = random.randrange(0, w-SIZE[0])
            y = random.randrange(0, h-SIZE[1])
            crop = figure.crop((x, y, x+SIZE[0], y+SIZE[1]))
            try:
                texts = get_texts(image_key, x, y)
                (h_lines, v_lines), contours = get_countours(image_key, x, y)
            except Exception as e:
                print(e)
                sleep(7)
                continue


            vis.figure('Original', crop)

            new_crop = crop.copy()
            draw_regions(new_crop, texts)
            vis.figure('Predicted Text', new_crop)

            new_crop = crop.copy()
            draw_regions(new_crop, contours)
            draw_bboxes(new_crop,
                        set((x1, y, x2, y+1) for y, x1, x2 in h_lines),
                        color=(255, 255, 0))
            draw_bboxes(new_crop,
                        set((x, y1, x+1, y2) for x, y1, y2 in v_lines),
                        color=(255, 255, 0))
            vis.figure('Predicted contours', new_crop)

            HISTORY.insert(0, f'{image_path}_{x}_{y}')
            HISTORY = HISTORY[:10]

            try:
                vis.table('History', HISTORY)
            except Exception as e: # pylint: disable=broad-except
                vis.text('History', str(e))

            sleep(7)


if __name__ == '__main__':
    parser = ArgumentParser(prog='preprocess_client.py')
    parser.add_argument('dataset', help='Dataset directory')
    parser.add_argument('--sock', help='Socket path',
                        default='/tmp/preprocess_contour_server')

    args = parser.parse_args()
    dataset.init(args.dataset)
    SOCK = args.sock

    print('Dataset directory:', args.dataset)
    print('Socket:', args.sock)
    main()
