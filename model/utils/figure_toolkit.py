import numpy as np
import cv2

FONT_SIZE = 0.5
FONT = cv2.FONT_ITALIC | cv2.FONT_HERSHEY_DUPLEX


def _int_all(bbox):
    return tuple(int(x) for x in bbox)


def detect_background_only(fig, region):
    x1, y1, x2, y2 = _int_all(region.bbox)
    target = cv2.cvtColor(fig.image[y1:y2, x1:x2], cv2.COLOR_BGR2GRAY)

    hist = np.histogram(target, 5, range=(0, 255))
    most_num = hist[0].max()
    other_num = hist[0].sum() - most_num
    ratio = other_num / (most_num + other_num)

    # print(region, ratio)

    return ratio < 0.1


def remove_foreground(fig, region, background=None):
    if background is None:
        x1, y1, x2, y2 = _int_all(region.bbox)
        target = cv2.cvtColor(fig.image[y1:y2, x1:x2], cv2.COLOR_BGR2GRAY)
        # This may happen if region.area is 0
        if target is None:
            return

        # Check bakground is black or white
        hist = np.histogram(target, 2)
        if hist[0][0] >= hist[0][1]:
            threshold = cv2.THRESH_BINARY + cv2.THRESH_TRIANGLE
        else:
            threshold = cv2.THRESH_BINARY_INV + cv2.THRESH_TRIANGLE

        _, thresh = cv2.threshold(target,
                                  0,
                                  255,
                                  threshold)
        # fig.image[y1:y2, x1:x2] = np.expand_dims(thresh, 2)
        # return

        paste = cv2.inpaint(fig.image[y1:y2, x1:x2], thresh, .5, cv2.INPAINT_TELEA)
        fig.image[y1:y2, x1:x2] = paste


def draw_grid(fig, step=100, *,
              color=None, alpha=0.7):
    height, width = fig.image.shape[:2]
    if fig.mode is fig.BIN:
        color = 255 if color is None else color
        background = 255
    else:
        color = (0, 0, 255) if color is None else color
        background = (255, 255, 255)

    for x in range(step, width, step):
        draw_anchor(fig, x, 'x', color=color, alpha=alpha)
        draw_text(fig, str(x), (x, 0),
                  color=color, background=background)
    for y in range(step, height, step):
        draw_anchor(fig, y, 'y', color=color, alpha=alpha)
        draw_text(fig, str(y), (0, y),
                  color=color, background=background)

    return fig


def draw_anchor(fig, anchor, axis, *,
                color=None, alpha=0.7):
    alpha_inv = 1.0 - alpha
    w, h = fig.size
    if color is None:
        if fig.mode is fig.BIN:
            color = 255
        else:
            color = (0, 0, 255)

    if axis == 'x':
        if anchor < 0 or anchor >= w:
            return fig
        if fig.mode is fig.BIN:
            fig.image[:, anchor] = (alpha * color +
                                    alpha_inv * fig.image[:, anchor])
        else:
            for c in range(fig.image.shape[2]):
                fig.image[:, anchor, c] = (alpha * color[c] +
                                           alpha_inv * fig.image[:, anchor, c])
    elif axis == 'y':
        if anchor < 0 or anchor >= h:
            return fig
        if fig.mode is fig.BIN:
            fig.image[anchor, :] = (alpha * color +
                                    alpha_inv * fig.image[anchor, :])
        else:
            for c in range(fig.image.shape[2]):
                fig.image[anchor, :, c] = (alpha * color[c] +
                                           alpha_inv * fig.image[anchor, :, c])
    else:
        raise ValueError('Axis should be either x or y')

    return fig


def fill_bbox(fig, bbox, color, alpha=0.5):
    x1, y1, x2, y2 = _int_all(bbox)
    alpha_inv = 1.0 - alpha

    if fig.mode is fig.BIN:
        fig.image[y1:y2, x1:x2] = (alpha * color +
                                   alpha_inv * fig.image[y1:y2, x1:x2])
    else:
        channels = fig.image.shape[2]
        for c in range(channels):
            fig.image[y1:y2, x1:x2, c] = (alpha * color[c] +
                                          alpha_inv * fig.image[y1:y2, x1:x2, c])

def draw_text(fig, text, xy, *,
              color=(0, 0, 0), background=None):
    (w, h), *_ = cv2.getTextSize(text, FONT, FONT_SIZE, 1)
    x, y = _int_all(xy)

    if background:
        fill_bbox(fig, (x, y, x+w, y+h+4), background)

    cv2.putText(fig.image,
                text,
                (x, y+h+1),
                FONT,
                FONT_SIZE,
                color,
                1,
                cv2.LINE_AA)


def draw_bbox(fig, bbox, caption=None, *,
              color=None, alpha=0.7):
    x1, y1, x2, y2 = _int_all(bbox)
    alpha_inv = 1.0 - alpha

    overlay = fig.image[y1:y2, x1:x2]
    if overlay.size == 0:
        return fig

    if color is None:
        if fig.mode is fig.BIN:
            color = 255
        else:
            color = (0, 0, 255)

    overlay[:, 0] = color
    overlay[:, -1] = color
    overlay[0, :] = color
    overlay[-1, :] = color

    if fig.mode is fig.BIN:
        fig.image[y1:y2, x1:x2] = (alpha * overlay[:, :] +
                                   alpha_inv * fig.image[y1:y2, x1:x2])
    else:
        channels = fig.image.shape[2]
        for c in range(channels):
            fig.image[y1:y2, x1:x2, c] = (alpha * overlay[:, :, c] +
                                          alpha_inv * fig.image[y1:y2, x1:x2, c])

    if caption:
        draw_text(fig, caption, (x1, y1),
                  color=color, background=(255, 255, 255))

    return fig


def draw_bboxes(fig, bboxes, captions=None, *,
                color=None, alpha=0.7):
    if color is None:
        if fig.mode is fig.BIN:
            color = 255
        else:
            color = (0, 0, 255)

    if captions is None:
        for bbox in bboxes:
            draw_bbox(fig, bbox, color=color, alpha=alpha)
    else:
        for caption, bbox in zip(captions, bboxes):
            draw_bbox(fig, bbox, caption, color=color, alpha=alpha)

    return fig


def draw_regions(fig, regions, captions=None, *,
                 color=None, alpha=0.7):
    if color is None:
        if fig.mode is fig.BIN:
            color = 255
        else:
            color = (0, 0, 255)

    if captions is None:
        for reg in regions:
            draw_bbox(fig, reg.bbox, color=color, alpha=alpha)
    else:
        for caption, reg in zip(captions, regions):
            draw_bbox(fig, reg.bbox, caption, color=color, alpha=alpha)

    return fig
