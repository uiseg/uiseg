import random


class Region:
    __slots__ = ('x1', 'y1', 'x2', 'y2')

    def __init__(self, x1, y1=None, x2=None, y2=None):
        if y1 is None and x2 is None and y2 is None:
            x1, y1, x2, y2 = x1.bbox

        elif x2 is None and y2 is None:
            x2, y2 = y1
            x1, y1 = x1

        if x1 > x2:
            x1, x2 = x2, x1
        if y1 > y2:
            y1, y2 = y2, y1

        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    def copy(self):
        return Region(*self.bbox)

    def shift(self, x, y):
        self.x1 += x
        self.x2 += x
        self.y1 += y
        self.y2 += y

        return self

    @property
    def w(self):
        return self.x2 - self.x1

    @property
    def h(self):
        return self.y2 - self.y1

    @property
    def dim(self):
        return (self.x1, self.y1, self.w, self.h)

    @property
    def bbox(self):
        return (self.x1, self.y1, self.x2, self.y2)

    @property
    def area(self):
        return (self.y2 - self.y1) * (self.x2 - self.x1)

    @property
    def center(self):
        return ((self.x1 + self.x2) / 2), ((self.y1 + self.y2) / 2)

    @classmethod
    def from_bbox(cls, bbox):
        return cls(*bbox)

    @classmethod
    def from_dim(cls, dim):
        x, y, w, h = dim
        return cls(x, y, x+w, y+h)

    def __str__(self):
        return f'Region({self.x1:.2f}, {self.y1:.2f}, {self.x2:.2f}, {self.y2:.2f})'

    __repr__ = __str__


def is_intersect(reg1, reg2):
    if reg1.x2 <= reg2.x1 or\
       reg2.x2 <= reg1.x1 or\
       reg1.y2 <= reg2.y1 or\
       reg2.y2 <= reg1.y1:
        return False

    return True


def intersection(reg1, reg2):
    if not is_intersect(reg1, reg2):
        return None

    rx1 = max(reg1.x1, reg2.x1)
    ry1 = max(reg1.y1, reg2.y1)
    rx2 = min(reg1.x2, reg2.x2)
    ry2 = min(reg1.y2, reg2.y2)

    return Region(rx1, ry1, rx2, ry2)


def iintersection(reg1, reg2):
    if not is_intersect(reg1, reg2):
        reg1.__init__(0., 0., 0., 0.)

    else:
        reg1.x1 = max(reg1.x1, reg2.x1)
        reg1.y1 = max(reg1.y1, reg2.y1)
        reg1.x2 = min(reg1.x2, reg2.x2)
        reg1.y2 = min(reg1.y2, reg2.y2)


def union(reg1, reg2):
    rx1 = min(reg1.x1, reg2.x1)
    ry1 = min(reg1.y1, reg2.y1)
    rx2 = max(reg1.x2, reg2.x2)
    ry2 = max(reg1.y2, reg2.y2)

    return Region(rx1, ry1, rx2, ry2)


def iunion(reg1, reg2):
    reg1.x1 = min(reg1.x1, reg2.x1)
    reg1.y1 = min(reg1.y1, reg2.y1)
    reg1.x2 = max(reg1.x2, reg2.x2)
    reg1.y2 = max(reg1.y2, reg2.y2)

    return reg1


Region.__or__ = union
Region.union = union

Region.__ior__ = iunion
Region.iunion = iunion

Region.__and__ = intersection
Region.intersection = intersection

Region.__iand__ = iintersection
Region.iintersection = iintersection

Region.is_intersect = is_intersect


def cover_ratio(reg1, reg2):
    """Return the ratio of Area(intersection) / Area(region2)"""
    inter = intersection(reg1, reg2)
    if inter is None:
        return 0

    return inter.area / reg2.area


def get_random_covered_region(reg1, reg2, proposal_dim):
    """Find a region in reg1 that has dim=proposal_dim and covers reg2"""
    w, h = proposal_dim
    if reg1.w < w or reg1.h < h:
        print('Cannot find a covered region', reg1, reg2, proposal_dim)
        return None

    inter = intersection(reg1, reg2)
    if inter is None:
        print('The two regions don\'t have intersection', reg1, reg2)
        return None

    min_x = max(reg1.x1, inter.x2 - w)
    min_y = max(reg1.y1, inter.y2 - h)
    max_x = min(reg1.x2 - w, inter.x1)
    max_y = min(reg1.y2 - h, inter.y1)

    if min_x > max_x:
        min_x, max_x = max_x, min_x
    if min_y > max_y:
        min_y, max_y = max_y, min_y

    x = random.randint(min_x, max_x)
    y = random.randint(min_y, max_y)

    return Region.from_dim((x, y, w, h))


def get_best_covered_region(img_region, regions, target_region, size):
    """Get best proposal by randomly generating n proposals, n is 20 by default"""
    best_region, least_err = None, 10000

    for _ in range(20):
        proposal = get_random_covered_region(img_region, target_region, size)
        if proposal is None:
            continue

        err = 0.0
        for reg in regions:
            cr = cover_ratio(proposal, reg)
            if cr > 0.15 and cr < 0.85:
                err = cr + abs(cr - 0.5)

        if err == 0.0:
            return proposal
        elif err < least_err:
            best_region = proposal
            least_err = err

    return best_region
