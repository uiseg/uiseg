import multiprocessing as mp
from multiprocessing.dummy import Pool as ThreadPool
from functools import partial

from tqdm import tqdm

PROCESS_TIMEOUT = 180


class TaskError(Exception):
    pass


def dispatch_task(func, candidates, *,
                  multiprocess=True, timeout=PROCESS_TIMEOUT, strict_error=False):
    task = partial(abortable_run, func, timeout=timeout, strict_error=strict_error)
    results = []
    if multiprocess:
        # Multi threaded
        with mp.Pool(processes=8) as pool:
            with tqdm(desc='Processing', total=len(candidates)) as pbar:
                for result in pool.imap_unordered(task, candidates):
                    if isinstance(result, TaskError):
                        if strict_error:
                            raise result
                        tqdm.write(str(result))
                        continue
                    else:
                        results.append(result)
                    pbar.update()
    else:
        # Single threaded
        with tqdm(desc='Processing', total=len(candidates)) as pbar:
            for cand in candidates:
                result = task(cand)
                if isinstance(result, TaskError):
                    if strict_error:
                        raise result
                    tqdm.write(str(result))
                    continue
                else:
                    results.append(result)
                pbar.update()

    return results


def abortable_run(task, *args, timeout=PROCESS_TIMEOUT, strict_error=False):
    try:
        with ThreadPool(processes=1) as p:
            result = p.apply_async(task, args=args)
            return result.get(timeout=timeout)

    except KeyboardInterrupt as e:
        raise e

    except Exception as e: # pylint: disable=broad-except
        if strict_error:
            raise e

        return TaskError(
            f'{e.__class__.__name__} when processing with arguments {args}: \n   {e}')
