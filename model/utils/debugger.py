import sys

from .figure import Figure
from .figure_toolkit import draw_regions
from .visualizer import Visualizer

class Debug:
    vis = None
    target = None

    server = 'http://localhost'
    env = 'main'

    def set_debug(self, debug=None, **kwargs):
        if debug is None:
            debug = self.vis is None

        if not debug:
            self.vis = None
            return

        env = kwargs.get('env', self.env)
        server = kwargs.get('server', self.server)
        target = kwargs.get('bbox', self.target)

        self.vis = Visualizer(env=env, server=server)

        self.env = env
        self.server = server
        self.target = target

    def figure(self, name, fig, *, regions=None):
        if self.vis is None:
            return

        if not isinstance(fig, Figure):
            fig = Figure(fig, copy=False)

        if regions is not None:
            fig = fig.copy()
            draw_regions(fig, regions)

        if self.target is not None:
            fig = fig.crop(self.target)

        self.vis.figure(name, fig)

    @property
    def on(self):
        return self.vis is not None


sys.modules[__name__] = Debug()
