from itertools import product, repeat

def proposal_gen(shape, size=(512, 512)):
    w, h = shape
    size_x, size_y = size
    proposals = list(product(range(0, w-size_x+1, int(size_x/2)),
                             range(0, h-size_y+1, int(size_y/2))))
    proposals.extend(zip(repeat(w-size_x),
                         range(0, h-size_y+1, int(size_y/2))))
    proposals.extend(zip(range(0, w-size_x+1, int(size_x/2)),
                         repeat(h-size_y)))
    proposals.append((w-size_x, h-size_y))
    return proposals
