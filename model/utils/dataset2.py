import os
import sys
from glob import glob
from os import path as osp
from pathlib import Path
from functools import reduce
from collections.abc import Set
import operator

PAGE_DIR = ('raw', 'merged')


def _split(p):
    i = p.rfind('.')
    if i == -1:
        return p, ''
    return p[:i], p[i+1:]


def _to_base(p):
    i = p.rfind('.')
    if i == -1:
        return p
    return p[:i]


def _to_suffix(p):
    i = p.rfind('.')
    if i == -1:
        return ''
    return p[i+1:]


class ExtGroup(Set):
    __slots__ = ('_path', '_ext', '_contents')

    def __init__(self, path, ext, contents=None):
        if contents is None:
            contents = frozenset()

        self._path = path
        self._ext = ext
        self._contents = frozenset(contents)

    def __contains__(self, name):
        return name in self._contents

    def __iter__(self):
        return iter(self._contents)

    def __len__(self):
        return len(self._contents)

    def __getitem__(self, name):
        if name in self._contents:
            return str(self._path / f'{name}.{self._ext}')

        raise FileNotFoundError(str(self._path / f'{name}.{self._ext}'))

    def get(self, name):
        return str(self._path / f'{name}.{self._ext}')

    def __str__(self):
        return f'ExtGroup(path{self._path}, ext={self._ext})'

    __repr__ = __str__


class Folder(Set):
    def __init__(self, path):
        self.path = Path(path)
        self._cache = set()

        _prefix_len = len(str(self.path))+1
        self.folders = set(s[_prefix_len:-1] for s in glob(f'{self.path}/*/'))

        contents = dict()
        for p in set(os.listdir(self.path)) - self.folders:
            name, ext = _split(p)

            if ext not in contents:
                contents[ext] = set()
            contents[ext].add(name)

        self.contents = {ext: ExtGroup(self.path, ext, names)
                         for ext, names in contents.items()}

        if contents:
            self.keys = reduce(operator.and_, contents.values())
        else:
            self.keys = set()

    def __iter__(self):
        return iter(self.keys)

    def __contains__(self, key):
        return key in self.keys

    def __getattr__(self, name):
        if name in self.folders:
            return self._add_folder(name)

        elif name in self.contents:
            return self.contents[name]

        return ExtGroup(self.path, name)

    __getitem__ = __getattr__

    def __len__(self):
        return len(self.keys)

    def __str__(self):
        return str(self.path)

    def __repr__(self):
        return f'{self.__class__.__name__}(\'{self.path.resolve()}\')'

    def efind(self, name):
        return set(ext for ext, items in self.contents.items()
                   if name in items)

    def pfind(self, name, exts=None):
        if exts is None:
            exts = set(self.contents)

        return set(str(self.path / f'{name}.{ext}')
                   for ext, items in self.contents.items()
                   if name in items and ext in exts)

    def update(self):
        for folder in self._cache & set(self.__dict__):
            del self.__dict__[folder]

        self.__init__(self.path)

    def mkdir(self, channel):
        channel = self.path / channel
        if not osp.exists(channel):
            os.mkdir(channel)

    exists = __contains__

    def _add_folder(self, name):
        self._cache.add(name)
        self.__dict__[name] = Folder(self.path / name)

        return self.__dict__[name]


def _raise(*_1, **_2):
    raise ValueError(
        f'Call {__name__}.init(dataset_dir) before accessing the dataset.')


_funcs = set([
    '__getitem__',
    '__getattr__',
    '__contains__',
    '__len__',
    '__iter__',
    'update',
    'exists',
    'mkdir',
    'pfind',
    'efind',
])


class Dataset(Folder):
    Folder = Folder
    ExtGroup = ExtGroup

    def __init__(self, path=None):
        if path is not None:
            _super = super()
            _super.__init__(path)
            for func in _funcs:
                setattr(Dataset, func, getattr(_super, func))
        else:
            self.path = None
            for func in _funcs:
                setattr(Dataset, func, _raise)

    def init(self, root):
        root = Path(root).absolute()
        if not osp.isdir(root):
            raise FileNotFoundError(
                f'"{root}" doesn\'t exist or it is not a directory')

        self.__init__(root)

    def __repr__(self):
        return f'Dataset({self.path})'

    @staticmethod
    def basekey(path):
        key = osp.basename(path)
        return _to_base(key)


sys.modules[__name__] = Dataset()
