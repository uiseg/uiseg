import os
import sys
from os import path as osp
from pathlib import Path
from functools import reduce
from collections.abc import Set
from operator import and_

PAGE_DIR = ('raw', 'merged')


def _split(p):
    i = p.rfind('.')
    if i == -1:
        return p, ''
    return p[:i], p[i+1:]


def _to_base(p):
    i = p.rfind('.')
    if i == -1:
        return p
    return p[:i]


def _to_suffix(p):
    i = p.rfind('.')
    if i == -1:
        return ''
    return p[i+1:]


def _to_crop_base(p):
    i = p.rfind('.')
    j = p.rfind('_', 0, i)
    if j == -1:
        return p[:i], ''
    if i == -1:
        return p[:j], p[j+1:]
    return p[:j], p[j+1:i]


def _to_crop(p):
    i = p.rfind('_')
    if i == -1:
        return p, ''
    return p[:i], p[i+1:]


class Page(Set):
    __slots__ = ('_root', 'keys', 'contents', 'suffixes')

    def __init__(self, path):
        self._root = path
        self.contents = set(os.listdir(path))
        self.suffixes = set(_to_suffix(p) for p in self.contents)
        if not self.suffixes:
            self.keys = set()
        else:
            keys = {suf: set() for suf in self.suffixes}
            for p in self.contents:
                key, suf = _split(p)
                keys[suf].add(key)

            self.keys = reduce(and_, keys.values())

    def __iter__(self):
        return iter(self.keys)

    def __contains__(self, key):
        return key in self.keys

    def __getattr__(self, type_name, keys=None):
        if type_name not in self.suffixes:
            return set()

        if keys is None:
            keys = self.keys

        path = str(self._root)
        ret = set()
        for key in keys:
            if key not in self.keys:
                continue

            ret.add(osp.join(path, f'{key}.{type_name}'))

        return ret

    def __len__(self):
        return len(self.keys)

    def __repr__(self):
        return f'{self.__class__.__name__}(\'{self._root.resolve()}\')'

    def path(self, type_name, key):
        return osp.join(self._root, f'{key}.{type_name}')

    def paths(self, type_name, keys=None):
        if keys is None:
            keys = self.keys

        return set(osp.join(self._root, f'{key}.{type_name}')
                   for key in keys)

    def update(self):
        self.__init__(self._root)

    def crops(self, crop, keys=None):
        if keys is None:
            keys = self.keys
        return crop.crops(keys)

    exists = __contains__


class PageCrop(Set):
    __slots__ = ('_root', 'keys', 'contents', 'suffixes')

    def __init__(self, path):
        self._root = path
        self.contents = set(os.listdir(path))
        self.suffixes = set(_to_suffix(p) for p in self.contents)
        if not self.suffixes:
            self.keys = {}
        else:
            keys = {}
            for p in self.contents:
                base, suf = _split(p)
                crop, idx = _to_crop(base)

                if crop not in keys:
                    keys[crop] = {suf: set() for suf in self.suffixes}

                keys[crop][suf].add(idx)

            self.keys = {}
            for crop, indexes in keys.items():
                indexes = reduce(and_, indexes.values())

                if indexes:
                    self.keys[crop] = indexes

    def __iter__(self):
        for key, indexes in self.keys.items():
            for idx in indexes:
                yield f'{key}_{idx}'

    def __contains__(self, key):
        key, idx = _to_crop(key)
        return key in self.keys and idx in self.keys[key]

    def __getattr__(self, type_name, keys=None):
        if type_name not in self.suffixes:
            return set()

        if keys is None:
            keys = self.keys

        path = str(self._root)
        ret = set()
        for crop in keys:
            if crop not in self.keys:
                continue

            ret.update(osp.join(path, f'{crop}_{idx}.{type_name}')
                       for idx in self.keys[crop])

        return ret

    def __len__(self):
        return sum(len(keys) for keys in self.keys.values())

    def __str__(self):
        return str(self._root)

    def __repr__(self):
        return f'{self.__class__.__name__}(\'{self._root}\')'

    def crops(self, keys=None):
        if keys is None:
            keys = self.keys
        elif isinstance(keys, str):
            keys = {keys}

        ret = set()
        for crop in keys:
            if crop not in self.keys:
                continue

            ret.update(f'{crop}_{idx}' for idx in self.keys[crop])

        return ret

    def path(self, type_name, key, idx=None):
        if idx is None:
            key = f'{key}.{type_name}'
        else:
            key = f'{key}_{idx}.{type_name}'
        return osp.join(self._root, key)

    def update(self):
        self.__init__(self._root)

    paths = __getattr__

    exists = __contains__


class Dataset:
    path = None
    _cache = {}

    Page = Page
    PageCrop = PageCrop

    def __init__(self, root=None):
        if root is not None:
            self.init(root)

    def __str__(self):
        return str(self.path)

    def __repr__(self):
        return f'Dataset(root={self.path})'

    def __contains__(self, name):
        self._check_init()

        return osp.exists(self.path / name)

    def __getattr__(self, name):
        self._check_init()

        if name in self._cache:
            return self._cache[name]

        path = self.path / name
        if not osp.exists(path):
            raise FileNotFoundError(f'No such directory "{path}"')

        if name in PAGE_DIR:
            target = Page(path)
        else:
            target = PageCrop(path)

        self._cache[name] = target
        self.__dict__[name] = target
        return target

    def __getitem__(self, name):
        self._check_init()
        return str(self.path / name)

    def init(self, root):
        root = Path(root)
        if not osp.isdir(root):
            raise FileNotFoundError(
                f'No such directory "{root}" or it is not a directory')

        self.path = root
        for d in self._cache:
            if d in self.__dict__:
                del self.__dict__[d]
        self._cache = {}

    def sanity_check(self):
        raise NotImplementedError

    def mkdir(self, channel):
        channel = self.path / channel
        if not osp.exists(channel):
            os.mkdir(channel)

    @staticmethod
    def basekey(path):
        key = osp.basename(path)
        return _to_base(key)

    def _check_init(self):
        if self.path is None:
            raise ValueError(
                f'Call {__name__}.init(dataset_dir) before accessing the dataset.')


sys.modules[__name__] = Dataset()
