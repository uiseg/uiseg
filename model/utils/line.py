class Axis:
    __slots__ = ('s', )

    def __init__(self, s):
        self.s = s

    def __str__(self):
        return f'Line.{self.s}'

class Line:
    __slots__ = ('axis', 'start', 'end', 'anchor')
    V = Axis('V')
    H = Axis('H')

    def __init__(self, axis, anchor, start=None, end=None):
        if start is None and end is None:
            anchor, start, end = anchor

        self.anchor = anchor
        self.start = start
        self.end = end

        if axis not in (self.H, self.V):
            raise ValueError('axis should be either V or H')
        self.axis = axis

    def copy(self):
        return Line(self.axis, self.anchor, self.start, self.end)

    def shift(self, x, y):
        if self.axis is Line.V:
            perp, para = x, y
        else:
            para, perp = x, y

        self.anchor += perp
        self.start += para
        self.end += para

        return self

    @property
    def dim(self):
        if self.axis is Line.V:
            return 1, self.length

        return self.length, 1

    @property
    def bbox(self):
        if self.axis is Line.V:
            return (self.anchor-0.5, self.start, self.anchor+0.5, self.end)

        return (self.start, self.anchor-0.5, self.end, self.anchor+0.5)

    @property
    def length(self):
        return self.end - self.start

    @property
    def center(self):
        c = (self.end + self.start) / 2
        if self.axis is Line.V:
            return c, self.anchor

        return self.anchor, c

    def __str__(self):
        return f'Line({self.axis}, {self.anchor}, {self.start}, {self.end})'

    __repr__ = __str__

def LineH(anchor, start, end):
    return Line(Line.H, anchor, start, end)

def LineV(anchor, start, end):
    return Line(Line.V, anchor, start, end)
