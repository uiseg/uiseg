# pylint: disable=protected-access
import os
import socket
import json
import signal
import pickle
from socketserver import UnixStreamServer, BaseRequestHandler
from functools import wraps


def _create_server_handler(service):
    class ServerHandler(BaseRequestHandler):
        def handle(self):
            try:
                received = self.request.recv(65536)
                received = str(received, encoding='utf-8').strip()
                received = json.loads(received)

                if 'server_call' in received:
                    func = getattr(service, received['server_call'])
                    args = received.get('args', [])
                    kwargs = received.get('kwargs', {})

                    results = func(service, *args, **kwargs)
                else:
                    results = service.server_handler(**received)

                if results:
                    self.request.sendall(pickle.dumps(results))

            except (BrokenPipeError, KeyboardInterrupt):
                pass

    return ServerHandler


class Service:
    def __init__(self, sock):
        self.sock = sock
        self._server_handler = _create_server_handler(self)
        self._role = None

    def ipython_client_handler(self):
        from IPython.terminal.embed import InteractiveShellEmbed
        InteractiveShellEmbed()()

    def server_handler(self, *args, **kwargs):
        raise NotImplementedError((
            'Server handler not implemented. '
            'This server has no use.'))

    def client_handler(self):
        raise NotImplementedError((
            'Client handler not implemented. '
            'Call start_service with only="server" '
            'if you don\'t want to start the client.'))

    def request(self, *args, timeout=None, **kwargs):
        if args:
            print('Only kwargs is allowed.')
            return ''

        data = json.dumps(kwargs)
        received = None
        try:
            with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as sock:
                sock.connect(self.sock)
                sock.sendall(data.encode('utf-8'))

                if timeout:
                    sock.settimeout(timeout)

                received = sock.recv(65536)

            if received:
                received = pickle.loads(received)

        except (socket.timeout, KeyboardInterrupt):
            pass

        return received

    @staticmethod
    def server_call(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            if self._role == 'client':
                self.request(server_call=func.__name__, args=args, kwargs=kwargs)
            elif self._role == 'server':
                func(*args, **kwargs)

        return wrapper


def start_server(service):
    service._role = 'server'
    sock_addr, handler = service.sock, service._server_handler
    try:
        os.remove(sock_addr)
    except FileNotFoundError:
        pass

    try:
        with UnixStreamServer(sock_addr, handler) as server:
            print(f'Server starts at {sock_addr}')
            server.serve_forever()

    except KeyboardInterrupt:
        os.remove(sock_addr)
        print('Server stopped')


def start_client(service):
    service._role = 'client'
    service.client_handler()


def start_service(service, only=''):
    if only == 'client':
        pid = -1
    elif only == 'server':
        pid = 0
    else:
        pid = os.fork()

    if pid == 0:    # Child process
        start_server(service)
    else:
        start_client(service)

    if pid > 0:
        os.kill(pid, signal.SIGINT)
