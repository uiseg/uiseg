import json

import requests
from tqdm import tqdm

from .figure import Figure
from .region import Region
from . import dataset2 as dataset, proposal_gen


def figure_loader(key):
    if key in dataset.raw.png:
        path = dataset.raw.png[key]

    elif key in dataset.raw.jpg:
        path = dataset.raw.jpg[key]

    else:
        raise FileNotFoundError(f'"{key}" not found in {dataset.raw}')

    return Figure.load(path)


def text_loader(key):
    with open(dataset.texts.json[key]) as f:
        texts = json.load(f)

    ret = set()
    for xy, regions in texts.items():
        x, y = xy.split('_')
        x, y = int(x), int(y)
        ret.update(Region.from_bbox(bbox).shift(x, y)
                   for bbox in regions['bboxes'])

    return ret
