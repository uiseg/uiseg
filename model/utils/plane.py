from pprint import pformat
from functools import reduce

from rtree.index import Index

from .region import Region


class Plane:
    def __init__(self, regions=None):
        if regions is not None:
            regions = set(regions)

        if regions:
            self._idx = len(regions)
            self._regions = {i: reg for i, reg in enumerate(regions)}
            self.rtree = Index((i, reg.bbox, None) for i, reg in self._regions.items())
            self._bound = Region.from_bbox(self.rtree.bounds)
        else:
            self._idx = 0
            self._regions = {}
            self.rtree = Index()
            self._bound = Region.from_bbox(self.rtree.bounds)

    @property
    def regions(self):
        return iter(self._regions.values())

    @property
    def bound(self):
        return self._bound.bbox

    def add_region(self, region):
        if region.area == 0:
            return

        self._bound |= region
        self.rtree.insert(self._idx, region.bbox)
        self._regions[self._idx] = region
        self._idx += 1

    def remove_region(self, region):
        for i, reg in self._regions.items():
            if reg is region:
                self._regions.pop(i)
                self.rtree.delete(i, reg.bbox)
                self._bound = Region.from_bbox(self.rtree.bounds)
                break

    def remove_regions(self, regions):
        if not isinstance(regions, set):
            regions = set(regions)

        ids = set()
        for i, reg in self._regions.items():
            if reg in regions:
                ids.add(i)
                self.rtree.delete(i, reg.bbox)

        for i in ids:
            self._regions.pop(i)

        self._bound = Region.from_bbox(self.rtree.bounds)
        # print(self.rtree.count(self.rtree.bounds), len(self))

    def update_region(self, region, x1=None, y1=None, x2=None, y2=None):
        if x1 is None and x2 is None and y1 is None and y2 is None:
            raise ValueError('No new value provided')

        self.remove_region(region)

        if x1 is not None:
            region.x1 = x1
        if x2 is not None:
            region.x2 = x2
        if y1 is not None:
            region.y1 = y1
        if y2 is not None:
            region.y2 = y2

        if region.area == 0:
            return

        self.add_region(region)

    def covered_regions(self, x1, y1=None, x2=None, y2=None):
        if y1 is None:      # type(x1) is Region
            return self.covered_regions(x1.x1, x1.y1, x1.x2, x1.y2)

        return set(self[x1:x2, y1:y2])

    def covered_region_with_indices(self, x1, y1=None, x2=None, y2=None):
        if y1 is None:      # type(x1) is Region
            return self.covered_regions(x1.x1, x1.y1, x1.x2, x1.y2)

        return set((idx, self._regions[idx])
                   for idx in self.rtree.intersection((x1, y1, x2, y2)))

    def get(self, index):
        return self._regions.get(index, None)

    def count(self, bbox):
        return self.rtree.count(bbox)

    def nearest(self, region, k=1):
        ret = set()
        for idx in self.rtree.nearest(region.bbox, num_results=k+1):
            cand = self._regions[idx]

            if cand is region:
                continue
            ret.add(cand)

        return ret

    def intersect(self, region):
        ret = set()
        for cand in self[region.x1:region.x2, region.y1:region.y2]:
            if cand is region:
                continue

            ret.add(cand)

        return ret

    def merge_overlapped(self):
        processed = set()
        while True:
            removed = set()
            for reg in self.regions:
                if reg in processed:
                    continue

                removed.update(self.intersect(reg))
                if removed:
                    self.remove_regions(removed)

                    target = Region(reg)
                    reduce(Region.__ior__, removed, target)
                    self.update_region(reg, *target.bbox)
                    break

                processed.add(reg)

            if not removed:
                break

    def __getitem__(self, s):
        if isinstance(s, tuple):
            x_slice, y_slice = s
        elif s is Ellipsis:
            x_slice = None
            y_slice = None
        else:
            x_slice = s
            y_slice = None

        x1, x2 = self._get_range(x_slice, 'x')
        y1, y2 = self._get_range(y_slice, 'y')

        for idx in self.rtree.intersection((x1, y1, x2, y2)):
            yield self._regions[idx]

    def _get_range(self, s, axis):
        if isinstance(s, slice):
            start, stop = s.start, s.stop
        else:   # number
            start, stop = s, s

        if start is None:
            start = self._bound.x1 if axis == 'x' else self._bound.y1
        if stop is None:
            stop = self._bound.x2 if axis == 'x' else self._bound.y2

        return start, stop

    def __setitem__(self, s, region_set):
        raise NotImplementedError(
            '{self.__class__.__name__} doesn\'t support __setitem__ operation')

    def __iter__(self):
        return iter(self._regions.values())

    def __repr__(self):
        name = self.__class__.__name__
        regions = pformat(set(self.regions))
        return (f'{name}('
                f'regions={regions})')

    def __str__(self):
        name = self.__class__.__name__
        regions = len(self._regions)
        return (f'{name}'
                f'{regions} region(s))')

    def __len__(self):
        return len(self._regions)
