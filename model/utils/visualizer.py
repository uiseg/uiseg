import time

import numpy as np
import pandas as pd
from pandas import DataFrame
import visdom

pd.set_option('display.max_colwidth', 120)


class Visualizer(object):
    """
    wrapper for visdom
    you can still access naive visdom function by
    self.line, self.scater, self._send, etc.
    """

    def __init__(self,
                 server='http://localhost',
                 port=8097,
                 env='main',
                 **kwargs):
        self.vis = visdom.Visdom(server=server,
                                 port=port,
                                 env=env,
                                 **kwargs)
        self._vis_kw = kwargs

        # e.g.（’loss',23） the 23th value of loss
        self.index = {}
        self.logs = {}

    def plot_many(self, d):
        """
        plot multi values
        @params d: dict (name,value) i.e. ('loss',0.11)
        """
        for k, v in d.items():
            if v is not None:
                self.plot(k, v)

    def plot(self, name, y, **kwargs):
        """
        self.plot('loss',1.00)
        """
        x = self.index.get(name, 0)
        self.vis.line(Y=np.array([y]),
                      X=np.array([x]),
                      win=name,
                      opts={'title': name},
                      update=None if x == 0 else 'append',
                      **kwargs)
        self.index[name] = x + 1

    def array_image(self, name, arr, **kwargs):
        """
        self.image('input_img',t.Tensor(64,64))
        self.image('input_imgs',t.Tensor(3,64,64))
        self.image('input_imgs',t.Tensor(100,1,64,64))
        self.image('input_imgs',t.Tensor(100,3,64,64),nrows=10)
        ！！！don‘t ~~self.image('input_imgs',t.Tensor(100,64,64),nrows=10)~~！！！
        """
        self.vis.images(arr,
                        win=name,
                        opts={'title': name},
                        **kwargs)

    def figure(self, name, figure, **kwargs):
        image = figure.image
        if len(image.shape) == 3:
            image = image.transpose((2, 0, 1))[::-1]
        elif len(image.shape) == 2:
            image = image[np.newaxis, ...]

        self.array_image(name, image, **kwargs)

    def image(self, name, img, **kwargs):
        img = np.asarray(img).transpose((2, 0, 1))

        self.array_image(name, img, **kwargs)

    def pyplot(self, name, plot, **kwargs):
        plot.canvas.draw()

        # Get the RGBA buffer from the figure
        w, h = plot.canvas.get_width_height()
        buf = np.frombuffer(plot.canvas.tostring_rgb(), dtype=np.uint8)
        buf.shape = (h, w, 3)
        buf = buf.transpose((2, 0, 1))

        self.array_image(name, buf, **kwargs)

    def log(self, info, win='logs'):
        """
        self.log({'loss':1,'lr':0.0001})
        """
        _time = time.strftime('%m%d_%H%M%S')
        _info = ''.join(
            [f'<p>{key}: {value}</p>'
             for key, value in info.items()]
        )

        self.logs[_time] = _info
        self.table(win, DataFrame.from_dict(self.logs,
                                            orient='index'))

    def table(self, name, table):
        if table.__class__.__name__ != 'DataFrame':
            table = DataFrame(table)
        self.vis.text(table.to_html(index=False,
                                    header=False,
                                    classes='table'),
                      win=name)

    def __getattr__(self, name):
        if name in self.__dict__:
            return self.__dict__[name]
        return getattr(self.vis, name)

    def state_dict(self):
        return {
            'index': self.index,
            'vis_kw': self._vis_kw,
            'logs': self.logs,
            'server': self.vis.server,
            'port': self.vis.port,
            'env': self.vis.env,
        }

    def load_state_dict(self, d):
        self.vis = visdom.Visdom(
            server=d.get('server', self.vis.server),
            port=d.get('port', self.vis.port),
            env=d.get('env', self.vis.env),
            **(self.d.get('vis_kw')))
        self.logs = d.get('logs', {})
        self.index = d.get('index', dict())

        return self
