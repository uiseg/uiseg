import numpy as np
import cv2

np.set_printoptions(linewidth=180)

class Figure:
    BGR = 'BGR'
    RGB = 'RGB'
    BIN = 'BIN'
    SUPPORT_MODES = (BGR, RGB, BIN)

    def __init__(self, arr, copy=True, mode=None):
        if copy:
            arr = arr.copy()

        if mode is None:
            shape = arr.shape
            if len(shape) == 3:
                mode = Figure.BGR
            elif len(shape) == 2:
                mode = Figure.BIN
        self.mode = mode

        self.image = arr

    @classmethod
    def load(cls, path):
        return cls(cv2.imread(path), copy=False)

    def save(self, path):
        cv2.imwrite(path, self.image)

    def copy(self):
        return self.__class__(self.image)

    @property
    def size(self):
        shape = self.image.shape
        return shape[1], shape[0]

    @classmethod
    def from_array(cls, arr, copy=True):
        return cls(arr, copy=copy)

    @classmethod
    def from_image(cls, image):
        # TODO: support different PIL modes
        return cls(cv2.cvtColor(np.asarray(image), cv2.COLOR_BGR2RGB),
                   copy=False)

    def to_image(self):
        from PIL import Image
        if self.mode is Figure.BGR:
            image = cv2.cvtColor(self.image, cv2.COLOR_BGR2RGB)

        elif self.mode in Figure.SUPPORT_MODES:
            image = self.image

        else:
            raise NotImplementedError(f'{self.mode} not supported')

        return Image.fromarray(image)

    def to_array(self, copy=True):
        if copy:
            return self.image.copy()
        return self.image

    def crop(self, bbox):
        x1, y1, x2, y2 = bbox
        w, h = self.size
        x1 = max(0, x1)
        y1 = max(0, y1)
        x2 = min(w, x2)
        y2 = min(h, y2)
        return Figure(self.image[y1:y2, x1:x2], copy=False)

    def show(self, async_show=True):
        if async_show:
            self.to_image().show()
        else:
            cv2.imshow('Image', self.image)
            cv2.waitKey(0)
