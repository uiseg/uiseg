import json
from os import path as osp

import numpy as np
from torch.utils.data import Dataset
from skimage import transform as sktsf
import tqdm

from . import utils

LABEL_NAMES = ('text', )

mean = np.array([0.485, 0.456, 0.406])[:, np.newaxis, np.newaxis]
std = np.array([0.229, 0.224, 0.225])[:, np.newaxis, np.newaxis]
def inverse_normalize(img):
    # approximate un-normalize for visualize
    return (img / std + mean).clip(min=0, max=1) * 255


def normalize(img):
    """
    https://github.com/pytorch/vision/issues/223
    return appr -1~1 RGB
    """
    return (img / 255.0 - mean) * std


def get_scale(size, min_size, max_size):
    w, h = size
    scale1 = min_size / min(h, w)
    scale2 = max_size / max(h, w)

    return min(scale1, scale2)


def preprocess(img, min_size, max_size=None):
    """Preprocess an image for feature extraction.

    The length of the shorter edge is scaled to :obj:`self.min_size`.
    After the scaling, if the length of the longer edge is longer than
    :param min_size:
    :obj:`self.max_size`, the image is scaled to fit the longer edge
    to :obj:`self.max_size`.

    After resizing the image, the image is subtracted by a mean image value
    :obj:`self.mean`.

    Args:
        img (~numpy.ndarray): An image. This is in CHW and RGB format.
            The range of its value is :math:`[0, 255]`.

    Returns:
        ~numpy.ndarray: A preprocessed image.

    """
    C, H, W = img.shape
    if max_size is not None:
        scale = get_scale((W, H), min_size, max_size)
    else:
        scale = min_size

    img = normalize(img)
    img = sktsf.resize(img, (C, H * scale, W * scale), mode='reflect')
    # both the longer and shorter should be less than
    # max_size and min_size
    return img


class Transform:
    def __init__(self, min_size, max_size, flip):
        self.min_size = min_size
        self.max_size = max_size
        self.flip = flip

    def __call__(self, img, bbox, label):
        _, H, W = img.shape
        img = preprocess(img, self.min_size, self.max_size)
        _, o_H, o_W = img.shape
        scale = o_H / H
        bbox = utils.resize_bbox(bbox, (H, W), (o_H, o_W))

        # horizontally flip
        if self.flip:
            img, params = utils.random_flip(
                img, x_random=True, return_param=True)
            bbox = utils.flip_bbox(
                bbox, (o_H, o_W), x_flip=params['x_flip'])

        # TODO: check whose stride is negative to fix this instead copy all
        # some of the strides of a given numpy array are negative.
        return img.copy(), bbox.copy(), label.copy(), scale


class WebUITextDataset(Dataset):
    """Bounding box dataset for PASCAL `VOC`_.

    .. _`VOC`: http://host.robots.ox.ac.uk/pascal/VOC/voc2012/

    The index corresponds to each image.

    When queried by an index, if :obj:`return_difficult == False`,
    this dataset returns a corresponding
    :obj:`img, bbox, label`, a tuple of an image, bounding boxes and labels.
    This is the default behaviour.
    If :obj:`return_difficult == True`, this dataset returns corresponding
    :obj:`img, bbox, label, difficult`. :obj:`difficult` is a boolean array
    that indicates whether bounding boxes are labeled as difficult or not.
    The bounding boxes are packed into a two dimensional tensor of shape
    :math:`(R, 4)`, where :math:`R` is the number of bounding boxes in
    the image. The second axis represents attributes of the bounding box.
    They are :math:`(y_{min}, x_{min}, y_{max}, x_{max})`, where the
    four attributes are coordinates of the top left and the bottom right
    vertices.

    The labels are packed into a one dimensional tensor of shape :math:`(R,)`.
    :math:`R` is the number of bounding boxes in the image.
    The class name of the label :math:`l` is :math:`l` th element of
    :obj:`LABEL_NAMES`.

    The array :obj:`difficult` is a one dimensional boolean array of shape
    :math:`(R,)`. :math:`R` is the number of bounding boxes in the image.
    If :obj:`use_difficult` is :obj:`False`, this array is
    a boolean array with all :obj:`False`.

    The type of the image, the bounding boxes and the labels are as follows.

    * :obj:`img.dtype == numpy.float32`
    * :obj:`bbox.dtype == numpy.float32`
    * :obj:`label.dtype == numpy.int32`
    * :obj:`difficult.dtype == numpy.bool`

    Args:
        data_dir (string): Path to the root of the training data.
            i.e. "/data/image/voc/VOCdevkit/VOC2007/"
        split ({'train', 'val', 'trainval', 'test'}): Select a split of the
            dataset. :obj:`test` split is only available for
            2007 dataset.
        year ({'2007', '2012'}): Use a dataset prepared for a challenge
            held in :obj:`year`.
        use_difficult (bool): If :obj:`True`, use images that are labeled as
            difficult in the original annotation.
        return_difficult (bool): If :obj:`True`, this dataset returns
            a boolean array
            that indicates whether bounding boxes are labeled as difficult
            or not. The default value is :obj:`False`.

    """

    SUPPORTED_SPLIT = set([
        'train',
        'val',
    ])

    def __init__(self, config, split, flip=True):
        self.data_dir = config.data.data_dir
        self.bboxes = {}
        self.labels = {}
        self.label_names = LABEL_NAMES
        with open(osp.join(self.data_dir, f'{split}.txt')) as f:
            self.ids = [line.split()[0] for line in f.read().splitlines()]

        self.transform = Transform(config.data.min_size,
                                   config.data.max_size,
                                   flip=flip)
        if split not in self.SUPPORTED_SPLIT:
            raise ValueError(f'Split "{split}" is not supported')

        ids = tqdm.tqdm(self.ids, desc=f'Load {split} bboxes')
        for _id in ids:
            anno_path = osp.join(self.data_dir, 'data', f'{_id}.json')
            bbox = []
            try:
                with open(anno_path) as f:
                    anno = json.load(f)

                for x1, y1, x2, y2 in anno['bboxes']:
                    bbox.append([y1, x1, y2, x2])

            except (FileNotFoundError, IOError):
                ids.write(f'Annotation file {anno_path} not found.')

            if bbox:
                self.bboxes[_id] = np.stack(bbox).astype(np.float32)
                self.labels[_id] = np.zeros(len(bbox), dtype=np.int32)

        ids.close()

        self.ids = list(self.bboxes.keys())
        self.bboxes = {_id: self.bboxes[_id] for _id in self.ids}
        self.labels = {_id: self.labels[_id] for _id in self.ids}

        print(f'Load {len(self)} {split} images')


    def __getitem__(self, i):
        """Returns the i-th example.

        Returns a color image and bounding boxes. The image is in CHW format.
        The returned image is RGB.

        Args:
            i (int): The index of the example.

        Returns:
            tuple of an image and bounding boxes

        """
        _id = self.ids[i]
        bbox = self.bboxes[_id]
        label = self.labels[_id]

        # Load an image
        img_file = osp.join(self.data_dir, 'data', f'{_id}.png')
        img = utils.read_image(img_file, color=True)

        return self.transform(img, bbox, label)

    def __len__(self):
        return len(self.ids)
