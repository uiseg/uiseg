def get_dataset(config):
    ret = {}
    dataset_name = config.data.dataset
    ret['name'] = dataset_name

    if dataset_name == 'voc':
        from .voc_dataset import VOCBboxDataset, inverse_normalize

        ret['dataset'] = VOCBboxDataset
        ret['test_dataset'] = VOCBboxDataset
        ret['inverse_normalize'] = inverse_normalize

    elif dataset_name == 'webui':
        from .webui_text_dataset import WebUITextDataset, inverse_normalize
        ret['dataset'] = WebUITextDataset
        ret['test_dataset'] = WebUITextDataset
        ret['inverse_normalize'] = inverse_normalize

    else:
        raise NotImplementedError(f'Cannot load "{dataset_name}" dataset.')

    return ret
