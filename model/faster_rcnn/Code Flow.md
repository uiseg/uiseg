# Code Flow

## Train `train.py`

* Load training / validation dataset from `data.dataset.Dataset` and
  `data.dataset.TestDataset`.
* Construct the model from `model.FasterRCNNVGG16`.
* Iterate over dataloader, call `trainer.train_step` for each batch.
* Constantly plot ground-truth / predicted images


## FasterRCNN `model/faster_rcnn.py`

The code flow of `FasterRCNN.forward` is as followed:

1. Extract image features from `self.extractor` module.
2. Get RPN results by forwarding image features into `self.rpn` module. This
   module requires image feature and its size and the scale parameter.
3. Get Final results (including predicted class-dependent bounding boxes,
   scores, etc.) by forwarding previous RPN results into `self.head` module.


However, it seems like no one directly use `forward` directly.


## FasterRCNNVGG16 `model/faster_rcnn_vgg16.py`


## Region Proposal Network `model/region_proposal_network.py`


## Trainer `trainer.py`

The trainer takes `FasterRCNN` model as input and creates
`AnchorTargetCreator`, `ProposalTargetCreator`, `optimizer` from
configurations. The `train_step` method calls `self.forward` internally, whose
code flow is as followed:

1. Extract image features from `self.extractor` module.
2. Get RPN results by forwarding image features into `self.rpn` module. This
   module requires image feature and its size and the scale parameter.
3. Instead of directly putting RPN results into `self.head` module, it samples
   ROIs using `ProposalTargetCreator`. Then, these ROIs are forwarded into
   `self.head` module.
4. Losses are calculated by summing up `rpn_loc_loss`, `rpn_cls_loss`,
   `roi_loc_loss`, and `rol_cls_loss`. An optimizer is used to optimize the
   networks.


## AnchorTargetCreator `model/utils/creator_tool.py`

The anchor target creator takes bounding boxes and anchors as inputs and return
the ground truth values for RPN.


## ProposalTargetCreator `model/utils/creator_tool.py`

The proposal target creator takes bounding boxes and region-of-interests as
inputs, and generate training data for head module.
