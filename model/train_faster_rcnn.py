import os
import ast
from argparse import ArgumentParser
from functools import reduce

import matplotlib
from torch.autograd import Variable
from torch.utils.data import DataLoader
from tqdm import tqdm
from pandas import DataFrame
import yaml
from chainer import cuda # pylint: disable=import-error,unused-import

from faster_rcnn import FasterRCNNVGG16, FasterRCNNTrainer
from faster_rcnn.utils.config import Config
from faster_rcnn.dataset import get_dataset
from faster_rcnn import array_tool as at
from faster_rcnn import eval_tool as et
from faster_rcnn import vis_tool as vt
from utils.visualizer import Visualizer

# fix for ulimit
# https://github.com/pytorch/pytorch/issues/973#issuecomment-346405667
import resource # pylint: disable=wrong-import-order

rlimit = resource.getrlimit(resource.RLIMIT_NOFILE)
resource.setrlimit(resource.RLIMIT_NOFILE, (20480, rlimit[1]))

matplotlib.use('agg')


def evaluate(loader, faster_rcnn, test_num=10000):
    pred_bboxes, pred_labels, pred_scores = list(), list(), list()
    gt_bboxes, gt_labels = list(), list()
    for ii, (imgs, gt_bboxes_, gt_labels_, scales) in tqdm(enumerate(loader)):
        scales = at.scalar(scales)
        imgs = imgs.cuda().float()
        imgs = Variable(imgs, volatile=True)

        pred_bboxes_, pred_labels_, pred_scores_ = faster_rcnn.predict(imgs, scales)
        gt_bboxes += list(gt_bboxes_.numpy())
        gt_labels += list(gt_labels_.numpy())
        pred_bboxes += pred_bboxes_
        pred_labels += pred_labels_
        pred_scores += pred_scores_
        if ii == test_num:
            break

    result = et.eval_detection_voc(
        pred_bboxes, pred_labels, pred_scores,
        gt_bboxes, gt_labels, use_07_metric=True)
    return result


def train(config):
    ret = get_dataset(config)
    dataset = ret['dataset'](config, split=config.data.train_split)
    dataloader = DataLoader(dataset,
                            batch_size=1,
                            shuffle=True,
                            # pin_memory=True,
                            num_workers=config.data.num_workers)
    test_dataset = ret['test_dataset'](config,
                                       split=config.data.test_split,
                                       flip=False)
    test_dataloader = DataLoader(test_dataset,
                                 batch_size=1,
                                 num_workers=config.data.test_num_workers,
                                 shuffle=False,
                                 pin_memory=True)
    print('Data loaded')

    if config.model.load_path:
        trainer = FasterRCNNTrainer.load(config.model.load_path, FasterRCNNVGG16)
        faster_rcnn = trainer.faster_rcnn
        config = trainer.config

        print(f'Load pretrained model from {config.model.load_path}')
        print('===== New config loaded =====')
        print(config)
        print('=============================')
    else:
        faster_rcnn = FasterRCNNVGG16(config)
        print('Model construct completed')

        trainer = FasterRCNNTrainer(faster_rcnn, config).cuda()

    vis = Visualizer(server=config.visualization.server,
                     port=config.visualization.port,
                     env=config.visualization.env)

    best_map = 0
    lr_ = config.training.lr
    for epoch in range(config.training.epoch):
        trainer.reset_meters()
        for ii, (img, bbox_, label_, scale) in tqdm(enumerate(dataloader)):
            if ii > config.training.train_num:
                break

            scale = at.scalar(scale)
            img, bbox, label = img.cuda().float(), bbox_.cuda(), label_.cuda()
            img, bbox, label = Variable(img), Variable(bbox), Variable(label)
            trainer.train_step(img, bbox, label, scale)

            if (ii + 1) % config.visualization.plot_every == 0:
                # Use file as a signal WTF...
                if os.path.exists(config.debug.debug_stub_file):
                    import pdb
                    pdb.set_trace()

                # plot loss
                vis.plot_many(trainer.get_meter_data())

                # plot groud truth bboxes
                ori_img_ = ret['inverse_normalize'](at.tonumpy(img[0]))
                gt_img = vt.visdom_bbox(ori_img_,
                                        at.tonumpy(bbox_[0]),
                                        at.tonumpy(label_[0]))
                vis.image('gt_img', gt_img)

                # plot predicted bboxes
                _bboxes, _labels, _scores = trainer.faster_rcnn.predict(img, scale)
                _bboxes = at.tonumpy(_bboxes[0])
                _labels = at.tonumpy(_labels[0]).reshape(-1)
                _scores = at.tonumpy(_scores[0])

                _bboxes = _bboxes[_scores > 0.7]
                _labels = _labels[_scores > 0.7]
                _scores = _scores[_scores > 0.7]

                pred_img = vt.visdom_bbox(ori_img_, _bboxes, _labels, _scores)
                vis.image('pred_img', pred_img)

                # rpn confusion matrix(meter)
                vis.text(
                    DataFrame(trainer.rpn_cm.value().tolist()).to_html(
                        header=False,
                        index=False,
                        classes='table'),
                    win='rpn_cm')
                # roi confusion matrix
                # vis.img('roi_cm', at.totensor(trainer.roi_cm.conf, False).float())
        eval_result = evaluate(test_dataloader,
                               faster_rcnn,
                               test_num=config.training.test_num)

        if eval_result['map'] > best_map:
            best_map = eval_result['map']
            best_path = trainer.save(best_map=best_map)
            print(f'Best result {best_map} saved at {best_path}')
        if epoch == 9:
            trainer.faster_rcnn.scale_lr(config.training.lr_decay)
            lr_ = lr_ * config.training.lr_decay

        vis.plot('test_map', eval_result['map'])

        if epoch == 13:
            save_path = trainer.save(info='final')
            print(f'Final model saved at {save_path}')
            break


def get_literal(v):
    if v == 'False' or v == 'false':
        return False
    elif v == 'True' or v == 'true':
        return True

    try:
        x = ast.literal_eval(v)
        return x
    except (SyntaxError, ValueError):
        return v


def parse_args():
    options = {}
    def reducer(prev, value):
        if prev:
            options[prev] = get_literal(value)
            return None

        if '=' in value:
            prev, *value = value.split('=')
            options[prev] = get_literal('='.join(value))
            return None

        return value

    parser = ArgumentParser()
    parser.add_argument('config', help='Config file')
    parser.add_argument('options', help='Overwritten Options', nargs='*')
    args = parser.parse_args()

    reduce(reducer, args.options, {})
    with open(args.config) as f:
        config = Config.from_dict(yaml.load(f))

    for key, value in options.items():
        config[key] = value

    print(config)
    return config

if __name__ == '__main__':
    train(parse_args())
