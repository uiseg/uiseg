# pylint: disable=unsubscriptable-object
from os import path as osp
import json
from operator import itemgetter
from argparse import ArgumentParser

import torch
from torch.autograd import Variable
from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm

from faster_rcnn import array_tool as at
from faster_rcnn import FasterRCNNVGG16
from faster_rcnn.utils.config import Config
from faster_rcnn.dataset.utils import read_image
from faster_rcnn.dataset.webui_text_dataset import (
    preprocess as preprocess_image,
    get_scale,
)
from webui.preprocess_contours import preprocess as preprocess_contours
import utils.dataset as dataset
from utils.task import dispatch_task
from utils.figure import Figure
from utils.region import Region
from utils import proposal_gen


SIZE = (512, 512)

class PredictDataset(Dataset):
    def __init__(self, candidates, min_size, max_size):
        self.candidates = list(candidates)
        self.scale = get_scale(SIZE, min_size, max_size)

    def __getitem__(self, i):
        path = self.candidates[i]
        image = read_image(path, color=True)
        image = preprocess_image(image, self.scale)
        return path, image.copy(), self.scale

    def __len__(self):
        return len(self.candidates)


def predict(loader, model):
    ret = []
    for path, image, scale in tqdm(loader, desc='Predicting'):
        path = path[0]
        scale = at.scalar(scale)
        image = image.cuda().float()
        *_, h, w = image.size()
        crops = {}

        for x, y in proposal_gen((w, h)):
            img = Variable(image[..., y:y+SIZE[1], x:x+SIZE[0]], volatile=True)

            bboxes, _, scores = model.predict(img, scale)
            bboxes = bboxes[0]
            scores = scores[0]

            # Return those bboxes that are predicted as foreground objects.
            bboxes = bboxes[scores > 0.7][:, [1, 0, 3, 2]]
            bboxes /= scale
            scores = scores[scores > 0.7]

            crops[(x, y, *SIZE)] = [Region.from_bbox(bbox)
                                    for bbox in bboxes.tolist()]

        result = dict(path=path, crops=crops)

        key = dataset.basekey(path)
        path = dataset.text_data2.path('json', key)
        crops = {f'{x}_{y}_{x+w}_{y+h}': [reg.bbox for reg in v]
                 for (x, y, w, h), v in crops.items()}
        with open(path, 'w') as f:
            json.dump(crops, f)

        ret.append(result)

    return ret

def merge_contours(pred):
    path = pred['path']
    crops = pred['crops']
    results = {}

    fig = Figure.load(path)
    for (x, y, w, h), texts in crops.items():
        crop_fig = fig.crop((x, y, x+w, y+h))
        key = f'{x}_{y}_{x+w}_{y+h}'
        results[key] = [reg.bbox
                        for reg in preprocess_contours(crop_fig, texts)]

    return dict(path=path, crops=results)


def main(dataset_dir, model_path):
    dataset.init(dataset_dir)
    print('Dataset directory:', dataset_dir)

    state_dict = torch.load(model_path)
    config = Config.from_dict(state_dict['config'])
    config.data.data_dir = str(dataset)
    config.model.load_path = model_path

    print(config)

    print('Loading model...')
    model = FasterRCNNVGG16(config)
    model.load_state_dict(state_dict['model'])
    model = model.cuda()

    dataset.mkdir('text_data2')
    existing = dataset.text_data2
    candidates = [(osp.getsize(path), path)
                  for path in dataset.raw.png
                  if dataset.basekey(path) not in existing]
    candidates.sort(key=itemgetter(0))
    candidates = [p[1] for p in candidates[1000:]]
    image_dataset = PredictDataset(candidates,
                                   config.data.min_size,
                                   config.data.max_size)
    print(f'Found {len(existing)} existing annotations and {len(candidates)} image(s)')

    loader = DataLoader(image_dataset,
                        batch_size=1,
                        num_workers=config.data.num_workers)
    crop_predictions = predict(loader, model)
    del model
    del state_dict
    del loader

    results = dispatch_task(merge_contours, crop_predictions)

    dataset.mkdir('contours')
    for result in tqdm(results, desc='Dumping'):
        path, crops = result['path'], result['crops']
        key = dataset.basekey(path)
        path = dataset.contours.path('json', key)

        with open(path, 'w') as f:
            json.dump(crops, f)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('dataset', help='Dataset directory')
    parser.add_argument('model', help='Model path')
    args = parser.parse_args()

    main(args.dataset, args.model)
