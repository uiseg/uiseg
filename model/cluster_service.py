import importlib
from random import sample
from functools import lru_cache
from argparse import ArgumentParser

import webui.segmentation2 as segmentation
import webui.cluster as cluster
from utils import dataset2 as dataset
from utils.figure_toolkit import draw_regions, draw_grid
from utils.service import Service, start_service
from utils.loaders import figure_loader, text_loader
import utils.debugger as debugger


figure_loader = lru_cache(maxsize=2)(figure_loader)
text_loader = lru_cache(maxsize=2)(text_loader)


def random_key():
    return sample(dataset.raw, k=1)[0]


def flatten(regions):
    ret = set()
    waiting = set(regions)
    while waiting:
        c = waiting.pop()

        ret.add(c)
        if isinstance(c, cluster.Cluster):
            waiting.update(c)

    return ret


class ClusterService(Service):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @Service.server_call
    def set_debug(self, debug=True):
        debugger.set_debug(debug)

    def client_handler(self):
        # key = 'e4eba9f88c1c7894d4245f4708818dc4'
        # fig, texts = self.get_data(key)

        # importlib.reload(segmentation)
        # importlib.reload(cluster)

        # from utils.plane import Plane
        # from functools import partial

        # (h_lines, v_lines), boxes, contours, texts = segmentation.segment(fig, texts)
        # regions, boxes = cluster.cluster(boxes, contours, texts, (h_lines, v_lines))

        # draw_grid(fig, step=100, color=(0, 0, 0,), alpha=0.3)
        # draw_regions(fig, flatten(regions))
        # draw_regions(fig, boxes, color=(255, 255, 0))
        # fig.show()

        # distance_func = partial(cluster.distance_func, Plane(h_lines | v_lines))

        from IPython.terminal.embed import InteractiveShellEmbed
        InteractiveShellEmbed(banner1='')()

    def get_data(self, key):
        return figure_loader(key).copy(), text_loader(key)

    def server_handler(self, key):
        fig, texts = self.get_data(key)

        importlib.reload(segmentation)
        importlib.reload(cluster)

        (h_lines, v_lines), boxes, contours, texts = segmentation.segment(fig, texts)
        regions = cluster.cluster(boxes, contours, texts, (h_lines, v_lines))

        draw_grid(fig, step=100, color=(0, 0, 0,), alpha=0.3)
        draw_regions(fig, flatten(regions))
        draw_regions(fig, boxes, color=(255, 255, 0))
        fig.show()

        return regions


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('dataset', help='Dataset directory')
    parser.add_argument('--sock', help='Socket path',
                        default='/tmp/cluster_server')

    group = parser.add_mutually_exclusive_group()
    group.add_argument('--server-only', action='store_true',
                       help='Only start server')
    group.add_argument('--client-only', action='store_true',
                       help='Only start client')

    args = parser.parse_args()
    dataset.init(args.dataset)
    service = ClusterService(args.sock)

    if args.server_only:
        start_service(service, only='server')
    elif args.client_only:
        start_service(service, only='client')
    else:
        start_service(service)
