import importlib
from random import sample
from functools import lru_cache
from argparse import ArgumentParser

import webui.segmentation as segmentation
import webui.generalization as generalization
from utils import dataset, debugger
from utils.region import Region
from utils.figure_toolkit import draw_regions, draw_grid
from utils.service import Service, start_service
from utils.loaders import figure_loader, text_loader


figure_loader = lru_cache(maxsize=2)(figure_loader)
text_loader = lru_cache(maxsize=2)(text_loader)


def random_key():
    return sample(dataset.raw, k=1)[0]


class GeneralizeService(Service):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.vis = None
        self.debug_crop = (0, 0, 512, 512)

    @Service.server_call
    def debug(self, debug):
        debugger.set_debug(debug)

    def client_handler(self):
        from IPython.terminal.embed import InteractiveShellEmbed
        InteractiveShellEmbed(banner1='')()

    def server_handler(self, key):
        fig = figure_loader(key).copy()
        texts = text_loader(key)

        importlib.reload(segmentation)
        importlib.reload(generalization)

        (h_lines, v_lines), texts, regions = segmentation.segment(fig, texts)
        proposals = generalization.generalize(texts, regions, (h_lines, v_lines))

        # debugger.set_debug(True)
        # plot, (ax1, ax2) = plt.subplots(nrows=2, figsize=(20, 4))

        # t = [r[1] for r in results if r[0]]
        # f = [r[1] for r in results if not r[0]]
        # ax1.hist(f, bins=50, range=(0, 100))
        # ax2.hist(t, bins=50, range=(0, 100))
        # # ax.scatter(f, [1] * len(f), alpha=0.2)
        # # ax.scatter(t, [0] * len(t), alpha=0.2)

        # debugger.vis.pyplot('Test', plot)

        h_line_regions = set(Region.from_bbox(line.bbox)
                             for line in h_lines)
        v_line_regions = set(Region.from_bbox(line.bbox)
                             for line in v_lines)

        draw_regions(fig, texts, color=(0, 255, 0))
        draw_regions(fig, regions, color=(255, 0, 0))
        draw_regions(fig, proposals, color=(0, 0, 255))
        draw_regions(fig, h_line_regions, color=(255, 255, 0))
        draw_regions(fig, v_line_regions, color=(255, 255, 0))
        draw_grid(fig, step=100, color=(0, 0, 0,), alpha=0.3)

        fig.show()


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('dataset', help='Dataset directory')
    parser.add_argument('--sock', help='Socket path',
                        default='/tmp/preprocess_contour_server')

    group = parser.add_mutually_exclusive_group()
    group.add_argument('--server-only', action='store_true',
                       help='Only start server')
    group.add_argument('--client-only', action='store_true',
                       help='Only start client')

    args = parser.parse_args()
    dataset.init(args.dataset)
    service = GeneralizeService(args.sock)

    if args.server_only:
        start_service(service, only='server')
    elif args.client_only:
        start_service(service, only='client')
    else:
        start_service(service)
