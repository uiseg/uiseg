import json
import importlib
import random
from functools import lru_cache
from argparse import ArgumentParser
from itertools import product, repeat

import requests
from tqdm import tqdm

import webui.preprocess_contours as preprocess_contours
import webui.merge_contour as merge_contour
from utils import dataset
from utils.region import Region
from utils.figure import Figure
from utils.figure_toolkit import draw_regions
from utils.service import Service, start_service

SIZE = (512, 512)
def proposal_gen(shape):
    w, h = shape
    size_x, size_y = SIZE
    proposals = list(product(range(0, w-size_x+1, int(size_x/2)),
                             range(0, h-size_y+1, int(size_y/2))))
    proposals.extend(zip(repeat(w-size_x),
                         range(0, h-size_y+1, int(size_y/2))))
    proposals.extend(zip(range(0, w-size_x+1, int(size_x/2)),
                         repeat(h-size_y)))
    proposals.append((w-size_x, h-size_y))
    return proposals


def dataset_loader(dataset_dir):
    print('Dataset directory:', dataset_dir)
    dataset.init(dataset_dir)


@lru_cache(maxsize=3)
def get_figure(key):
    if key not in dataset.raw:
        raise KeyError(f'Key {key} not found.')

    path = dataset.raw.path('png', key)
    return Figure.load(path)


@lru_cache(maxsize=10)
def get_texts(key, x, y):
    r = requests.get(f'http://localhost:8000/{key}?x={x}&y={y}')
    return r.json()


class MergeService(Service):
    def random_run(self):
        key = random.sample(dataset.raw, k=1)[0]
        self.request(key=key)

    client_handler = Service.ipython_client_handler

    def server_handler(self, data):
        data = json.loads(data)

        total_regions, total_h_lines, total_v_lines = set(), set(), set()
        key = data['key']
        fig = get_figure(key).copy()
        importlib.reload(preprocess_contours)
        importlib.reload(merge_contour)

        for x, y in tqdm(proposal_gen(fig.size), desc=key):
            sub_fig = fig.crop((x, y, x+SIZE[0], y+SIZE[1]))
            texts = get_texts(key, x, y)['bboxes']
            texts = set(Region.from_bbox(bbox) for bbox in texts)

            (h_lines, v_lines), regions = preprocess_contours.preprocess(sub_fig, texts)

            total_regions.update(reg.shift(x, y) for reg in regions)
            total_h_lines.update(line.shift(x, y) for line in h_lines)
            total_v_lines.update(line.shift(x, y) for line in v_lines)

        total_regions, total_h_lines, total_v_lines = merge_contour.merge(
            total_regions, total_h_lines, total_v_lines)
        draw_regions(fig, total_regions, color=(0, 255, 0))
        draw_regions(fig, total_h_lines, color=(255, 255, 0))
        draw_regions(fig, total_v_lines, color=(255, 255, 0))
        fig.show()


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('dataset', help='Dataset directory')
    parser.add_argument('--sock', help='Socket path',
                        default='/tmp/merge_contour_server')

    group = parser.add_mutually_exclusive_group()
    group.add_argument('--server-only', action='store_true',
                       help='Only start server')
    group.add_argument('--client-only', action='store_true',
                       help='Only start client')

    args = parser.parse_args()
    dataset_loader(args.dataset)
    service = MergeService(args.sock)

    if args.server_only:
        start_service(service, only='server')
    elif args.client_only:
        start_service(service, only='client')
    else:
        start_service(service)
