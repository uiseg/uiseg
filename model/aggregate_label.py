import json
from argparse import ArgumentParser

import utils.dataset2 as dataset
from utils.region import Region

def webui_loader(path):
    with open(path) as f:
        raw_obj = json.load(f)

    raw_obj = [o for o in raw_obj if len(o['children']) > 1]
    return len(raw_obj)


def rico_loader(path):
    with open(path) as f:
        raw_obj = json.load(f)

    raw_obj = raw_obj['activity']['root']
    candidates = []

    waiting = [raw_obj]
    while waiting:
        target = waiting.pop()
        if not target['visible-to-user'] or 'children' not in target:
            continue

        children = target['children']
        if len(children) == 0:
            continue

        if len(children) != 1:
            candidates.append(target)

        waiting.extend(children)

    return len(candidates)


def main(output_path, rico):
    keys = set(key for key in dataset.raw['json'] if key.endswith('label'))
    node_count = 0
    ignored_count = 0
    data = []

    for key in keys:
        raw_key, *_ = key.split('.')

        if rico:
            node_count += rico_loader(dataset.raw.json[raw_key])
        else:
            node_count += webui_loader(dataset.raw.json[raw_key])

        with open(dataset.raw.json[key]) as f:
            obj = json.load(f)

        nodes = obj['nodes']
        labels = obj['labels']
        ignored = set()

        if rico:
            nodes = {node['index']: Region.from_bbox(node['bbox']) for node in nodes}
            max_height = max(node.y2 for node in nodes.values())
            lower_bound, upper_bound = 0.03125 * max_height, 0.934375 * max_height
            for i, node in nodes.items():
                y1, y2 = node.y1, node.y2
                if (y1 < lower_bound and y2 < lower_bound) or (y1 > upper_bound and y2 > upper_bound):
                    ignored.add(i)

            ignored_count += len(ignored)

        data.extend(o['labels'] for o in labels if o['index'] not in ignored)

    with open(output_path, 'w') as f:
        json.dump(data, f)

    print('Number of node(s):', node_count, 'Ignored:', ignored_count)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('dataset', help='Dataset directory')
    parser.add_argument('output', help='Output path',
                        default='output.json')
    parser.add_argument('--rico', help='If using RICO dataset', action='store_true')

    args = parser.parse_args()
    dataset.init(args.dataset)

    main(args.output, args.rico)
