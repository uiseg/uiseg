import traceback
from random import sample
from argparse import ArgumentParser
from functools import lru_cache

import torch
import numpy as np
import bottle
from bottle import Bottle

from utils import proposal_gen
import utils.dataset2 as dataset
import webui.segmentation2 as segmentation
import webui.cluster as cluster
from utils.loaders import figure_loader
from utils.region import Region
from faster_rcnn import FasterRCNNVGG16
from faster_rcnn.utils.config import Config
from faster_rcnn.dataset.utils import read_image
from faster_rcnn.dataset.webui_text_dataset import (
    preprocess as preprocess_image,
    get_scale,
)

SIZE = (512, 512)

@lru_cache(maxsize=10)
def get_image(key):
    if key in dataset.raw.png:
        path = dataset.raw.png[key]

    elif key in dataset.raw.jpg:
        path = dataset.raw.jpg[key]

    else:
        raise FileNotFoundError(f'"{key}" not found in {dataset.raw}')

    return read_image(path, color=True)


def model_loader(model_path):
    state_dict = torch.load(model_path)
    config = Config.from_dict(state_dict['config'])
    config.data.data_dir = str(dataset)
    config.model.load_path = model_path

    print(config)

    print('Loading model...')
    model = FasterRCNNVGG16(config)
    model.load_state_dict(state_dict['model'])
    model = model.cuda()

    return config, model


def predict_text(key, proposals):
    ret = list()
    image = get_image(key)

    for proposal in proposals:
        x1, y1 = tuple(int(v) for v in proposal)
        x2, y2 = x1 + SIZE[0], y1 + SIZE[1]

        scale = get_scale(SIZE, config.data.min_size, config.data.max_size)
        crop = image[:, y1:y2, x1:x2].copy()
        crop = preprocess_image(crop, scale)
        crop = torch.FloatTensor(crop[np.newaxis, ...]).cuda()
        crop = torch.autograd.Variable(crop, volatile=True)

        bboxes, _, scores = model.predict(crop, scale)
        bboxes = bboxes[0][scores[0] > 0.7][:, [1, 0, 3, 2]]
        if bboxes.shape[0] == 0:
            continue

        bboxes = bboxes / scale + torch.Tensor([x1, y1, x1, y1])

        ret.extend(bboxes.tolist())

    return set(Region.from_bbox(bbox) for bbox in ret)


def normalize_cluster(regions):
    if not regions:
        return []

    index = 0
    mapping = {regions: 0}
    parent_mapping = {regions: -1}
    waiting = [(regions, r) for r in regions]

    while waiting:
        index += 1
        parent, target = waiting.pop()

        if isinstance(target, cluster.Cluster):
            waiting.extend((target, child) for child in target)

        mapping[target] = index
        parent_mapping[target] = mapping[parent]

    return [{'index': index,
             'dim': region.dim,
             'parent': parent_mapping[region],
             } for region, index in mapping.items()]


def flatten(regions):
    ret = set()
    waiting = set(regions)
    while waiting:
        c = waiting.pop()

        ret.add(c)
        if isinstance(c, cluster.Cluster):
            waiting.update(c)

    return ret


def predict(key):
    try:
        figure = figure_loader(key)
        texts = predict_text(key, proposal_gen(figure.size))

        (h_lines, v_lines), boxes, contours, texts = segmentation.segment(
            figure, texts)
        regions = cluster.cluster(boxes, contours, texts, (h_lines, v_lines))
        results = normalize_cluster(regions)

        return dict(status='ok', results=results)

    except Exception as e:
        stack = traceback.format_list(traceback.extract_stack()[:-3] +
                                      traceback.extract_tb(e.__traceback__))
        return dict(status='error', results=stack, message=str(e))


def random_key():
    return sample(dataset.raw, k=1)[0]


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('dataset', help='Dataset directory')
    parser.add_argument('model', help='Model path')
    args = parser.parse_args()

    print('Dataset directory:', args.dataset)
    dataset.init(args.dataset)
    config, model = model_loader(args.model)

    app = Bottle()
    app.route('/<key>')(predict)
    app.route('/random')(random_key)
    bottle.run(app, host='0.0.0.0', port=8080)
