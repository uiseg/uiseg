# WebUI Crawler

## Usage

1. Install dependencies: `PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=1 npm install`
2. Make sure Google Chrome 64~66 is installed
3. Comment out the following lines in `node_modules/puppeteer/lib/Page.js:707-708`
   ```
      if (options.fullPage)
        await this.setViewport(this._viewport);
   ```
   This is important because these two lines make bounding boxes fail to match
   screenshots
4. Type `node main.js --help` to see usage
