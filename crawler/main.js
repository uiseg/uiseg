const fs = require('fs')
const md5 = require('md5')
const colors = require('colors') // eslint-disable-line
const path = require('path')
const program = require('commander')
const puppeteer = require('puppeteer')
const normalizeUrl = require('normalize-url')
const PrettyError = require('pretty-error')

const inject = require('./crawler_inject')

// Enable PrettyError
const pe = new PrettyError()
pe.start()

// Process arguments
program
    .option('--dataset-dir <DIR>', 'Dataset directory')
    .option('--sites <DIR>', 'The file in which each line is a website', './sites.txt')
    .option('--limit <N>', 'Maximum number of web pages to be crawled', 5000)
    .parse(process.argv)

if (!program.datasetDir) {
    console.error('saveDir is a required argument. Abort.')
    process.exit(-1)
}

if (!fs.existsSync(program.sites)) {
    console.error(`No sites file found in ${program.sites}. Abort`)
    process.exit(-1)
}

// Global variables
const DEBUG = !(process.env.DEBUG === undefined)
const SAVE_DIR = path.join(program.datasetDir, 'raw')
const RECORD_PATH = path.join(program.datasetDir, 'records.txt')

let browser

// Make sure SAVE_DIR exists
if (!fs.existsSync(SAVE_DIR)) {
    fs.mkdirSync(SAVE_DIR)
}
// Read records and sites
const sites = new Set(fs
    .readFileSync(program.sites).toString()
    .split('\n')
    .filter(n => !!n)
    .map(normalizeUrl))

if (fs.existsSync(RECORD_PATH)) {
    let i = 0
    for(const line of fs.readFileSync(RECORD_PATH).toString().split('\n')) {
        if (!line) continue
        const [hash, url, realUrl] = line.split(' ')
        if (sites.has(url)) ++i
        sites.delete(url)
    }
    console.log(`Remove ${i} websites that have been crawled.`.green)
}

const SITES = Array.from(sites.values())
console.log(`Found ${SITES.length} candidates websites.`.green)
const RECORDS = []


// Exit function
// Write processed records to file
function onExit() {
    console.error(`▲ EXIT [${program.identifier}]`.red)
    if (RECORDS.length > 0) {
        const recordData = RECORDS.map(rec => `${rec.hash} ${rec.url} ${rec.realUrl}\n`)
        fs.appendFileSync(RECORD_PATH, recordData.join(''))
    } else {
        console.error('No records')
    }

    if (browser) {
        browser.close().then(() => process.exit())
    }
}

async function resetStatus() {
    if (browser) {
        const pages = await browser.pages()
        await Promise.all(pages.map(page => page.close()))
    }
}

async function loadPage(url) {
    url = normalizeUrl(url) // eslint-disable-line
    let realUrl = url
    try {
        const page = await browser.newPage()
        await page.goto(url, { timeout: 40000 })

        const viewport = page.viewport()
        viewport.width = 1024
        await page.setViewport(viewport)

        const randomLink = await page.evaluate(inject.findOneLink)
        if (randomLink !== '') {
            realUrl = normalizeUrl(randomLink) // eslint-disable-line
            await page.goto(randomLink, { timeout: 40000 })
            await page.evaluate(inject.scrollLeftTop)
        }

        return { page, url, realUrl }
    } catch (error) {
        return { error, url, realUrl }
    }
}

async function saveContent(page, prefix) {
    try {
        const pngPath = path.join(SAVE_DIR, `${prefix}.png`)
        await page.screenshot({
            fullPage: true,
            path: pngPath,
        })

        // const htmlPath = path.join(SAVE_DIR, `${prefix}.html`)
        // const innerHTML = await page.evaluate(inject.getBoundingClientRect)
        // fs.writeFile(htmlPath, innerHTML, (err) => {
        //     if (err) throw err
        // })

        const jsonPath = path.join(SAVE_DIR, `${prefix}.json`)
        const rects = await page.evaluate(inject.getBoundingClientRect)
        fs.writeFile(jsonPath, rects, (err) => {
            if (err) throw err
        })

        await page.close()
        return null
    } catch (e) {
        return e
    }
}

async function initPuppeteer() {
    browser = await puppeteer.launch({
        executablePath: 'google-chrome-unstable',
        ignoreHTTPSErrors: true,
        headless: true,
    })

    browser.on('disconnected', () => {
        browser = null
    })

    await resetStatus()

    return browser
}

async function crawl(urls) {
    const errors = []
    const resultPages = await Promise.all(urls.map(url => loadPage(url)))
    if (DEBUG) {
        console.log(`${'[PROGRESS]'.green} ${urls.length} loaded.`)
    }
    for (const { page, error, url, realUrl } of resultPages) {
        if (error) {
            if (DEBUG) {
                console.log(`${'[ERROR]'.red} processed url ${url} (realUrl ${realUrl})`)
            }
            console.error(error)
            errors.push(error)
            continue
        }

        const prefix = md5(url)
        const saveError = await saveContent(page, prefix)
        if (saveError) {
            errors.push(saveError)
            if (DEBUG) {
                console.log(`${'[ERROR]'.red} processed url ${url} (realUrl ${realUrl})`)
            }
            console.error(saveError)
        } else {
            RECORDS.push({ hash: prefix, url, realUrl })
            if (DEBUG) { // eslint-disable-line
                console.log(`${'[PROGRESS]'.green} processed url ${url} (realUrl ${realUrl})`)
            }
        }
    }
    await resetStatus()
    return errors
}

(async() => {
    try {
        await initPuppeteer()
        console.log(`Browser starts at ${browser.wsEndpoint()}`)
    } catch (e) {
        console.error('Failed to create Chrome instance. Abort.'.red)
        throw e
    }

    while(SITES.length > 0 || RECORDS.length >= program.limit) {
        if (!browser) return
        const urls = SITES.splice(0, 10)
        await crawl(urls)
    }
    onExit()
})().catch((err) => {
    // Only handle unexpected errors that will terminate the crawler
    console.log(err)
    onExit()
})

process.on('SIGTERM', onExit)
process.on('SIGINT', onExit)
