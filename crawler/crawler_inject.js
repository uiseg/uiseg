function getBoundingClientRect() {
    function filterNodes() {
        const targets = []
        const filterTags = new Set([
            '#comment', 'SCRIPT', 'WBR', 'NOBR'])
        const nodeIterator = document.createNodeIterator(
            document.body,
            NodeFilter.SHOW_ALL,
            () => NodeFilter.FILTER_ACCEPT,
        )

        let currentNode

        while (currentNode = nodeIterator.nextNode()) {
            if (filterTags.has(currentNode.nodeName)) {
                targets.push(currentNode)
            }
        }

        for (const x of targets) {
            if (x.parentNode) {
                const parent = x.parentNode
                parent.removeChild(x)
                parent.normalize()
            }
        }
    }

    function getRect(el) {
        const rects = []
        let target

        if (el.getClientRects) {
            target = el.getClientRects()
        } else if (el.nodeName === '#text') {
            const range = document.createRange()
            range.selectNode(el)
            target = range.getClientRects()
            range.detach()
        } else {
            return []
        }

        for (const rect of target) {
            if (rect.width === 0 || rect.height === 0) continue

            rects.push([
                rect.x,
                rect.y,
                rect.width,
                rect.height,
            ])
        }

        return rects
    }

    function isNodeVisible(node, [left, top, width, height]) {
        const [x, y, w, h] = node.dim
        return !(
            w === 0 ||
            h === 0 ||
            (x + w < left) ||
            (x > width + left) ||
            (y + h < top) ||
            (y > height + top)
        )
    }

    function createNodes(node, parent = null) {
        const nodes = []
        for (const dim of getRect(node)) {
            nodes.push({
                name: node.nodeName.toLowerCase(),
                prev: null,
                next: null,
                dim,
                node,
                parent,
                children: [],
                classes: Array.from(node.classList || []),
            })
        }
        return nodes
    }

    const ret = []
    let flatten = [] // eslint-disable-line

    filterNodes()

    const body = document.querySelector('body')
    const bodyRect = body.getBoundingClientRect()
    const documentDim = [bodyRect.x, bodyRect.y, bodyRect.width, bodyRect.height]

    const stack = createNodes(body)
    let p
    let idx = 0

    function sameDim(a, b) {
        for (let i = 0; i < 4; ++i) {
            if (Math.abs(a.dim[i] - b.dim[i]) >= 0.001) return false
        }

        return true
    }
    while (p = stack.pop()) {
        let { parent, node, children } = p // eslint-disable-line

        if (p.dim.every(v => !Number.isNaN(v)) &&
                isNodeVisible(p, documentDim)) {
            if (parent) {
                // Make sure it is visible
                // NOTE: don't move dim ??
                // p.dim[0] = Math.max(p.dim[0], parent.dim[0])
                // p.dim[1] = Math.max(p.dim[1], parent.dim[1])
                // p.dim[2] = Math.min(p.dim[2],
                //     parent.dim[0] + parent.dim[2] - p.dim[0])
                // p.dim[3] = Math.min(p.dim[3],
                //     parent.dim[1] + parent.dim[3] - p.dim[1])
                if (sameDim(p, parent) && p.name !== '#text') {
                    parent.classes = Array.from(new Set(
                        parent.classes.concat(p.classes)))
                } else if (p.dim[2] > 0 && p.dim[3] > 0) {
                    if (parent.children.length !== 0) {
                        parent.children[0].prev = p
                        p.next = parent.children[0]
                    }
                    p.index = idx++
                    flatten.push(p)
                    parent.children.unshift(p)
                    parent = p
                }
            } else {
                p.index = idx++
                flatten.push(p)
                ret.push(p)
                parent = p
            }
        }

        for (let i = 0; i < node.childNodes.length; i++) {
            stack.unshift(...createNodes(node.childNodes[i], parent))
        }
    }

    function sortFunc(a, b) {
        if (a.dim[1] < b.dim[1]) return 1
        else if (a.dim[1] > b.dim[1]) return -1
        return 0
    }
    // Only allowed to return string, so we have to flatten the tree structure.
    for (const node of flatten) {
        if (node.node.id) node.id = node.node.id

        if (node.parent) {
            node.parent = node.parent.index
        } else {
            delete node.parent
        }

        if (node.prev) {
            node.prev = node.prev.index
        } else {
            delete node.prev
        }

        if (node.next) {
            node.next = node.next.index
        } else {
            delete node.next
        }

        node.children.sort(sortFunc)
        node.children = node.children.map(x => x.index)
        delete node.node
    }
    return JSON.stringify(flatten)
}

function scrollLeftTop() {
    window.scrollTo(0, 0)
}

function findOneLink() {
    function extractHostname(url) {
        let hostname

        if (url.indexOf('://') > -1) {
            hostname = url.split('/')[2]
        } else {
            hostname = url.split('/')[0]
        }

        // find & remove port number
        hostname = hostname.split(':')[0]
        // find & remove "?"
        hostname = hostname.split('?')[0]

        return hostname
    }
    const ret = []
    for(const x of document.querySelectorAll('a')) {
        const domain = extractHostname(x.href)
        if (domain === window.location.hostname) {
            ret.push(x.href)
        }
    }
    if (ret.length > 0) {
        return ret[Math.floor(Math.random() * ret.length)]
    }

    return ''
}

module.exports = {
    findOneLink,
    scrollLeftTop,
    getBoundingClientRect,
}
